import { XAttribute } from '@/ts/base/schema';

export interface CellInfo {
  col: number;
  row: number;
  prop: Pick<XAttribute, 'id' | 'propId'>;
}
export interface ReportInfo {
  name: string;
  code: string;
  data: ReporDatatInfo;
}
interface ReporDatatInfo {
  data: any[];
  setting: ReportSettingInfo;
}
export interface ReportSettingInfo {
  row_h: number[];
  col_w: number[];
  cells: cell[];
  styleList: any[];
  classList?: any[];
  customBorders?: any[];
  grdatr?: any;
  mergeCells?: any[];
}

interface cell {
  col: number;
  row: number;
  prop: XAttribute;
}
export interface stagDataInfo {
  row: number;
  col: number;
  fieldId: string;
  value: any;
}

declare module 'handsontable/settings' {
  interface CellMeta {
    /**
     * 单元格渲染类型
     */
    renderType?: 'text' | 'input' | 'computed';
  }
}
