/** 数字转字母 */
export const numberToLetters = (number: any) => {
  let result = '';
  while (number > 0) {
    number--; // 调整偏移
    let remainder = number % 26;
    let letter = String.fromCharCode(65 + remainder);
    result = letter + result;
    number = Math.floor(number / 26);
  }
  return result;
};

/** 解析坐标 */
export const parseCoordinate = (str: any) => {
  let columnLetters = str.match(/[A-Z]+/)[0];
  let rowNumber = Number(str.match(/\d+/)[0]);
  let columnNumber = 0;
  for (let i = 0; i < columnLetters.length; i++) {
    columnNumber *= 26;
    columnNumber += columnLetters.charCodeAt(i) - 'A'.charCodeAt(0);
  }
  return [columnNumber, rowNumber - 1];
};
