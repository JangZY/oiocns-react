import { EditModal } from '@/executor/tools/editModal';
import useStorage from '@/hooks/useStorage';
import { model } from '@/ts/base';
import FormService from '@/ts/scripting/core/services/FormService';
import Toolbar, { Item } from 'devextreme-react/toolbar';
import React from 'react';
import { getItemNums } from '../Utils';
import FormItem from './formItem';

interface IWorkFormProps {
  data: { [key: string]: any };
  allowEdit: boolean;
  info?: model.FormInfo;
  service: FormService;
}

const WorkFormViewer: React.FC<IWorkFormProps> = (props) => {
  const [colNum, setColNum] = useStorage('workFormColNum', '一列');
  return (
    <div style={{ padding: 16 }}>
      <Toolbar height={60}>
        <Item
          location="center"
          locateInMenu="never"
          render={() => (
            <div className="toolbar-label">
              <b style={{ fontSize: 28 }}>{props.service.form.name}</b>
            </div>
          )}
        />
        <Item
          location="after"
          locateInMenu="never"
          widget="dxButton"
          visible={props.allowEdit && props.info?.allowSelect}
          options={{
            text: '数据选择',
            type: 'default',
            stylingMode: 'outlined',
            onClick: () => {
              EditModal.showFormSelect({
                form: props.service.form,
                fields: props.service.fields,
                belong: props.service.belong,
                multiple: false,
                onSave: (values) => {
                  if (values.length > 0) {
                    props.service.setFieldsValue(values[0]);
                  }
                },
              });
            },
          }}
        />
        <Item
          location="after"
          locateInMenu="never"
          widget="dxSelectBox"
          options={{
            text: '项排列',
            value: colNum,
            items: getItemNums(),
            onItemClick: (e: { itemData: string }) => {
              setColNum(e.itemData);
            },
          }}
        />
      </Toolbar>
      <div style={{ display: 'flex', width: '100%', flexWrap: 'wrap', gap: 10 }}>
        <FormItem
          key={'name'}
          data={props.data}
          numStr={colNum}
          rules={[]}
          readOnly={props.allowEdit}
          field={{
            id: 'name',
            name: '名称',
            code: 'name',
            valueType: '描述型',
            remark: '数据的名称。',
            options: { hideField: true },
          }}
          belong={props.service.belong}
          service={props.service}
        />
        {props.service.fields.map((field) => {
          return (
            <FormItem
              key={field.id}
              data={props.data}
              form={props.service.form}
              numStr={colNum}
              rules={[
                ...(props.service.formData?.rules ?? []),
                ...(props.service.work?.model.rules ?? []),
              ].filter((a) => a.destId == field.id)}
              readOnly={!props.allowEdit}
              field={field}
              belong={props.service.belong}
              service={props.service}
              onValuesChange={(field, value) => {
                props.service.onValueChange(field, value, props.data);
              }}
              setFieldsValue={(data) => props.service.setFieldsValue(data)}
            />
          );
        })}
      </div>
    </div>
  );
};

export default WorkFormViewer;
