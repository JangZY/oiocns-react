import React, { useState, useEffect } from 'react';
import { useEffectOnce } from 'react-use';
import QrCode from 'qrcode.react';
import { SelectBox, Form } from 'devextreme-react';
import { GroupItem, SimpleItem } from 'devextreme-react/form';
import { Button, Modal, Card } from 'antd';
import orgCtrl from '@/ts/controller';
import { PlusCircleOutlined, CloseOutlined } from '@ant-design/icons';
import { IForm } from '@/ts/core';
import { deepClone } from '@/ts/base/common';
import cls from './index.module.less';

interface Iprops {
  current: IForm;
}

const INITLABELPRINTFORM = {
  hide: false,
  qrcodeSize: '55',
  fontSize: '9',
  leaveNote: '0',
  qrcodeSource: 'id',
  width: '60',
  height: '35',
};

const INITLABELPRINTCOUNT = ['资产编号', '资产名称', '存放地点', '规格型号'];

/** 条码打印配置 */
const PrintConfig: React.FC<Iprops> = ({ current }) => {
  const [formData, setFormData] = useState<{ [key: string]: string | boolean }>(
    current.metadata.print?.config,
  );
  const [count, setCount] = useState<{ label: string; value: string; type: string }[]>(
    current.metadata.print?.qrcodeConfig || [],
  );
  const values = deepClone(current?.attributes);
  values.unshift({
    id: 'id',
    name: '唯一标识',
    code: 'id',
    valueType: '描述型',
    remark: '唯一标识',
  } as any);

  useEffectOnce(() => {
    if (!formData || JSON.stringify(formData) === '{}') {
      setFormData(INITLABELPRINTFORM);
    }
    if (count.length === 0) {
      const countArr: { label: string; value: string; type: string }[] = [];
      INITLABELPRINTCOUNT.map((item) => {
        const curItem = findItem(item);
        if (curItem) {
          countArr.push({
            label: curItem.name || '',
            type: curItem.valueType || '',
            value: curItem.id || '',
          });
        }
        setCount(countArr);
      });
    }
  });

  const findItem = (title: string) => {
    const attribute = current.attributes.find((i) => i.name === title);
    if (attribute) return attribute;
    return '';
  };

  useEffect(() => {
    current.metadata.print = {
      config: formData,
      qrcodeConfig: count || [],
    };
  }, [count, formData]);

  /** 预览渲染 */
  const renderPreview = () => {
    Modal.info({
      icon: <></>,
      title: '模板打印预览',
      okText: '关闭',
      closeIcon: <CloseOutlined />,
      maskClosable: true,
      content: (
        <>
          {formData && (
            <div
              className={cls['print-preview']}
              style={{
                width: `${formData.width}mm`,
                height: `${formData.height}mm`,
              }}>
              <div className={cls['print-preview-top']}>
                <QrCode
                  level="H"
                  size={Number(formData.qrcodeSize)}
                  renderAs="canvas"
                  value={current.id}
                />
                <div>
                  {count.map((item) => {
                    return (
                      <div
                        className={cls['print-list']}
                        key={item.value}
                        style={{
                          fontSize: `${formData.fontSize}px`,
                        }}>
                        {item.label}:{item.label}
                      </div>
                    );
                  })}
                </div>
              </div>
              <div
                className={cls['print-preview-bottom']}
                style={{
                  fontSize: `${formData.fontSize}px`,
                }}>
                {
                  orgCtrl.user.companys.find((i) => i.id === formData.qrcodeCompanyName)
                    ?.name
                }
              </div>
            </div>
          )}
        </>
      ),
    });
  };

  /** 打印基础信息配置 */
  const renderBaseConfig = () => {
    return (
      <Card
        title="标签打印设置"
        extra={
          <Button type="link" onClick={() => renderPreview()}>
            预览
          </Button>
        }>
        <Form
          scrollingEnabled
          formData={formData}
          onFieldDataChanged={(e) => {
            setFormData({ ...formData, [e.dataField as string]: e.value });
          }}>
          <GroupItem>
            <SimpleItem
              dataField="hide"
              editorType="dxCheckBox"
              label={{ text: '是否隐藏打印按钮' }}
            />
            <SimpleItem
              dataField="width"
              editorType="dxNumberBox"
              label={{ text: '模板长度(mm)' }}
            />
            <SimpleItem
              dataField="height"
              editorType="dxNumberBox"
              label={{ text: '模板高度(mm)' }}
            />
            <SimpleItem
              dataField="qrcodeSource"
              editorType="dxSelectBox"
              label={{ text: '二维码绑定字段' }}
              editorOptions={{
                displayExpr: 'name',
                valueExpr: 'id',
                dataSource: values || [],
              }}
            />
            <SimpleItem
              dataField="qrcodeSize"
              editorType="dxNumberBox"
              label={{ text: '二维码大小(px)' }}
            />
            <SimpleItem
              dataField="fontSize"
              editorType="dxNumberBox"
              label={{ text: '字体大小(px)' }}
            />
            <SimpleItem
              dataField="leaveNote"
              editorType="dxNumberBox"
              label={{ text: '顶部留白高度(mm)' }}
            />
          </GroupItem>
        </Form>
      </Card>
    );
  };

  /** 二维码信息配置 */
  const rederQrCodeConfig = () => {
    return (
      <Card>
        <div className={cls['print-code-config']}>
          <div className={cls['print-code-content']}>
            <div className={cls['print-code-content-data']}>
              <QrCode level="H" size={150} renderAs="canvas" value={current.id} />
              <div>
                {count.map((item, index) => {
                  return (
                    <div key={index} className={cls['print-code-list']}>
                      <SelectBox
                        style={{ fontStyle: { size: 50 } }}
                        label={`属性-${index + 1}`}
                        searchMode="contains"
                        searchExpr={'name'}
                        value={item.value}
                        dataSource={current.attributes}
                        displayExpr={'name'}
                        valueExpr={'id'}
                        onValueChanged={(value) => {
                          setCount((prev) => {
                            const data = [...prev];
                            data[index] = {
                              label:
                                current.attributes.find((a) => a.id === value.value)
                                  ?.name || '',
                              type:
                                current.attributes.find((a) => a.id === value.value)
                                  ?.property?.valueType || '',
                              value: value.value,
                            };
                            return data;
                          });
                        }}
                      />
                      <CloseOutlined
                        className={cls['print-code-closeicon']}
                        onClick={() => {
                          setCount((prev) => {
                            const data = [...prev];
                            data.splice(index, 1);
                            return data;
                          });
                        }}
                      />
                    </div>
                  );
                })}
              </div>
            </div>
            <div className={cls['print-code-content-comp']}>
              <Form
                scrollingEnabled
                formData={formData}
                onFieldDataChanged={(e) => {
                  setFormData({ ...formData, [e.dataField as string]: e.value });
                }}>
                <GroupItem>
                  <SimpleItem
                    dataField="qrcodeCompanyName"
                    editorType="dxSelectBox"
                    label={{ text: '绑定单位名称' }}
                    editorOptions={{
                      displayExpr: 'name',
                      valueExpr: 'id',
                      showClearButton: true,
                      dataSource:
                        orgCtrl.user.companys.map((item) => {
                          return {
                            id: item.id,
                            name: item.name,
                          };
                        }) || [],
                    }}
                  />
                </GroupItem>
              </Form>
            </div>
          </div>
          <PlusCircleOutlined
            className={cls['print-code-plusicon']}
            onClick={() =>
              setCount([
                ...count,
                {
                  label: '',
                  type: '',
                  value: '',
                },
              ])
            }
          />
        </div>
      </Card>
    );
  };

  return (
    <>
      {renderBaseConfig()}
      {rederQrCodeConfig()}
    </>
  );
};
export default PrintConfig;
