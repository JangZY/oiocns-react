import { schema } from '@/ts/base';
import { IForm, IProperty, orgAuth } from '@/ts/core';
import { List } from 'devextreme-react';
import { Button, Modal, Select } from 'antd';
import React, { useState } from 'react';
import OpenFileDialog from '@/components/OpenFileDialog';
import Toolbar, { Item } from 'devextreme-react/toolbar';
import FormItem from './formItem';
import { ItemDragging } from 'devextreme-react/list';
import { Emitter, generateUuid } from '@/ts/base/common';
import useStorage from '@/hooks/useStorage';
import { getItemNums, getItemWidth } from '../../Utils';
import styles from './index.module.less';
import { FullProperties } from '@/config/column';

const FormRender: React.FC<{
  current: IForm;
  notityEmitter: Emitter;
  onItemSelected: (index: number) => void;
}> = ({ current, notityEmitter, onItemSelected }) => {
  if (current.metadata.attributes === undefined) {
    current.metadata.attributes = [];
  }
  const [key, setKey] = useState(generateUuid());
  const [openDialog, setDialog] = React.useState(false);
  const [colNum, setColNum] = useStorage('workFormColNum', '一列');
  const showDialog = React.useCallback(() => setDialog(true), []);
  const onReorder = React.useCallback((e: { fromIndex: number; toIndex: number }) => {
    const fromAttr = current.metadata.attributes.splice(e.fromIndex, 1);
    current.metadata.attributes.splice(e.toIndex, 0, ...fromAttr);
  }, []);
  return (
    <div style={{ padding: 16 }}>
      <Toolbar height={60}>
        <Item
          location="center"
          locateInMenu="never"
          render={() => (
            <div className="toolbar-label">
              <b style={{ fontSize: 20 }}>{current.name}</b>
            </div>
          )}
        />
        <Item
          location="after"
          locateInMenu="never"
          render={() => (
            <Button
              type="primary"
              onClick={() => {
                let value: string;
                const modal = Modal.confirm({
                  icon: <></>,
                  title: '加入内置属性',
                  okText: '确认',
                  cancelText: '取消',
                  maskClosable: true,
                  content: (
                    <Select<string>
                      style={{ width: '100%' }}
                      options={['实体商品', '虚拟商品', '报表数据'].map((i) => {
                        return {
                          value: i,
                          label: i,
                        };
                      })}
                      onChange={(e) => {
                        value = e;
                      }}
                    />
                  ),
                  onOk: () => {
                    if (value) {
                      FullProperties(value)
                        .filter((item) => {
                          return !current.attributes
                            .map((item) => item.propId)
                            .includes(item.id);
                        })
                        .forEach((item) => {
                          current.metadata.attributes.push({
                            propId: item.id,
                            property: item,
                            ...item,
                            rule: '{}',
                            options: {
                              isNative: true,
                              visible: true,
                              isRequired: true,
                            },
                            formId: current.id,
                            authId: orgAuth.SuperAuthId,
                          });
                        });
                      setKey(generateUuid());
                    }
                    modal.destroy();
                  },
                  onCancel: () => modal.destroy(),
                });
              }}>
              + 添加内置属性
            </Button>
          )}
        />
        <Item
          location="after"
          locateInMenu="never"
          render={() => (
            <Button type="primary" onClick={showDialog}>
              + 添加属性
            </Button>
          )}
        />
        <Item
          location="after"
          locateInMenu="never"
          widget="dxSelectBox"
          options={{
            text: '项排列',
            value: colNum,
            items: getItemNums(),
            onItemClick: (e: { itemData: string }) => {
              setColNum(e.itemData);
            },
          }}
        />
      </Toolbar>
      <div className={styles.dragList}>
        <style>{`:root {
        --primary-dragList: ${getItemWidth(colNum)}
        }`}</style>
        <List<schema.XAttribute, string>
          key={key}
          itemKeyFn={(attr: schema.XAttribute) => attr.id}
          dataSource={current.metadata.attributes}
          height={'calc(100vh - 175px)'}
          width={'100%'}
          searchEnabled
          scrollingEnabled
          searchMode="contains"
          focusStateEnabled={false}
          activeStateEnabled={false}
          pageLoadMode="scrollBottom"
          searchExpr={['name', 'remark']}
          scrollByContent={false}
          allowItemDeleting
          onItemClick={(e) => {
            e.event?.stopPropagation();
            if (e.itemData) {
              const index = current.metadata.attributes.findIndex(
                (i) => i.id === e.itemData?.id,
              );
              if (index > -1) {
                onItemSelected(index);
                return;
              }
            }
            onItemSelected(e.itemIndex as number);
          }}
          onItemReordered={onReorder}
          onItemDeleted={() => onItemSelected(-1)}
          itemRender={(attr: schema.XAttribute) => {
            return (
              <FormItem attr={attr} current={current} notityEmitter={notityEmitter} />
            );
          }}
          itemDeleteMode="static">
          <ItemDragging
            autoScroll
            allowReordering
            dragDirection="both"
            dropFeedbackMode="push"
            bindingOptions={{
              location: 'before',
            }}
            data={current.metadata.attributes}
          />
        </List>
      </div>
      {openDialog && (
        <OpenFileDialog
          multiple
          title={`选择属性`}
          accepts={['属性']}
          rootKey={current.spaceKey}
          excludeIds={current.attributes.filter((i) => i.propId).map((a) => a.propId)}
          onCancel={() => setDialog(false)}
          onOk={(files) => {
            (files as IProperty[]).forEach((item) => {
              current.metadata.attributes.push({
                propId: item.id,
                property: item.metadata,
                ...item.metadata,
                rule: '{}',
                options: {
                  visible: true,
                  isRequired: true,
                },
                formId: current.id,
                authId: orgAuth.SuperAuthId,
              });
            });
            setDialog(false);
          }}
        />
      )}
    </div>
  );
};

export default FormRender;
