import { IForm } from '@/ts/core';
import React, { useRef } from 'react';
import Viewer from './Viewer';
import useAsyncLoad from '@/hooks/useAsyncLoad';
import { Spin } from 'antd';
import FormService from '@/ts/scripting/core/services/FormService';

const WorkForm: React.FC<{
  form: IForm;
}> = ({ form }) => {
  const service = useRef(FormService.fromIForm(form));
  const [loaded] = useAsyncLoad(() => form.loadContent(), [form]);
  if (loaded) {
    return <Viewer data={{}} allowEdit={false} service={service.current} />;
  }
  return (
    <Spin tip={'信息加载中...'} spinning={!loaded}>
      <div style={{ width: '100%', height: '100%' }}></div>
    </Spin>
  );
};

export default WorkForm;
