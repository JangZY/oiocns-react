import Node from './Node';
import Concurrent from './ConcurrentNode';
import Condition from './ConditionNode';
import DeptWay from './DeptWayNode';
import { WorkNodeDisplayModel } from '@/utils/work';
import { AddNodeType } from '@/utils/work';
import React from 'react';
import { ITarget } from '@/ts/core';
import EndNode from './EndNode';

export interface NodeProps {
  onInsertNode: Function;
  onDelNode: Function;
  onSelected: Function;
  config: any;
  level: any;
  isEdit: boolean;
  target?: ITarget;
}

//解码渲染的时候插入dom到同级
export const decodeAppendDom = (node: WorkNodeDisplayModel, props: NodeProps) => {
  switch (node.type) {
    case AddNodeType.CONDITION:
      return <Condition {...props} />;
    case AddNodeType.CONCURRENTS:
      return <Concurrent {...props} />;
    case AddNodeType.ORGANIZATIONA:
      return <DeptWay {...props} />;
    case AddNodeType.END:
      return <EndNode {...props} />;
    default:
      return <Node {...props} />;
  }
};
