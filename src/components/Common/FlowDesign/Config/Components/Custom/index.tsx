import React, { useEffect, useState } from 'react';
import { Card, Divider, Space } from 'antd';
import cls from './index.module.less';
import { WorkNodeDisplayModel } from '@/utils/work';
import ShareShowComp, { FormOption } from '@/components/Common/ShareShowComp';
import { IBelong, ITarget, IWork, IPrint } from '@/ts/core';
import { command, model, schema } from '@/ts/base';
import { IForm, Form as SForm } from '@/ts/core/thing/standard/form';
import OpenFileDialog from '@/components/OpenFileDialog';
import { SelectBox } from 'devextreme-react';
import { getUuid } from '@/utils/tools';
import Rule from '../../Rule';
import ExecutorConfigModal from './configModal';
import ExecutorShowComp from '@/components/Common/ExecutorShowComp';
import SelectAuth from '@/components/Common/SelectAuth';
import TreeSelectItem from '@/components/DataStandard/WorkForm/Viewer/customItem/treeItem';
import useAsyncLoad from '@/hooks/useAsyncLoad';
import { CloseOutlined } from '@ant-design/icons';
import PrintConfigModal from '../PrintNode/PrintModal';
import orgCtrl from '@/ts/controller';

interface IProps {
  work: IWork;
  current: WorkNodeDisplayModel;
  belong: IBelong;
  refresh: () => void;
}

/**
 * @description: 审批对象
 * @return {*}
 */

const CustomNode: React.FC<IProps> = (props) => {
  const [executors, setExecutors] = useState<model.Executor[]>([]);
  const [formModel, setFormModel] = useState<string>('');
  const [primaryForms, setPrimaryForms] = useState<schema.XForm[]>();
  const [destType, setDestType] = useState(props.current.destType ?? '人员');
  const [destId, setDestId] = useState<string | undefined>(props.current.destId);
  const [executorModal, setExecutorModal] = useState(false);
  const [openType, setOpenType] = useState<string>(''); // 打开弹窗
  const [primaryPrints, setPrimaryPrints] = useState(props.current.print ?? []);
  const [printType, setPrintType] = useState<string>(
    (props.current.printData && props.current.printData.type) ?? '默认无',
  );
  const [printLoaded, prints] = useAsyncLoad(async () => {
    return await props.work.application.loadAllPrint();
  });
  useEffect(() => {
    const fetchData = async () => {
      const newData: any = [];
      try {
        const IDsIprint = await orgCtrl.loadAllDefaultIPrint(props.work.belongId);
        const IPrints = await orgCtrl.loadAllIPrint();
        IPrints.forEach((item) => {
          if (
            props.work.belongId == item.belongId &&
            !item.groupTags.includes('已删除')
          ) {
            if (
              item.path.length > 0 &&
              item.path[1] == IDsIprint[0] &&
              item.path[2] == IDsIprint[1]
            ) {
              newData.push(item);
            }
          }
        });
        const newPrimaryPrint = primaryPrints.filter(
          (obj1) => !newData.some((obj2: any) => obj1.id === obj2.id),
        );
        setPrimaryPrints([
          { id: '默认无', name: '默认无', table: '默认无' },
          ...newPrimaryPrint,
          ...newData,
        ]);
      } catch (error) {
        console.error('Error fetching data:', error);
      }
    };
    fetchData();
  }, []);
  useEffect(() => {
    props.current.primaryForms = props.current.primaryForms || [];
    props.current.executors = props.current.executors || [];
    setExecutors(props.current.executors);
    setPrimaryForms(props.current.primaryForms);
    setPrimaryForms(props.current.primaryForms);
  }, [props.current]);
  const formViewer = React.useCallback((form: schema.XForm) => {
    command.emitter(
      'executor',
      'open',
      new SForm({ ...form, id: '_' + form.id }, props.belong.directory),
      'preview',
    );
  }, []);
  useEffect(() => {
    if (primaryPrints.length) {
      prints?.forEach((item) => {
        primaryPrints.forEach((item2) => {
          if (item.id == item2.id) {
            item2.table = item.table;
            item2.name = item.name;
          }
        });
      });
      let newPrintData = primaryPrints.filter((item) => item.id == '默认无');
      if (newPrintData.length == 0) {
        setPrimaryPrints([
          { id: '默认无', name: '默认无', table: '默认无' },
          ...primaryPrints,
        ]);
      }
    }
  }, [printLoaded]);
  const handleRemoveItem = (e, data) => {
    e.stopPropagation();
    if (data.id == '默认无') {
      return false;
    }
    const newPrintData = props.current.print.filter((option) => option.id !== data.id);
    const newPrintData2 = props.current.printData.attributes.filter(
      (option) => option.title !== data.id,
    );
    props.current.print = newPrintData;
    setPrimaryPrints([...newPrintData]);
    props.current.printData.attributes = newPrintData2;
  };
  /** 审批对象选择组件 */
  const rednerSelectedFC = (type: '身份' | '角色') => {
    const getTreeItems = (targets: ITarget[], pid?: string): any[] => {
      return targets.reduce((result, item) => {
        // 递归处理子目标
        const children = item.subTarget ? getTreeItems(item.subTarget, item.id) : [];
        // 添加当前项到结果数组
        result.push({
          id: item.id,
          text: item.name,
          parentId: pid,
          value: item.id,
        } as model.FiledLookup);
        // 添加子项到结果数组
        result.push(...children);
        return result;
      }, [] as model.FiledLookup[]);
    };
    switch (type) {
      case '身份': {
        const dataSource: model.FiledLookup[] = getTreeItems([props.belong]);
        return (
          <Space direction="vertical" style={{ width: '100%' }}>
            <TreeSelectItem
              displayType={2}
              readOnly={false}
              flexWrap={'nowrap'}
              label={'组织选择'}
              defaultValue={props.current.destId}
              speciesItems={dataSource}
              onValueChanged={(e) => {
                props.current.destId = e.value;
                props.current.destName =
                  '角色:' + dataSource.find((a) => a.id == e.value)?.text;
              }}
            />
          </Space>
        );
      }
      case '角色':
        return (
          <SelectAuth
            excludeAll
            space={props.belong}
            value={destId}
            onChange={(value, label) => {
              if (props.current.destId !== value) {
                props.current.destName = '权限: ' + label;
                props.current.destId = value;
                setDestId(value);
              }
            }}
          />
        );
      default:
        return <div>暂不支持配置 {type}</div>;
    }
  };

  return (
    <div className={cls[`app-roval-node`]}>
      <div className={cls[`roval-node`]}>
        <Card
          type="inner"
          title={
            <div>
              <Divider type="vertical" className={cls['divider']} />
              <span>审批对象</span>
            </div>
          }
          className={cls[`card-info`]}
          extra={
            <>
              <SelectBox
                value={destType}
                valueExpr={'value'}
                displayExpr={'label'}
                style={{ width: 120, display: 'inline-block' }}
                onSelectionChanged={(e) => {
                  if (props.current.destType != e.selectedItem.value) {
                    props.current.destType = e.selectedItem.value;
                    setDestType(e.selectedItem.value);
                    props.current.destId = e.selectedItem.value === '人员' ? '0' : '';
                    props.current.destName = '';
                    setDestId(props.current.destId);
                  }
                }}
                dataSource={[
                  { value: '人员', label: '人员' },
                  { value: '身份', label: '角色' },
                  { value: '角色', label: '权限' },
                ]}
              />
            </>
          }>
          {destType && rednerSelectedFC(destType as '身份')}
        </Card>
        <Card
          type="inner"
          title={
            <div>
              <Divider type="vertical" className={cls['divider']} />
              <span>表单管理</span>
            </div>
          }
          className={cls[`card-info`]}
          extra={
            <a
              onClick={() => {
                setFormModel('主表');
              }}>
              添加
            </a>
          }>
          {primaryForms && primaryForms.length > 0 && (
            <span>
              <ShareShowComp
                departData={primaryForms}
                onClick={formViewer}
                deleteFuc={(id: string) => {
                  props.current.primaryForms = primaryForms?.filter((a) => a.id != id);
                  props.current.forms = props.current.forms.filter((a) => {
                    return !(a.typeName == '主表' && a.id == id);
                  });
                  setPrimaryForms(props.current.primaryForms);
                }}
                tags={(id) => {
                  const info = props.current.forms.find(
                    (a) => a.typeName == '主表' && a.id == id,
                  );
                  if (info) {
                    return <FormOption operateRule={info} typeName="主表" />;
                  }
                }}
              />
            </span>
          )}
        </Card>
        <Card
          type="inner"
          title={
            <div>
              <Divider type="vertical" className={cls['divider']} />
              <span>执行器配置</span>
            </div>
          }
          className={cls[`card-info`]}
          extra={
            <>
              <a
                type="link"
                onClick={() => {
                  setExecutorModal(true);
                }}>
                添加
              </a>
            </>
          }>
          {executors && executors.length > 0 && (
            <span>
              <ExecutorShowComp
                work={props.work}
                executors={executors}
                deleteFuc={(id: string) => {
                  const exes = executors.filter((a) => a.id != id);
                  setExecutors(exes);
                  props.current.executors = exes;
                }}
              />
            </span>
          )}
        </Card>
        <Rule
          work={props.work}
          current={props.current}
          primaryForms={primaryForms ?? []}
          detailForms={[]}
        />
        <Card
          type="inner"
          title={
            <div>
              <Divider type="vertical" className={cls['divider']} />
              <span>打印模板设置</span>
            </div>
          }
          className={cls['card-info']}
          extra={
            <>
              <a
                onClick={() => {
                  setOpenType('打印模板添加');
                }}>
                添加
              </a>
            </>
          }>
          <SelectBox
            showClearButton
            value={printType}
            placeholder="请选择打印模板"
            dataSource={primaryPrints}
            displayExpr={'name'}
            valueExpr={'id'}
            onFocusIn={() => {
              setPrintType('');
            }}
            onValueChange={(e) => {
              props.current.printData
                ? (props.current.printData.type = e)
                : (props.current.printData = { attributes: [], type: e });
              setPrintType(e);
              if (e == '默认无') {
                setOpenType('');
              } else if (e == null) {
                setOpenType('');
              } else {
                setOpenType('打印模板设置');
              }
            }}
            itemRender={(data) => (
              <div style={{ display: 'flex', justifyContent: 'space-between' }}>
                <span style={{ whiteSpace: 'nowrap' }}>{data.name}</span>
                <CloseOutlined onClick={(e) => handleRemoveItem(e, data)} />
              </div>
            )}
          />
        </Card>
      </div>
      {formModel != '' && (
        <OpenFileDialog
          multiple
          title={`选择表单`}
          rootKey={props.belong.directory.key}
          accepts={['表单']}
          excludeIds={primaryForms?.map((i) => i.id)}
          onCancel={() => setFormModel('')}
          onOk={(files) => {
            if (files.length > 0) {
              const forms = (files as unknown[] as IForm[]).map((i) => i.metadata);
              props.current.primaryForms.push(...forms);
              setPrimaryForms(props.current.primaryForms);
              props.current.forms = [
                ...(props.current.forms ?? []),
                ...forms.map((item) => {
                  return {
                    id: item.id,
                    typeName: formModel,
                    allowAdd: false,
                    allowEdit: false,
                    allowSelect: false,
                  };
                }),
              ];
            }
            setFormModel('');
          }}
        />
      )}
      {executorModal ? (
        <ExecutorConfigModal
          refresh={(param) => {
            if (param) {
              executors.push({
                id: getUuid(),
                trigger: param.trigger,
                funcName: param.funcName,
                changes: [],
                hookUrl: '',
                belongId: props.belong.id,
                acquires: [],
              });
              setExecutors([...executors]);
              props.current.executors = executors;
            }
            setExecutorModal(false);
          }}
          current={props.current}
        />
      ) : null}
      {openType == '打印模板设置' && (
        <PrintConfigModal
          refresh={() => {
            setOpenType('');
          }}
          current={props.current}
          work={props.work}
          printType={props.current.printData.type}
          primaryForms={primaryForms ? primaryForms : []}
          detailForms={[]}
          type={'workNode'}
        />
      )}
      {openType == '打印模板添加' && printLoaded && (
        <OpenFileDialog
          multiple
          title={`选择打印模板`}
          rootKey={props.work.application.key}
          currentKey={props.work.application.key}
          accepts={['打印模板']}
          fileContents={prints}
          excludeIds={primaryPrints.map((i) => i.id)}
          leftShow={false}
          rightShow={false}
          onCancel={() => setOpenType('')}
          onOk={(files) => {
            if (files.length > 0) {
              const prints = (files as unknown[] as IPrint[]).map((i) => i.metadata);
              props.current.primaryPrints = [
                ...(props.current.primaryPrints ?? []),
                ...prints,
              ];
              const setPrintInfos = (bool: boolean) => {
                props.current.print = [...(props.current.print ?? []), ...prints];
              };
              setPrintInfos(false);
              setPrimaryPrints((prevState: any) => [...prints, ...prevState]);
            }
            setOpenType('');
          }}
        />
      )}
    </div>
  );
};
export default CustomNode;
