import React, { useEffect, useState } from 'react';
import { Card, Divider } from 'antd';
import cls from './index.module.less';
import { WorkNodeDisplayModel } from '@/utils/work';
import ShareShowComp, { FormOption } from '@/components/Common/ShareShowComp';
import { IBelong, IForm, IPrint, IWork, TargetType } from '@/ts/core';
import OpenFileDialog from '@/components/OpenFileDialog';
import { command, model, schema } from '@/ts/base';
import { Form } from '@/ts/core/thing/standard/form';
import { SelectBox } from 'devextreme-react';
import { getUuid } from '@/utils/tools';
import Rule from '../../Rule';
import ExecutorShowComp from '@/components/Common/ExecutorShowComp';
import ExecutorConfigModal from '../ApprovalNode/configModal';
import ButtonConfig from '../../Button';
import PrintConfigModal from '../PrintNode/PrintModal';
import { CloseOutlined } from '@ant-design/icons';
import useAsyncLoad from '@/hooks/useAsyncLoad';
import orgCtrl from '@/ts/controller';

interface IProps {
  work: IWork;
  belong: IBelong;
  current: WorkNodeDisplayModel;
  refresh: () => void;
}
/**
 * @description: 角色
 * @return {*}
 */

const RootNode: React.FC<IProps> = (props) => {
  const rule = JSON.parse(props.work.metadata.rule ?? '{}');
  const [formModel, setFormModel] = useState<string>('');
  props.current.primaryForms = props.current.primaryForms || [];
  props.current.detailForms = props.current.detailForms || [];
  props.current.buttons = props.current.buttons || [];
  const [primaryForms, setPrimaryForms] = useState(props.current.primaryForms);
  const [primaryPrints, setPrimaryPrints] = useState(props.current.print);
  const [detailForms, setDetailForms] = useState(props.current.detailForms);
  const [allowInitiate, setAllowInitiate] = useState<boolean>(
    props.work.metadata.allowInitiate,
  );
  const [applyType, setApplyType] = useState<string>(rule.applyType ?? '默认');
  const [executors, setExecutors] = useState<model.Executor[]>(
    props.current.executors ?? [],
  );
  const [executorModal, setExecutorModal] = useState(false);
  const [printModalCreate, setPrintModalCreate] = useState(false);
  const [printModal, setPrintModal] = useState(false);
  const formViewer = React.useCallback((form: schema.XForm) => {
    command.emitter(
      'executor',
      'open',
      new Form({ ...form, id: '_' + form.id }, props.belong.directory),
      'preview',
    );
  }, []);
  const [printType, setPrintType] = useState<string>(
    props.current.printData.type ?? '默认无',
  );
  const [formLoaded, forms] = useAsyncLoad(async () => {
    return await props.work.application.loadAllForms();
  });
  const [printLoaded, prints] = useAsyncLoad(async () => {
    return await props.work.application.loadAllPrint();
  });
  useEffect(() => {
    prints?.forEach((item) => {
      primaryPrints.forEach((item2) => {
        if (item.id == item2.id) {
          item2.table = item.table;
          item2.name = item.name;
        }
      });
    });
    let newPrintData = primaryPrints.filter((item) => item.id == '默认无');
    if (newPrintData.length == 0) {
      setPrimaryPrints([
        { id: '默认无', name: '默认无', table: '默认无' },
        ...primaryPrints,
      ]);
    }
  }, [printLoaded]);
  useEffect(() => {
    const fetchData = async () => {
      const newData: any = [];
      try {
        const IDsIprint = await orgCtrl.loadAllDefaultIPrint(props.work.belongId);
        const IPrints = await orgCtrl.loadAllIPrint();
        IPrints.forEach((item) => {
          if (
            props.work.belongId == item.belongId &&
            !item.groupTags.includes('已删除')
          ) {
            if (
              item.path.length > 0 &&
              item.path[1] == IDsIprint[0] &&
              item.path[2] == IDsIprint[1]
            ) {
              newData.push(item);
            }
          }
        });
        const newPrimaryPrint = primaryPrints.filter(
          (obj1) => !newData.some((obj2: any) => obj1.id === obj2.id),
        );
        setPrimaryPrints([
          { id: '默认无', name: '默认无', table: '默认无' },
          ...newPrimaryPrint,
          ...newData,
        ]);
      } catch (error) {
        console.error('Error fetching data:', error);
      }
    };
    fetchData();
  }, []);
  return (
    <div className={cls[`app-roval-node`]}>
      <div className={cls[`roval-node`]}>
        <Card
          type="inner"
          title={
            <div>
              <Divider type="vertical" className={cls['divider']} />
              <span>打开方式</span>
            </div>
          }
          className={cls['card-info']}
          extra={<></>}>
          <SelectBox
            showClearButton
            value={applyType}
            dataSource={[
              { text: '默认', value: '默认' },
              { text: '选择', value: '选择' },
              { text: '列表', value: '列表' },
              { text: '财务', value: '财务' },
              { text: '总账', value: '总账' },
              { text: '组合办事', value: '组合办事' },
            ]}
            displayExpr={'text'}
            valueExpr={'value'}
            onValueChange={(e) => {
              rule.applyType = e;
              props.work.metadata.rule = JSON.stringify(rule);
              setApplyType(e);
            }}
          />
        </Card>
        <Card
          type="inner"
          title={
            <div>
              <Divider type="vertical" className={cls['divider']} />
              <span>是否允许直接发起</span>
            </div>
          }
          className={cls['card-info']}
          extra={<></>}>
          <SelectBox
            showClearButton
            value={allowInitiate}
            dataSource={[
              { text: '是', value: true },
              { text: '否', value: false },
            ]}
            displayExpr={'text'}
            valueExpr={'value'}
            onValueChange={(e) => {
              rule.allowInitiate = e;
              props.work.metadata.rule = JSON.stringify(rule);
              setAllowInitiate(e);
            }}
          />
        </Card>
        <Card
          type="inner"
          title={
            <div>
              <Divider type="vertical" className={cls['divider']} />
              <span>主表配置</span>
            </div>
          }
          className={cls['card-info']}
          bodyStyle={{ padding: '12px' }}
          extra={
            <a
              onClick={() => {
                setFormModel('主表');
              }}>
              添加
            </a>
          }
          actions={[<>当进行选择操作时，新增创建新的物，变更修改旧的物。</>]}>
          {primaryForms && primaryForms.length > 0 && (
            <span>
              <ShareShowComp
                departData={primaryForms}
                onClick={formViewer}
                deleteFuc={(id: string) => {
                  props.current.primaryForms = primaryForms?.filter((a) => a.id != id);
                  props.current.forms = props.current.forms.filter((a) => {
                    return !(a.typeName == '主表' && a.id == id);
                  });
                  setPrimaryForms(props.current.primaryForms);
                }}
                tags={(id) => {
                  const info = props.current.forms.find(
                    (a) => a.typeName == '主表' && a.id == id,
                  );
                  if (info) {
                    return <FormOption operateRule={info} typeName="主表" />;
                  }
                }}
              />
            </span>
          )}
        </Card>
        <Card
          type="inner"
          title={
            <div>
              <Divider type="vertical" className={cls['divider']} />
              <span>子表配置</span>
            </div>
          }
          className={cls[`card-info`]}
          bodyStyle={{ padding: detailForms.length ? '12px' : '0' }}
          extra={
            <a
              onClick={() => {
                setFormModel('子表');
              }}>
              添加
            </a>
          }>
          {detailForms.length > 0 && (
            <span>
              <ShareShowComp
                departData={detailForms}
                onClick={formViewer}
                deleteFuc={(id: string) => {
                  props.current.detailForms = detailForms?.filter((a) => a.id != id);
                  props.current.forms = props.current.forms.filter((a) => {
                    return !(a.typeName == '子表' && a.id == id);
                  });
                  setDetailForms(props.current.detailForms);
                }}
                tags={(id) => {
                  const info = props.current.forms.find(
                    (a) => a.typeName == '子表' && a.id == id,
                  );
                  if (info) {
                    return (
                      <FormOption
                        operateRule={info}
                        typeName="子表"
                        work={props.work}
                        departData={detailForms}
                      />
                    );
                  }
                }}
              />
            </span>
          )}
        </Card>
        <Card
          type="inner"
          title={
            <div>
              <Divider type="vertical" className={cls['divider']} />
              <span>执行器配置</span>
            </div>
          }
          className={cls[`card-info`]}
          bodyStyle={{ padding: executors && executors.length ? '24px' : '0' }}
          extra={
            <>
              <a
                onClick={() => {
                  setExecutorModal(true);
                }}>
                添加
              </a>
            </>
          }>
          {executors && executors.length > 0 && (
            <span>
              <ExecutorShowComp
                work={props.work}
                executors={executors}
                deleteFuc={(id: string) => {
                  var exes = executors.filter((a) => a.id != id);
                  setExecutors(exes);
                  props.current.executors = exes;
                }}
              />
            </span>
          )}
        </Card>
        <ButtonConfig work={props.work} current={props.current} />
        <Rule
          work={props.work}
          current={props.current}
          primaryForms={primaryForms}
          detailForms={detailForms}
        />
        <Card
          type="inner"
          title={
            <div>
              <Divider type="vertical" className={cls['divider']} />
              <span>打印模板设置</span>
            </div>
          }
          className={cls['card-info']}
          extra={
            <>
              <a
                onClick={() => {
                  setPrintModalCreate(true);
                }}>
                添加
              </a>
            </>
          }>
          <SelectBox
            showClearButton
            value={printType}
            placeholder="请选择打印模板"
            dataSource={primaryPrints}
            displayExpr={'name'}
            valueExpr={'id'}
            onFocusIn={() => {
              setPrintType('');
            }}
            onValueChange={(e) => {
              props.current.printData.type = e;
              setPrintType(e);
              if (e == '默认无') {
                setPrintModal(false);
              } else if (e == null) {
                setPrintModal(false);
              } else {
                setPrintModal(true);
              }
            }}
            itemRender={(data) => (
              <div style={{ display: 'flex', justifyContent: 'space-between' }}>
                <span style={{ whiteSpace: 'nowrap' }}>{data.name}</span>
                <CloseOutlined
                  onClick={(e) => {
                    e.stopPropagation();
                    if (data.id == '默认无') {
                      return false;
                    }
                    const newPrintData = primaryPrints.filter(
                      (option) => option.id !== data.id,
                    );
                    const newPrintData2 = props.current.printData.attributes.filter(
                      (option) => option.title !== data.id,
                    );
                    props.current.print = newPrintData;
                    setPrimaryPrints([...newPrintData]);
                    props.current.printData.attributes = newPrintData2;
                  }}
                />
              </div>
            )}
          />
        </Card>
        {formModel != '' && formLoaded && (
          <OpenFileDialog
            multiple
            title={`选择${formModel}表单`}
            rootKey={props.work.application.key}
            currentKey={props.work.application.key}
            accepts={['表单', '报表']}
            fileContents={forms}
            excludeIds={(formModel === '子表' ? detailForms : primaryForms).map(
              (i) => i.id,
            )}
            leftShow={false}
            rightShow={false}
            onCancel={() => setFormModel('')}
            onOk={(files) => {
              if (files.length > 0) {
                const forms = (files as unknown[] as IForm[]).map((i) => i.metadata);
                const setFormInfos = (bool: boolean) => {
                  props.current.forms = [
                    ...(props.current.forms ?? []),
                    ...forms.map((item) => {
                      return {
                        id: item.id,
                        typeName: formModel,
                        allowAdd: true,
                        allowEdit: bool,
                        allowSelect: bool,
                      };
                    }),
                  ];
                };
                if (formModel === '子表') {
                  props.current.detailForms = [...props.current.detailForms, ...forms];
                  setFormInfos(true);
                  setDetailForms(props.current.detailForms);
                } else {
                  props.current.primaryForms = [...props.current.primaryForms, ...forms];
                  setFormInfos(false);
                  setPrimaryForms(props.current.primaryForms);
                }
              }
              setFormModel('');
            }}
          />
        )}
        {executorModal ? (
          <ExecutorConfigModal
            refresh={(param) => {
              if (param) {
                executors.push({
                  id: getUuid(),
                  trigger: param.trigger,
                  funcName: param.funcName as any,
                  changes: [],
                  hookUrl: '',
                  belongId: props.belong.id,
                  acquires: [],
                });
                setExecutors([...executors]);
                props.current.executors = executors;
              }
              setExecutorModal(false);
            }}
            current={props.current}
          />
        ) : null}
        {printModal && (
          <PrintConfigModal
            refresh={(cur) => {
              setPrintModal(false);
            }}
            print={primaryPrints}
            resource={props.current}
            work={props.work}
            printType={props.current.printData.type}
            primaryForms={primaryForms}
            detailForms={detailForms}
          />
        )}
        {printModalCreate && printLoaded && (
          <OpenFileDialog
            multiple
            title={`选择打印模板`}
            rootKey={props.work.application.key}
            currentKey={props.work.application.key}
            accepts={['打印模板']}
            fileContents={prints}
            excludeIds={primaryPrints.map((i) => i.id)}
            leftShow={false}
            rightShow={false}
            onCancel={() => setPrintModalCreate(false)}
            onOk={(files) => {
              if (files.length > 0) {
                const prints = (files as unknown[] as IPrint[]).map((i) => i.metadata);
                props.current.primaryPrints = [
                  ...(props.current.primaryPrints ?? []),
                  ...prints,
                ];
                const setPrintInfos = (bool: boolean) => {
                  props.current.print = [...(props.current.print ?? []), ...prints];
                };
                setPrintInfos(false);
                setPrimaryPrints((prevState: any) => [...prints, ...prevState]);
              }
              setPrintModalCreate(false);
            }}
          />
        )}
      </div>
    </div>
  );
};
export default RootNode;
