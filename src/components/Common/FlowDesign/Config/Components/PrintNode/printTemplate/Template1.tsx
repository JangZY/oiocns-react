import React, { FC, useEffect, useState } from 'react';
import './index.less';
import css from './designer.module.less';
import { IWorkTask } from '@/ts/core';
import orgCtrl from '@/ts/controller';
import { schema } from '@/ts/base';
import QrCode from 'qrcode.react';

interface IProps {
  printData: any;
  current: IWorkTask;
  print: any;
  loading: () => void;
  styleTemplate: string;
}

const Template1: FC<IProps> = ({ printData, current, loading, styleTemplate, print }) => {
  const [subTableFlag, setSubTableFlag] = useState(false);
  const [sameRenderType] = useState<string[]>([
    '引用型',
    '数值型',
    '货币型',
    '日期型',
    '描述型',
    '时间型',
  ]);
  const [items, setItems] = useState<any[]>([]);
  const [attributes, setAttributes] = useState<{ [key: string]: any }>({});
  const [cheChangeAttributes, setCheChangeAttributes] = useState<any[]>([]);
  const [sumAttributes, setSumAttributes] = useState<any[]>([]);
  useEffect(() => {
    //变更处理
    let changeArr: any[] = [];
    let changeAttributes: any = [];
    let updatedChangeArr: any[] = [];
    current.instanceData &&
      current.name.indexOf('变更') > -1 &&
      Object.entries(current.instanceData.data).forEach(([_key, value]) => {
        if (value[0].before.length > 0) {
          current.instanceData?.node.detailForms.forEach((item: any) => {
            if (item.id === _key) {
              changeAttributes = item.attributes;
            }
          });

          //比较after和before中的数组的id值，确认是弄的同一个数据
          value[0].before.forEach((item: any, index) => {
            value[0].after.forEach((item2: any) => {
              if (item.id === item2.id) {
                let Obj = {};
                //找到相同的数据
                for (const key in item) {
                  if (!isNaN(Number(key))) {
                    changeAttributes.forEach((att: any) => {
                      //再添加编号和资产名称
                      if (['卡片编号', '资产编号'].includes(att.name) && att.id == key) {
                        Obj = {
                          ...Obj,
                          ...{ CardID: item2[key] },
                        };
                      }
                      if (att.name == '资产名称' && att.id == key) {
                        Obj = {
                          ...Obj,
                          ...{ IDName: item2[key] },
                        };
                      }
                    });
                    if (item[key] !== item2[key]) {
                      changeArr.push({
                        ...Obj,
                        key,
                        before: item[key],
                        after: item2[key],
                      });
                    }
                  }
                }
              }
            });
          });
          updatedChangeArr = changeArr.map((item: any, index) => {
            changeAttributes.forEach((item2: any) => {
              if (item.key === item2.id) {
                item = {
                  ...item,
                  ...item2,
                };
              }
            });
            return item;
          });
          setCheChangeAttributes(updatedChangeArr);
        }
      });
    //打印样式和数据源处理
    print &&
      print.forEach((item: any) => {
        if (item.id === printData.type) {
          let newItems = item.table;
          setItems(newItems);
          let newAttributes: { [key: string]: any } = {};
          printData.attributes.forEach((item: any) => {
            if (item.title === printData.type) {
              newAttributes = item;
            }
          });
          Object.entries(newAttributes).forEach(([key, value]) => {
            if (value && value.typeName == '表单') {
              setSubTableFlag(true);
            }
            if (key.includes('sum')) {
              //分类合计
              //拿到分类合计，找到这个合计中的分类项目
              let sumArr: any = [];
              let sumInfo = '';
              let sumIDS: any = [];
              let treeData: any = [];
              value.attributes.forEach((item: any) => {
                sumIDS.push(item.id);
                if (item.valueType == '分类型') {
                  sumInfo = item.id;
                  //找到分类项目
                  treeData = buildTree(item.lookups, undefined, 1); //转化成树结构
                  //拿到主表的信息，把提交上来的表单数据去掉主表，剩下的就是子表的提交数据
                  let primaryIDs: any = [];
                  current.instanceData?.node.primaryForms.forEach((cur: any) => {
                    primaryIDs.push(cur.id);
                  });
                  //过滤掉主表的数据,拿到子表的提交数据
                  Object.entries(current.instanceData!.data).forEach(([_obj, data]) => {
                    if (!primaryIDs.includes(_obj)) {
                      //不是主表的东西
                      sumArr.push({ detail: data });
                    }
                  });
                }
              });
              let sumIDSInfos: any = [];
              //拿到这个分类项目，找到需要合计的层级
              sumArr.forEach((item: any) => {
                if (item.detail.length > 0) {
                  item.detail[0].after.forEach((item2: any, index: number) => {
                    if (item2[sumInfo]) {
                      let sumObjArr: any = [];
                      sumIDS.forEach((item3: any) => {
                        if (item3 == 3) {
                          sumObjArr.push({
                            num: 1,
                          });
                        }
                        if (item3 != sumInfo && item3 != 1 && item3 != 3) {
                          sumObjArr.push({
                            [item3]: item2[item3],
                          });
                        }
                        if (item3 == sumInfo) {
                          const text = findParentNodeByIdAndLevel(
                            treeData,
                            item2[item3],
                            value.level,
                          );
                          sumObjArr.push({
                            mainKey: {
                              id: text && text.id,
                              name: text && text.text,
                            },
                          });
                        }
                      });
                      sumIDSInfos.push(sumObjArr);
                    }
                  });
                }
              });
              const Data = mergeData(sumIDSInfos);
              setSumAttributes(Data);
            }
          });
          setAttributes(newAttributes);
        }
      });
  }, []);
  const mergeData = (data: any[][]): any[] => {
    const mergedData = data.reduce((acc: any, item: any) => {
      const id = item[0].mainKey.id;
      if (!acc[id]) {
        acc[id] = {
          ...item[0], // mainKey 对象
          num: 0,
          values: {},
        };
      }

      acc[id].num += item[1].num;

      for (let i = 2; i < item.length; i++) {
        const key = Object.keys(item[i])[0];
        if (!acc[id].values[key]) {
          acc[id].values[key] = 0;
        }
        acc[id].values[key] += item[i][key];
      }

      return acc;
    }, {});
    return Object.values(mergedData).map((item: any) => {
      const { values, ...rest } = item;
      return {
        ...rest,
        ...values,
      };
    });
  };
  const Tab: FC<any> = ({ value }) => {
    const attrsObj = attributes[value];
    if (!attrsObj) {
      return <span></span>;
    }
    const [userDetail, setUserDetail] = useState<{ [key: string]: string }>({});
    const type = attrsObj['valueType'];
    const curData = current.instanceData?.data || [];
    const attrXfieldId: string = attrsObj['xfield']?.name.split('-')[0];
    const fieldAfter = curData[attrXfieldId] ? curData[attrXfieldId][0]?.after : [];
    const fieldValue = fieldAfter[0]?.[attrsObj['id']];
    useEffect(() => {
      if (type === '用户型') {
        (async () => {
          const res = orgCtrl.user.findMetadata<schema.XEntity>(fieldValue);
          setUserDetail({ ...userDetail, [fieldValue]: res?.name });
        })();
      }
    }, []);
    if (~sameRenderType.indexOf(type)) {
      if (attrsObj.valueType === '描述型' && attrsObj.id === '111') {
        //这里后期要修改，要跟节点审批人之类的这些相互关联，目前就直接是一个流程节点的审批，然后放到这
        //tasks应该是节点的东西，records应该是审批人的列表反馈，这个后面再优化吧。
        if (
          current.instance &&
          current.instance.tasks &&
          current.instance.tasks.length > 0 &&
          current.instance.tasks[0].records &&
          current.instance.tasks[0].records?.length > 0 &&
          current.instance.tasks[0].records[0].comment
        ) {
          return <span>{current.instance.tasks[0].records[0].comment}</span>;
        }
        return <span></span>;
      }
      return <span>{fieldValue}</span>;
    }
    if (type === '选择型') {
      return (
        <div style={{ display: 'flex', flexWrap: 'wrap' }}>
          {attrsObj.lookups
            .filter((item: any) => !item.hide)
            .map((lookup: any) => (
              <label
                className="print-checkbox-label"
                key={lookup.id}
                style={{ marginRight: '30px', display: 'flex', alignItems: 'center' }}>
                <input
                  type="checkbox"
                  value={lookup.value}
                  checked={
                    attrsObj.widget == '多选框'
                      ? current.instanceData!.data[
                          attrsObj.xfield.name.split('-')[0]
                        ][0].after?.[0]?.[attrsObj.id].includes(lookup.value)
                      : current.instanceData!.data[attrsObj.xfield.name.split('-')[0]][0]
                          .after?.[0]?.[attrsObj.id] === lookup.value
                  }
                />
                <span style={{ verticalAlign: 'middle', marginLeft: '8px' }}>
                  {lookup.text}
                </span>
              </label>
            ))}
        </div>
      );
    } else if (type === '分类型') {
      let flText = '';
      attrsObj.lookups.forEach((item: any) => {
        if (
          item.value ==
          current.instanceData!.data[attrsObj.xfield.name.split('-')[0]][0].after?.[0]?.[
            attrsObj.id
          ]
        ) {
          flText = item.text;
        }
      });
      return <span>{flText}</span>;
    } else if (type === '用户型') {
      return <span>{userDetail[fieldValue]}</span>;
    }
  };
  const buildTree = (items: any[], parentId: any, level: number): any[] => {
    const tree: any[] = [];
    items.forEach((item) => {
      if (item.parentId === parentId) {
        const children = buildTree(items, item.id, level + 1);
        const node = { ...item, level, children };
        tree.push(node);
      }
    });
    return tree;
  };
  const findParentNodeByIdAndLevel = (
    tree,
    selectedId,
    targetLevel,
    currentLevel = 1,
  ) => {
    for (let node of tree) {
      if (node.value == selectedId) {
        return node;
      } else {
        if (
          currentLevel === targetLevel &&
          node.children &&
          node.children.some((child: any) => child.value === selectedId)
        ) {
          return node;
        }
        if (node.children.length > 0) {
          const parentNode: any = findParentNodeByIdAndLevel(
            node.children,
            selectedId,
            targetLevel,
            currentLevel + 1,
          );
          if (parentNode) {
            return parentNode;
          }
        }
      }
    }
    return null;
  };
  const SubTab: FC<any> = ({ id, valueType, value, checkKey }) => {
    const [userDetail, setUserDetail] = useState<{ [key: string]: string }>({});
    useEffect(() => {
      if (valueType === '用户型') {
        (async () => {
          const res = orgCtrl.user.findMetadata<schema.XEntity>(value);
          setUserDetail({ ...userDetail, [value]: res?.name });
        })();
      }
    }, []);
    const attributesData = attributes[id].attributes;
    const attrsObj = attributesData.filter((item: any) => item.id == checkKey)[0];
    if (valueType == '选择型') {
      const lookups = attrsObj.checkTrue
        ? attrsObj.lookups.filter((item: any) =>
            attrsObj.widget == '多选框'
              ? value.includes(item.value)
              : value === item.value,
          )
        : attrsObj.lookups;
      return (
        <div style={{ display: 'flex', flexWrap: 'wrap', justifyContent: 'center' }}>
          {lookups
            .filter((item: any) => !item.hide)
            .map((lookup: any) => (
              <label
                className="print-checkbox-label"
                key={lookup.id}
                style={{ marginRight: '30px', display: 'flex', alignItems: 'center' }}>
                <input
                  type="checkbox"
                  value={lookup.value}
                  checked={
                    attrsObj.widget == '多选框'
                      ? value.includes(lookup.value)
                      : value === lookup.value
                  }
                />
                <span style={{ verticalAlign: 'middle', marginLeft: '8px' }}>
                  {lookup.text}
                </span>
              </label>
            ))}
        </div>
      );
    } else if (valueType == '分类型') {
      if (!attrsObj.parentCheck) {
        let flText = '';
        const lookups = attrsObj.lookups;
        lookups.forEach((item: any) => {
          if (item.value == value) {
            flText = item.text;
          }
        });
        return <span>{flText}</span>;
      } else {
        const treeData = buildTree(attrsObj.lookups, undefined, 1);
        const parentNode = findParentNodeByIdAndLevel(
          treeData,
          value,
          attrsObj.parentCheck,
        );
        return <span>{parentNode.text}</span>;
      }
    } else if (valueType == '用户型') {
      return <span>{userDetail[value]}</span>;
    }
    return <span></span>;
  };
  useEffect(() => {
    // 添加一个延时来查看下一次渲染时的load值
    //要分辨有没有子表，没有子表的话，加载这个loading方法
    if (!subTableFlag && loading) {
      loading();
    }
  }, [subTableFlag]);
  return (
    <html>
      <head>
        <style>{styleTemplate}</style>
      </head>
      <body>
        <div className="printSection">
          {items.map((item, index) => {
            return (
              <div
                className={index !== 0 ? 'ContentTitle' : ''}
                key={index - 200}
                style={{ position: 'relative' }}>
                {item.qrcode &&
                  item.qrcode!.map((qcitem: any, qcindex: number) => {
                    return (
                      <div
                        className={'qccode'}
                        key={qcindex - 100000}
                        style={{
                          position: 'absolute',
                          top: qcitem.style.top - qcitem.style.size / 2 + 'px',
                          left: qcitem.style.left - qcitem.style.size / 2 + 'px',
                        }}>
                        <QrCode
                          level="H"
                          size={qcitem.style.size}
                          renderAs="canvas"
                          value={''}
                          data-index={-888}
                        />
                      </div>
                    );
                  })}
                {item.title && item.title.flag && (
                  <div className={'head'}>
                    <div className={'Table-title'} style={item.title.style}>
                      {item.title.name}
                    </div>
                  </div>
                )}
                {item.subtitle && item.subtitle.flag && (
                  <table
                    style={{ width: '100%' }}
                    className={css.printSubtitleContent}
                    cellSpacing=""
                    cellPadding="10">
                    <tbody>
                      <tr>
                        <td style={item.subtitle.style} className={'subtitle-container'}>
                          <span
                            className="tableData"
                            data-index={-888}
                            dangerouslySetInnerHTML={{ __html: item.subtitle.name }}
                            style={{ whiteSpace: 'pre-wrap' }}
                          />
                          {item.subtitle.dataSource && (
                            <Tab value={`attr${index}_subtitle`} />
                          )}
                          {item.subtitle.text &&
                            item.subtitle.text.length &&
                            item.subtitle.text.map(
                              (subtitleItem: any, subtitleIndex: number) => (
                                <div
                                  key={subtitleIndex + 100000}
                                  className={css.textDiv}
                                  style={subtitleItem.style}>
                                  <span
                                    data-index={-888}
                                    style={{ whiteSpace: 'pre-wrap' }}
                                    dangerouslySetInnerHTML={{
                                      __html: subtitleItem.name,
                                    }}
                                  />
                                  {subtitleItem.dataSource && (
                                    <Tab
                                      value={`attr${index}_subText_${subtitleIndex}`}
                                    />
                                  )}
                                </div>
                              ),
                            )}
                        </td>
                      </tr>
                    </tbody>
                  </table>
                )}
                {cheChangeAttributes.length > 0 && index == 0 && (
                  <div
                    style={{
                      width: '100%',
                      display: 'flex',
                      flexDirection: 'column',
                      fontSize: '14px',
                    }}>
                    <div style={{ display: 'flex', width: '100%', height: '50px' }}>
                      <div className="table-cell">序号</div>
                      <div className="table-cell">卡片编号</div>
                      <div className="table-cell">资产名称</div>
                      <div className="table-cell">变更项</div>
                      <div className="table-cell">变更前值</div>
                      <div className="table-cell">变更后值</div>
                    </div>
                    {cheChangeAttributes.map((changeItem: any, changeIndex: number) => (
                      <div
                        style={{ display: 'flex', width: '100%', height: '50px' }}
                        key={changeIndex + 30280}>
                        <div
                          className="table-cell"
                          style={
                            changeIndex == cheChangeAttributes.length - 1
                              ? { borderBottom: 'none' }
                              : {}
                          }>
                          {changeIndex + 1}
                        </div>
                        <div
                          className="table-cell"
                          style={
                            changeIndex == cheChangeAttributes.length - 1
                              ? { borderBottom: 'none' }
                              : {}
                          }>
                          {changeItem.CardID}
                        </div>
                        <div
                          className="table-cell"
                          style={
                            changeIndex == cheChangeAttributes.length - 1
                              ? { borderBottom: 'none' }
                              : {}
                          }>
                          {changeItem.IDName}
                        </div>
                        <div
                          className="table-cell"
                          style={
                            changeIndex == cheChangeAttributes.length - 1
                              ? { borderBottom: 'none' }
                              : {}
                          }>
                          {changeItem.name}
                        </div>
                        <div
                          className="table-cell"
                          style={
                            changeIndex == cheChangeAttributes.length - 1
                              ? { borderBottom: 'none' }
                              : {}
                          }>
                          {changeItem.before}
                        </div>
                        <div
                          className="table-cell"
                          style={
                            changeIndex == cheChangeAttributes.length - 1
                              ? { borderBottom: 'none' }
                              : {}
                          }>
                          {changeItem.after}
                        </div>
                      </div>
                    ))}
                  </div>
                )}
                <table
                  style={{ width: '100%' }}
                  className={'printContent'}
                  border={1}
                  cellSpacing=""
                  cellPadding="10">
                  <tbody style={item.style}>
                    {item.data.map((item2, index2) => (
                      <>
                        <tr key={index2} data-index={index2} className={'dynamic-row'}>
                          {item2.data.data &&
                            item2.data.type != 'table' &&
                            item2.data.type != 'sum' &&
                            item2.data.data.map((item3: any, index3: number) => (
                              <td
                                key={index3 + 1000}
                                colSpan={item3.colNumber}
                                data-index={index3}
                                className={'tableData'}
                                style={item3.style}>
                                <span
                                  dangerouslySetInnerHTML={{ __html: item3.name }}
                                  style={{ whiteSpace: 'pre-wrap' }}
                                />
                                {item3.dataSource && (
                                  <Tab value={`attr${index}_${index2}_${index3}`} />
                                )}
                                {item3.text &&
                                  item3.text.length &&
                                  item3.text.map((item4: any, index4: number) => (
                                    <div
                                      key={index4 + 100000}
                                      className={css.textDiv}
                                      style={item4.style}>
                                      <span
                                        dangerouslySetInnerHTML={{ __html: item4.name }}
                                        style={{ whiteSpace: 'pre-wrap' }}
                                      />
                                      {item4.dataSource && (
                                        <Tab
                                          value={`attr${index}_${index2}_${index3}_${index4}`}
                                        />
                                      )}
                                    </div>
                                  ))}
                              </td>
                            ))}
                          {item2.data.emptydata &&
                            item2.data.emptydata.length > 0 &&
                            item2.data.emptydata.map((_item3: any, index3: number) => {
                              return (
                                <td
                                  key={index3 + 2000}
                                  colSpan={1}
                                  data-index={index3}
                                  className="empty">
                                  <span data-index={-888}></span>
                                </td>
                              );
                            })}
                        </tr>
                        {item2.data.type == 'sum' &&
                          attributes[`attr${index}_${index2}_sum`] &&
                          attributes[`attr${index}_${index2}_sum`].attributes.length >
                            0 && (
                            <tr style={{ border: 'none', width: '100%' }}>
                              <td
                                colSpan={item2.data.data[0].colNumber}
                                style={{ padding: 0, border: 0, width: '100%' }}>
                                {attributes[`attr${index}_${index2}_sum`] &&
                                  attributes[`attr${index}_${index2}_sum`].attributes
                                    .length == 0 && <div></div>}
                                {attributes[`attr${index}_${index2}_sum`] &&
                                  attributes[`attr${index}_${index2}_sum`].attributes
                                    .length > 0 && (
                                    <table style={{ width: '100%' }}>
                                      <tbody>
                                        <tr key={index2 + 19823}>
                                          {attributes[`attr${index}_${index2}_sum`] &&
                                            attributes[
                                              `attr${index}_${index2}_sum`
                                            ].attributes.map(
                                              (
                                                subdataitem: any,
                                                subdataindex: number,
                                              ) => (
                                                <th
                                                  key={subdataindex + 100000}
                                                  style={{
                                                    width: `${subdataitem.width}%`,
                                                    height: `${
                                                      subdataitem.height
                                                        ? subdataitem.height
                                                        : 50
                                                    }px`,
                                                    textAlign: 'center',
                                                    borderLeft:
                                                      subdataindex == 0
                                                        ? 'none'
                                                        : '1px solid black',
                                                    borderTop:
                                                      index2 != 0 &&
                                                      item.data[index2 - 1].data.type ==
                                                        'table'
                                                        ? '1px solid black'
                                                        : 'none',
                                                  }}>
                                                  {subdataitem.name}
                                                  {subdataitem.addonAfter &&
                                                    subdataitem.addonAfter}
                                                </th>
                                              ),
                                            )}
                                        </tr>
                                        {sumAttributes &&
                                          sumAttributes.length > 0 &&
                                          sumAttributes.map(
                                            (cur: any, curIndex: number) => (
                                              <tr key={index2 + 190823}>
                                                <td
                                                  style={{
                                                    width: `${
                                                      attributes[
                                                        `attr${index}_${index2}_sum`
                                                      ] &&
                                                      attributes[
                                                        `attr${index}_${index2}_sum`
                                                      ].attributes[curIndex].width
                                                    }%`,
                                                    height: `${
                                                      attributes[
                                                        `attr${index}_${index2}_sum`
                                                      ] &&
                                                      attributes[
                                                        `attr${index}_${index2}_sum`
                                                      ].attributes[curIndex].height
                                                        ? attributes[
                                                            `attr${index}_${index2}_sum`
                                                          ] &&
                                                          attributes[
                                                            `attr${index}_${index2}_sum`
                                                          ].attributes[curIndex].height
                                                        : 50
                                                    }px`,
                                                    textAlign: 'center',
                                                    borderTop: '1px solid black',
                                                    borderLeft: 'none',
                                                  }}>
                                                  {curIndex + 1}
                                                </td>
                                                <td
                                                  style={{
                                                    width: `${
                                                      attributes[
                                                        `attr${index}_${index2}_sum`
                                                      ] &&
                                                      attributes[
                                                        `attr${index}_${index2}_sum`
                                                      ].attributes[curIndex].width
                                                    }%`,
                                                    height: `${
                                                      attributes[
                                                        `attr${index}_${index2}_sum`
                                                      ] &&
                                                      attributes[
                                                        `attr${index}_${index2}_sum`
                                                      ].attributes[curIndex].height
                                                        ? attributes[
                                                            `attr${index}_${index2}_sum`
                                                          ] &&
                                                          attributes[
                                                            `attr${index}_${index2}_sum`
                                                          ].attributes[curIndex].height
                                                        : 50
                                                    }px`,
                                                    textAlign: 'center',
                                                    borderTop: '1px solid black',
                                                    borderLeft: '1px solid black',
                                                  }}>
                                                  {cur.mainKey.name}
                                                </td>
                                                <td
                                                  style={{
                                                    width: `${
                                                      attributes[
                                                        `attr${index}_${index2}_sum`
                                                      ] &&
                                                      attributes[
                                                        `attr${index}_${index2}_sum`
                                                      ].attributes[curIndex].width
                                                    }%`,
                                                    height: `${
                                                      attributes[
                                                        `attr${index}_${index2}_sum`
                                                      ] &&
                                                      attributes[
                                                        `attr${index}_${index2}_sum`
                                                      ].attributes[curIndex].height
                                                        ? attributes[
                                                            `attr${index}_${index2}_sum`
                                                          ] &&
                                                          attributes[
                                                            `attr${index}_${index2}_sum`
                                                          ].attributes[curIndex].height
                                                        : 50
                                                    }px`,
                                                    textAlign: 'center',
                                                    borderTop: '1px solid black',
                                                    borderLeft: '1px solid black',
                                                  }}>
                                                  {cur.num}
                                                </td>
                                                {attributes[
                                                  `attr${index}_${index2}_sum`
                                                ] &&
                                                  attributes[
                                                    `attr${index}_${index2}_sum`
                                                  ].attributes
                                                    .slice(3)
                                                    .map(
                                                      (cur2: any, curIndex2: number) => (
                                                        <td
                                                          key={curIndex2 + 92882}
                                                          colSpan={
                                                            attributes[
                                                              `attr${index}_${index2}_sum`
                                                            ] &&
                                                            attributes[
                                                              `attr${index}_${index2}_sum`
                                                            ].attributes[curIndex]
                                                              .colNumber
                                                          }
                                                          style={{
                                                            width: `${
                                                              attributes[
                                                                `attr${index}_${index2}_sum`
                                                              ] &&
                                                              attributes[
                                                                `attr${index}_${index2}_sum`
                                                              ].attributes[curIndex].width
                                                            }%`,
                                                            height: `${
                                                              attributes[
                                                                `attr${index}_${index2}_sum`
                                                              ] &&
                                                              attributes[
                                                                `attr${index}_${index2}_sum`
                                                              ].attributes[curIndex]
                                                                .height
                                                                ? attributes[
                                                                    `attr${index}_${index2}_sum`
                                                                  ] &&
                                                                  attributes[
                                                                    `attr${index}_${index2}_sum`
                                                                  ].attributes[curIndex]
                                                                    .height
                                                                : 50
                                                            }px`,
                                                            textAlign: 'center',
                                                            borderTop: '1px solid black',
                                                            borderLeft: '1px solid black',
                                                          }}>
                                                          {cur[cur2.id]}
                                                        </td>
                                                      ),
                                                    )}
                                              </tr>
                                            ),
                                          )}
                                      </tbody>
                                    </table>
                                  )}
                              </td>
                            </tr>
                          )}
                        {item2.data.type == 'table' && (
                          //子表区域
                          <tr style={{ border: 'none', width: '100%' }}>
                            <td
                              colSpan={item2.data.data[0].colNumber}
                              style={{ padding: 0, border: 0, width: '100%' }}>
                              {attributes[`attr${index}_${index2}`] &&
                                attributes[`attr${index}_${index2}`].attributes.filter(
                                  (att: any) => !att.hide,
                                ).length == 0 && <div></div>}
                              {attributes[`attr${index}_${index2}`] &&
                                attributes[`attr${index}_${index2}`].attributes.filter(
                                  (att: any) => !att.hide,
                                ).length > 0 && (
                                  <table style={{ width: '100%' }}>
                                    <tbody>
                                      <tr key={index2 + 19823}>
                                        {attributes[`attr${index}_${index2}`] &&
                                          attributes[`attr${index}_${index2}`].attributes
                                            .filter((att: any) => !att.hide)
                                            .map(
                                              (
                                                subdataitem: any,
                                                subdataindex: number,
                                              ) => (
                                                <th
                                                  key={subdataindex + 100000}
                                                  style={{
                                                    width: `${subdataitem.width}%`,
                                                    height: `${
                                                      subdataitem.height
                                                        ? subdataitem.height
                                                        : 50
                                                    }px`,
                                                    textAlign: 'center',
                                                    borderLeft:
                                                      subdataindex == 0
                                                        ? 'none'
                                                        : '1px solid black',
                                                    borderTop:
                                                      index2 != 0 &&
                                                      (item.data[index2 - 1].data.type ==
                                                        'table' ||
                                                        item.data[index2 - 1].data.type ==
                                                          'sum')
                                                        ? '1px solid black'
                                                        : 'none',
                                                  }}>
                                                  {subdataitem.name}
                                                </th>
                                              ),
                                            )}
                                      </tr>
                                      {current.instanceData?.data[
                                        attributes[`attr${index}_${index2}`]?.id
                                      ]
                                        ?.filter(
                                          (cur) =>
                                            cur.nodeId == current.instanceData?.node.id,
                                        )[0]
                                        ?.after.map((subItem: any, subIndex: number) => (
                                          <tr key={subIndex + 30080}>
                                            {attributes[`attr${index}_${index2}`] &&
                                              attributes[
                                                `attr${index}_${index2}`
                                              ].attributes
                                                .filter((item: any) => !item.hide)
                                                .map(
                                                  (
                                                    subdataitem: any,
                                                    subdataindex: number,
                                                  ) => (
                                                    <td
                                                      key={subdataindex + 50080}
                                                      style={{
                                                        width: `${subdataitem.width}%`,
                                                        height: `${
                                                          subdataitem.height
                                                            ? subdataitem.height
                                                            : 50
                                                        }px`,
                                                        textAlign: 'center',
                                                        borderLeft:
                                                          subdataindex == 0
                                                            ? 'none'
                                                            : '1px solid black',
                                                        borderTop: '1px solid black',
                                                      }}>
                                                      {sameRenderType.includes(
                                                        subdataitem.valueType,
                                                      ) ? (
                                                        <div>
                                                          {subItem[subdataitem.id]}
                                                        </div>
                                                      ) : subItem[subdataitem.id] ? (
                                                        <SubTab
                                                          id={`attr${index}_${index2}`}
                                                          checkKey={subdataitem.id}
                                                          value={subItem[subdataitem.id]}
                                                          valueType={
                                                            subdataitem.valueType
                                                          }
                                                          key={subdataindex}
                                                        />
                                                      ) : (
                                                        <span></span>
                                                      )}
                                                    </td>
                                                  ),
                                                )}
                                          </tr>
                                        ))}
                                    </tbody>
                                  </table>
                                )}
                            </td>
                          </tr>
                        )}
                      </>
                    ))}
                  </tbody>
                </table>
                {item.footer && item.footer.flag && (
                  <div
                    className="footer-container-div"
                    style={item.footer.style}
                    dangerouslySetInnerHTML={{ __html: item.footer.name }}></div>
                )}
              </div>
            );
          })}
        </div>
      </body>
    </html>
  );
};
export default Template1;
