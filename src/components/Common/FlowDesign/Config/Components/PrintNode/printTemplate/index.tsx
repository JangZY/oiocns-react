import React, { FC } from 'react';
import './index.less';
import { styleTemplate } from './style';
import { IWorkTask } from '@/ts/core';
import Template1 from './Template1';

interface IProps {
  printData: any;
  print: any;
  current: IWorkTask;
  loading: () => void;
}
const Template: FC<IProps> = ({ printData, current, loading, print }) => {
  return (
    <>
      <Template1
        printData={printData}
        print={print}
        current={current}
        loading={loading}
        styleTemplate={styleTemplate}
      />
    </>
  );
};
export default Template;
