import React, { FC, useState, useEffect } from 'react';
import FullScreenModal from '@/components/Common/fullScreen';
import PrintTemplate from '../PrintNode/Template';
import { IPrint, IWork, IWorkTask } from '@/ts/core';
import { schema } from '@/ts/base';

interface IProps {
  refresh: (cur: any) => void;
  printType: string;
  print: any;
  work: IWork | IWorkTask;
  primaryForms: schema.XForm[];
  detailForms: schema.XForm[];
  resource: any;
  type?: string;
}

const ConfigModal: FC<IProps> = (props) => {
  const [print2, setPrint2] = useState<IPrint>();
  useEffect(() => {
    props.print &&
      props.print.forEach((item: any) => {
        if (item.id === props.printType) {
          const print = item as IPrint;
          setPrint2(print);
        }
      });
  }, []);
  return (
    <FullScreenModal
      open
      title={'打印模板配置'}
      onCancel={() => props.refresh(props.resource)}
      destroyOnClose
      width={'80vw'}
      bodyHeight={'70vh'}>
      <>
        <PrintTemplate
          resource={props.resource}
          printType={props.printType}
          work={props.work}
          primaryForms={props.primaryForms}
          detailForms={props.detailForms}
          print={print2 as IPrint}
          type={props.type}
        />
      </>
    </FullScreenModal>
  );
};

export default ConfigModal;
