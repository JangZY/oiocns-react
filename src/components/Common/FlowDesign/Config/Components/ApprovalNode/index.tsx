import React, { useEffect, useState } from 'react';
import { AiOutlineUser } from 'react-icons/ai';
import { Col, Radio, Form, InputNumber, Card, Divider } from 'antd';
import cls from './index.module.less';
import { WorkNodeDisplayModel } from '@/utils/work';
import ShareShowComp, { FormOption } from '@/components/Common/ShareShowComp';
import { IBelong, IWork, IPrint } from '@/ts/core';
import SelectIdentity from '@/components/Common/SelectIdentity';
import { command, model, schema } from '@/ts/base';
import { IForm, Form as SForm } from '@/ts/core/thing/standard/form';
import OpenFileDialog from '@/components/OpenFileDialog';
import { SelectBox } from 'devextreme-react';
import { getUuid } from '@/utils/tools';
import Rule from '../../Rule';
import ExecutorConfigModal from './configModal';
import ExecutorShowComp from '@/components/Common/ExecutorShowComp';
import useAsyncLoad from '@/hooks/useAsyncLoad';
import { CloseOutlined } from '@ant-design/icons';
import PrintConfigModal from '../PrintNode/PrintModal';
import orgCtrl from '@/ts/controller';
interface IProps {
  work: IWork;
  current: WorkNodeDisplayModel;
  belong: IBelong;
  refresh: () => void;
}

/**
 * @description: 审批对象
 * @return {*}
 */

const ApprovalNode: React.FC<IProps> = (props) => {
  const [executors, setExecutors] = useState<model.Executor[]>([]);
  const [openType, setOpenType] = useState<string>(''); // 打开弹窗
  const [primaryForms, setPrimaryForms] = useState<schema.XForm[]>([]);
  const [radioValue, setRadioValue] = useState(1);
  const [destType, setDestType] = useState<string>();
  const [currentData, setCurrentData] = useState<{ id: string; name: string }>();
  const [executorModal, setExecutorModal] = useState(false);
  const [primaryPrints, setPrimaryPrints] = useState(props.current.print ?? []);
  const [printType, setPrintType] = useState<string>(
    (props.current.printData && props.current.printData.type) ?? '默认无',
  );
  useEffect(() => {
    props.current.primaryForms = props.current.primaryForms || [];
    props.current.executors = props.current.executors || [];
    setDestType(props.current.destType ?? '身份');
    setExecutors(props.current.executors);
    setPrimaryForms(props.current.primaryForms);
    setRadioValue(props.current.num == 0 ? 1 : 2);
    setCurrentData({
      id: props.current.destId,
      name: props.current.destName,
    });
  }, [props.current]);
  const [formLoaded, forms] = useAsyncLoad(async () => {
    return await props.work.application.loadAllForms();
  });
  const [printLoaded, prints] = useAsyncLoad(async () => {
    return await props.work.application.loadAllPrint();
  });
  useEffect(() => {
    const fetchData = async () => {
      const newData: any = [];
      try {
        const IDsIprint = await orgCtrl.loadAllDefaultIPrint(props.work.belongId);
        const IPrints = await orgCtrl.loadAllIPrint();
        IPrints.forEach((item) => {
          if (
            props.work.belongId == item.belongId &&
            !item.groupTags.includes('已删除')
          ) {
            if (
              item.path.length > 0 &&
              item.path[1] == IDsIprint[0] &&
              item.path[2] == IDsIprint[1]
            ) {
              newData.push(item);
            }
          }
        });
        const newPrimaryPrint = primaryPrints.filter(
          (obj1) => !newData.some((obj2: any) => obj1.id === obj2.id),
        );
        setPrimaryPrints([
          { id: '默认无', name: '默认无', table: '默认无' },
          ...newPrimaryPrint,
          ...newData,
        ]);
      } catch (error) {
        console.error('Error fetching data:', error);
      }
    };
    fetchData();
  }, []);
  useEffect(() => {
    if (primaryPrints.length) {
      prints?.forEach((item) => {
        primaryPrints.forEach((item2) => {
          if (item.id == item2.id) {
            item2.table = item.table;
            item2.name = item.name;
          }
        });
      });
      let newPrintData = primaryPrints.filter((item) => item.id == '默认无');
      if (newPrintData.length == 0) {
        setPrimaryPrints([
          { id: '默认无', name: '默认无', table: '默认无' },
          ...primaryPrints,
        ]);
      }
    }
  }, [printLoaded]);
  const formViewer = React.useCallback((form: schema.XForm) => {
    command.emitter(
      'executor',
      'open',
      new SForm({ ...form, id: '_' + form.id }, props.belong.directory),
      'preview',
    );
  }, []);
  const handleRemoveItem = (e, data) => {
    e.stopPropagation();
    if (data.id == '默认无') {
      return false;
    }
    const newPrintData = props.current.print.filter((option) => option.id !== data.id);
    const newPrintData2 = props.current.printData.attributes.filter(
      (option) => option.title !== data.id,
    );
    props.current.print = newPrintData;
    setPrimaryPrints([...newPrintData]);
    props.current.printData.attributes = newPrintData2;
  };
  const loadDestType = () => {
    switch (destType) {
      case '身份': {
        return (
          <>
            <Divider />
            <div className={cls['roval-node-select']}>
              <Col className={cls['roval-node-select-col']}></Col>
              <Radio.Group
                onChange={(e) => {
                  if (e.target.value == 1) {
                    props.current.num = 0;
                  } else {
                    props.current.num = 1;
                  }
                  setRadioValue(e.target.value);
                }}
                style={{ paddingBottom: '10px' }}
                value={radioValue}>
                <Radio value={1} style={{ width: '100%' }}>
                  全部: 需征得该角色下所有人员同意
                </Radio>
                <Radio value={2}>部分会签: 指定审批该节点的人员的数量</Radio>
              </Radio.Group>
              {radioValue === 2 && (
                <Form.Item label="会签人数">
                  <InputNumber
                    min={1}
                    onChange={(e: number | null) => {
                      props.current.num = e ?? 1;
                    }}
                    value={props.current.num}
                    placeholder="请设置会签人数"
                    addonBefore={<AiOutlineUser />}
                    style={{ width: '60%' }}
                  />
                </Form.Item>
              )}
            </div>
          </>
        );
      }
      case '发起人':
        return <a>发起人</a>;
      default:
        return <></>;
    }
  };

  const loadDialog = () => {
    switch (openType) {
      case '身份':
        return (
          <SelectIdentity
            open={openType == '身份'}
            exclude={[]}
            multiple={false}
            space={props.belong}
            finished={(selected) => {
              if (selected.length > 0) {
                const item = selected[0];
                props.current.destType = '身份';
                props.current.destId = item.id;
                props.current.destName = item.name;
                setCurrentData(item);
                props.refresh();
              }
              setOpenType('');
            }}
          />
        );
      case '其他办事':
        return (
          <OpenFileDialog
            title={'选中其它办事'}
            rootKey={'disk'}
            accepts={['办事']}
            allowInherited
            excludeIds={[props.work.id]}
            onCancel={() => setOpenType('')}
            onOk={(files) => {
              if (files.length > 0) {
                const work = files[0] as IWork;
                let name = `${work.name} [${work.directory.target.name}]`;
                props.current.destId = work.id;
                props.current.destName = name;
                setCurrentData({ id: work.id, name: name });
              } else {
                setCurrentData({
                  id: '',
                  name: '',
                });
              }
              setOpenType('');
              props.refresh();
            }}
          />
        );
      case '主表': {
        return (
          <>
            {formLoaded && (
              <OpenFileDialog
                multiple
                title={`选择${openType}表单`}
                rootKey={props.work.application.key}
                accepts={['表单', '报表']}
                fileContents={forms}
                excludeIds={primaryForms.map((i) => i.id)}
                leftShow={false}
                rightShow={false}
                onCancel={() => setOpenType('')}
                onOk={(files) => {
                  if (files.length > 0) {
                    const forms = (files as unknown[] as IForm[]).map((i) => i.metadata);
                    props.current.primaryForms.push(...forms);
                    setPrimaryForms(props.current.primaryForms);
                    props.current.forms = [
                      ...(props.current.forms ?? []),
                      ...forms.map((item) => {
                        return {
                          id: item.id,
                          typeName: openType,
                          allowAdd: false,
                          allowEdit: false,
                          allowSelect: false,
                        };
                      }),
                    ];
                  }
                  setOpenType('');
                }}
              />
            )}
          </>
        );
      }
      case '打印模板添加': {
        return (
          <>
            {printLoaded && (
              <OpenFileDialog
                multiple
                title={`选择打印模板`}
                rootKey={props.work.application.key}
                currentKey={props.work.application.key}
                accepts={['打印模板']}
                fileContents={prints}
                excludeIds={primaryPrints.map((i) => i.id)}
                leftShow={false}
                rightShow={false}
                onCancel={() => setOpenType('')}
                onOk={(files) => {
                  if (files.length > 0) {
                    const prints = (files as unknown[] as IPrint[]).map(
                      (i) => i.metadata,
                    );
                    props.current.primaryPrints = [
                      ...(props.current.primaryPrints ?? []),
                      ...prints,
                    ];
                    const setPrintInfos = (bool: boolean) => {
                      props.current.print = [...(props.current.print ?? []), ...prints];
                    };
                    setPrintInfos(false);
                    setPrimaryPrints((prevState: any) => [...prints, ...prevState]);
                  }
                  setOpenType('');
                }}
              />
            )}
          </>
        );
      }
      case '打印模板设置': {
        return (
          <>
            <PrintConfigModal
              refresh={() => {
                setOpenType('');
              }}
              current={props.current}
              work={props.work}
              printType={props.current.printData.type}
              primaryForms={primaryForms}
              detailForms={[]}
              type={'workNode'}
            />
          </>
        );
      }
      default:
        return <></>;
    }
  };

  const loadExecutor = () => {
    return (
      <>
        <Card
          type="inner"
          title={
            <div>
              <Divider type="vertical" className={cls['divider']} />
              <span>执行器配置</span>
            </div>
          }
          className={cls[`card-info`]}
          bodyStyle={{ padding: '12px' }}
          extra={
            <>
              <a
                type="link"
                onClick={() => {
                  setExecutorModal(true);
                }}>
                添加
              </a>
            </>
          }>
          {executors && executors.length > 0 && (
            <span>
              <ExecutorShowComp
                work={props.work}
                executors={executors}
                deleteFuc={(id: string) => {
                  const exes = executors.filter((a) => a.id != id);
                  setExecutors(exes);
                  props.current.executors = exes;
                }}
              />
            </span>
          )}
        </Card>
        {executorModal && (
          <ExecutorConfigModal
            refresh={(param) => {
              if (param) {
                executors.push({
                  id: getUuid(),
                  trigger: param.trigger,
                  funcName: param.funcName as any,
                  changes: [],
                  hookUrl: '',
                  belongId: props.belong.id,
                  acquires: [],
                });
                setExecutors([...executors]);
                props.current.executors = executors;
              }
              setExecutorModal(false);
            }}
            current={props.current}
          />
        )}
      </>
    );
  };

  return (
    <div className={cls[`app-roval-node`]}>
      <div className={cls[`roval-node`]}>
        <Card
          type="inner"
          title={
            <div>
              <Divider type="vertical" className={cls['divider']} />
              <span>审批对象</span>
            </div>
          }
          className={cls[`card-info`]}
          extra={
            <>
              <SelectBox
                value={destType}
                valueExpr={'value'}
                displayExpr={'label'}
                style={{ width: 120, display: 'inline-block' }}
                onSelectionChanged={(e) => {
                  switch (e.selectedItem.value) {
                    case '身份':
                      props.current.destType = '身份';
                      setCurrentData(undefined);
                      break;
                    case '其他办事':
                      props.current.destType = '其他办事';
                      setCurrentData(undefined);
                      break;
                    case '发起人':
                      props.current.num = 1;
                      props.current.destId = '1';
                      props.current.destName = '发起人';
                      props.current.destType = '发起人';
                      setCurrentData({ id: '1', name: '发起人' });
                      break;
                    default:
                      break;
                  }
                  if (destType != e.selectedItem.value) {
                    setDestType(e.selectedItem.value);
                    props.refresh();
                  }
                }}
                dataSource={[
                  { value: '身份', label: '指定角色' },
                  { value: '其他办事', label: '其他办事' },
                  { value: '发起人', label: '发起人' },
                ]}
              />
              {destType && destType !== '发起人' && (
                <a
                  style={{ paddingLeft: 10, display: 'inline-block' }}
                  onClick={() => {
                    setOpenType(destType);
                  }}>
                  {`+ 选择${destType === '身份' ? '角色' : destType}`}
                </a>
              )}
            </>
          }>
          {currentData && currentData.id != '1' && (
            <ShareShowComp
              key={'审批对象'}
              departData={[currentData]}
              deleteFuc={(_) => {
                props.current.destId = '';
                props.current.destName = '';
                setCurrentData(undefined);
                props.refresh();
              }}
            />
          )}
          {loadDestType()}
        </Card>
        <Card
          type="inner"
          title={
            <div>
              <Divider type="vertical" className={cls['divider']} />
              <span>主表配置</span>
            </div>
          }
          className={cls[`card-info`]}
          extra={
            <a
              onClick={() => {
                setOpenType('主表');
              }}>
              添加
            </a>
          }>
          {primaryForms.length > 0 && (
            <span>
              <ShareShowComp
                key={'表单'}
                departData={primaryForms}
                onClick={formViewer}
                deleteFuc={(id: string) => {
                  props.current.primaryForms = primaryForms.filter((a) => a.id != id);
                  props.current.forms = props.current.forms.filter((a) => a.id != id);
                  setPrimaryForms(props.current.primaryForms);
                }}
                tags={(id) => {
                  const info = props.current.forms.find((a) => a.id == id);
                  if (info) {
                    return <FormOption operateRule={info} typeName="主表" />;
                  }
                }}
              />
            </span>
          )}
        </Card>
        {loadExecutor()}
        {loadDialog()}
        <Rule
          work={props.work}
          current={props.current}
          primaryForms={primaryForms}
          detailForms={[]}
        />
        <Card
          type="inner"
          title={
            <div>
              <Divider type="vertical" className={cls['divider']} />
              <span>打印模板设置</span>
            </div>
          }
          className={cls['card-info']}
          extra={
            <>
              <a
                onClick={() => {
                  setOpenType('打印模板添加');
                }}>
                添加
              </a>
            </>
          }>
          <SelectBox
            showClearButton
            value={printType}
            placeholder="请选择打印模板"
            dataSource={primaryPrints}
            displayExpr={'name'}
            valueExpr={'id'}
            onFocusIn={() => {
              setPrintType('');
            }}
            onValueChange={(e) => {
              props.current.printData
                ? (props.current.printData.type = e)
                : (props.current.printData = { attributes: [], type: e });
              setPrintType(e);
              if (e == '默认无') {
                setOpenType('');
              } else if (e == null) {
                setOpenType('');
              } else {
                setOpenType('打印模板设置');
              }
            }}
            itemRender={(data) => (
              <div style={{ display: 'flex', justifyContent: 'space-between' }}>
                <span style={{ whiteSpace: 'nowrap' }}>{data.name}</span>
                <CloseOutlined onClick={(e) => handleRemoveItem(e, data)} />
              </div>
            )}
          />
        </Card>
      </div>
    </div>
  );
};
export default ApprovalNode;
