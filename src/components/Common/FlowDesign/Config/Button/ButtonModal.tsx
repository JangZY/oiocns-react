import { schema } from '@/ts/base';
import { Executor, Rule, WorkNodeButton } from '@/ts/base/model';
import { Form, Input, Modal, Select, Space } from 'antd';
import React, { useEffect, useMemo, useState } from 'react';

interface IProps {
  current: WorkNodeButton;
  onOk: (row: WorkNodeButton) => void;
  onCancel: () => void;
  rules: Rule[];
  executors: Executor[];
}

const ButtonModal: React.FC<IProps> = (props) => {
  const [form] = Form.useForm();

  const rules = useMemo(() => {
    return props.rules.filter(r => r.isManual);
  }, [props.rules]);

  const executors = useMemo(() => {
    return props.executors.filter(r => r.trigger == 'manual');
  }, [props.executors]);

  const [type, setType] = useState(props.current.type);
  useEffect(() => {
    form.setFieldsValue(props.current);
  }, [props.current])
  return (
    <Modal
      destroyOnClose
      title="按钮配置"
      width={480}
      open={true}
      bodyStyle={{ border: 'none', padding: 0, marginLeft: '32px', marginRight: '32px' }}
      onOk={() => {
        Object.assign(props.current, form.getFieldsValue());
        props.onOk(props.current);
      }}
      onCancel={props.onCancel}>
      <Form preserve={false} layout="vertical" 
        form={form}>
        <Form.Item label="标识" name="code" required>
          <Input />
        </Form.Item>
        <Form.Item label="按钮文字" name="name" required>
          <Input />
        </Form.Item>
        <Form.Item label="操作目标" name="type" required >
          <Select options={[
            { label: '手动规则', value: 'rule' },
            { label: '手动执行器', value: 'executor' },
          ]} onSelect={setType}/>
        </Form.Item>
        {type=='rule' ? (
          <Form.Item label="规则" name="ruleId" required >
            <Select options={rules.map(r => ({
              label: r.name, 
              value: r.id
            }))}/>
          </Form.Item>
        ) : (
          <Form.Item label="执行器" name="executorId" required >
            <Select options={executors.map(r => ({
              label: r.funcName, 
              value: r.id
            }))}/>
          </Form.Item>
        )}
      </Form>
    </Modal>
  );
};
export default ButtonModal;
