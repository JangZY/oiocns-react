import { Popover, Input, Button, List, Divider } from 'antd';
import React, { useEffect, useState } from 'react';
import { IMessage, ISession, ISysFileInfo, MessageType } from '@/ts/core';
import OpenFileDialog from '@/components/OpenFileDialog';
import ScrollList from '@/components/Common/ScrollList';
import { parseCiteMsg } from '../components/parseMsg';
import Emoji from '../components/emoji';
import EntityIcon from '@/components/Common/GlobalComps/entityIcon';
import OrgIcons from '@/components/Common/GlobalComps/orgIcons';
import UploadIcon from '@/components/Common/FileUpload/uploadIcon';
import { IDistributionTask } from '@/ts/core/thing/standard/distributiontask';
import { CreateTaskModal } from '../Task/CreateTaskModal';
import 'lodash';
import HistoryMessage from '../HistoryMessage';
import { schema } from '@/ts/base';
const TextArea = Input.TextArea;
/**
 * @description: 输入区域
 * @return {*}
 */

interface IProps {
  chat: ISession;
  citeText?: IMessage;
  writeContent?: string;
  closeCite: () => void;
}

const GroupInputBox = (props: IProps) => {
  const [openEmoji, setOpenEmoji] = useState(false);
  const [atShow, setAtShow] = useState<boolean>(false);
  const [message, setMessage] = useState(props.chat.inputContent.message);
  const [openMore, setOpenMore] = useState(false);
  const [openType, setOpenType] = useState<string>();
  const [createTask, setCreateTask] = useState<boolean>(false);
  const [historyMessageModal, setHistoryMessageModal] = useState<boolean>(false);
  const [task, setTask] = useState<IDistributionTask | null>(null);

  useEffect(() => {
    if (props.writeContent) {
      setMessage(props.writeContent);
    }
  }, [props.writeContent]);

  useEffect(() => {
    props.chat.inputContent.message = message;
  }, [message]);

  /** 发送消息 */
  const sendMessage = () => {
    if (message.length > 0) {
      const vaildMentions: string[] = [];
      for (const mention of props.chat.inputContent.mentions) {
        if (message.includes(mention.text) && !vaildMentions.includes(mention.id)) {
          vaildMentions.push(mention.id);
        }
      }
      props.chat.sendMessage(MessageType.Text, message, vaildMentions, props.citeText);
      setMessage('');
      props.closeCite();
    }
  };

  /** 引用展示 */
  const citeShowText = (val: IMessage) => {
    return (
      <div className="cite-text">
        <div className="cite-text-content">
          <OrgIcons
            type="/toolbar/close"
            size={30}
            notAvatar
            onClick={() => props.closeCite()}
            className="cite-text-close-icon"
          />
          {parseCiteMsg(val)}
        </div>
      </div>
    );
  };

  /** 渲染@列表 */
  const RenderAtList = () => {
    const [filter, setFilter] = useState('');
    const [members, setMembers] = useState<schema.XTarget[]>(
      props.chat.members.filter((i) => i.id != props.chat.userId),
    );
    useEffect(() => {
      loadMembers().then((res) => setMembers(res));
    }, [filter]);
    const loadMembers = async () => {
      if (props.chat.memberCount === props.chat.members.length) {
        return props.chat.members
          .filter((i) => i.id != props.chat.userId)
          .filter((i) => i.name.includes(filter) || i.code.includes(filter));
      } else {
        await props.chat.target.loadMembers(false, filter);
        return props.chat.members.filter((i) => i.id != props.chat.userId);
      }
    };
    return (
      <div className="chat-at-list">
        <ScrollList
          loaded={true}
          height={'390px'}
          searchValue={filter}
          setSearchValue={(v) => setFilter(v)}
          data={members}
          onLoadMore={() => loadMembers().then((res) => setMembers(res))}
          renderItem={(item) => {
            return (
              <div
                key={item.id}
                className="chat-at-list-item"
                onClick={() => {
                  props.chat.inputContent.mentions.push({
                    id: item.id,
                    text: `@${item.name} `,
                  });
                  setAtShow(false);
                  setMessage((message) => message + item.name + ' ');
                }}>
                <EntityIcon disableInfo entity={item} size={35} />
                <span>{item.name}</span>
              </div>
            );
          }}
        />
      </div>
    );
  };
  const moreListItem = [
    {
      title: '发送云文档',
      type: '/toolbar/store',
      onClick: () => setOpenType('文件'),
    },
    {
      title: '发送任务',
      type: '/toolbar/task',
      onClick: () => setOpenType('任务'),
    },
    {
      title: '发送语音',
      type: '/toolbar/audio',
    },
    {
      title: '视频通话',
      type: '/toolbar/video',
    },
  ];

  const handleHistoryMessage = () => {
    setHistoryMessageModal(false);
  };
  return (
    <div className="chat-send-box">
      <div style={{ width: '100%' }}>
        {props.citeText && citeShowText(props.citeText)}
      </div>
      <div className="chat-send-box-main">
        <div style={{ width: '100%' }}>
          {atShow && (
            <Popover
              align={{
                points: ['t', 'l'],
              }}
              content={<RenderAtList />}
              open={atShow}
              trigger={['click', 'contextMenu']}
              onOpenChange={setAtShow}></Popover>
          )}
          <TextArea
            value={message}
            autoSize={{ minRows: 1, maxRows: 5 }}
            allowClear={true}
            placeholder={`Enter键发送, Alt+Enter键换行。`}
            bordered={false}
            onChange={(e) => {
              const value = e.target.value;
              if (!value.endsWith('\n')) {
                if (value.endsWith('@')) {
                  setMessage(value);
                  setAtShow(true);
                } else {
                  setMessage(value);
                }
              } else {
                setMessage(value);
              }
            }}
            onPressEnter={(e) => {
              e.preventDefault();
              if (e.altKey === true && e.key === 'Enter') {
                setMessage((pre) => pre + '\n');
              } else {
                sendMessage();
              }
            }}
          />
        </div>
        <OrgIcons type="/toolbar/setFull" size={22} notAvatar />
        <Divider type="vertical" style={{ margin: '0' }} />

        <Popover
          content={
            <Emoji
              onSelect={(emoji: string) => {
                setOpenEmoji(false);
                setMessage((message) => message + emoji);
              }}
            />
          }
          open={openEmoji}
          trigger={['click', 'contextMenu']}
          onOpenChange={setOpenEmoji}>
          <OrgIcons type="/toolbar/emoji" size={22} notAvatar />
        </Popover>
        <UploadIcon
          size={22}
          onSelected={async (file) => {
            let msgType = MessageType.File;
            if (file.groupTags.includes('图片')) {
              msgType = MessageType.Image;
            } else if (file.groupTags.includes('视频')) {
              msgType = MessageType.Video;
            }
            await props.chat.sendMessage(msgType, JSON.stringify(file.shareInfo()), []);
          }}
        />
        {/* TODO 聊天历史入口 暂时隐藏 */}
        {/* <OrgIcons
          type="/toolbar/historyChat"
          size={22}
          notAvatar
          onClick={() => {
            setHistoryMessageModal(true);
          }}
        /> */}
        <Popover
          content={
            <List
              style={{ paddingLeft: '20px', paddingTop: '20px' }}
              grid={{ gutter: 4, column: 1 }}
              itemLayout="horizontal"
              dataSource={moreListItem}
              renderItem={(item) => (
                <List.Item style={{ cursor: 'pointer' }} onClick={item.onClick}>
                  <List.Item.Meta
                    avatar={<OrgIcons type={item.type} size={22} notAvatar />}
                    title={item.title}
                    description=""
                  />
                </List.Item>
              )}
            />
          }
          open={openMore}
          trigger={['click', 'contextMenu']}
          onOpenChange={setOpenMore}>
          <OrgIcons type="/toolbar/addCircle" size={22} notAvatar />
        </Popover>
        <Divider type="vertical" style={{ margin: '0' }} />
        <Button
          disabled={!message.length}
          size="middle"
          onClick={() => sendMessage()}
          style={{
            padding: '2px 14px 2px 6px',
          }}
          type={message.length > 0 ? 'primary' : 'default'}
          icon={<OrgIcons type="/toolbar/send" size={20} notAvatar />}>
          <span style={{ fontSize: '14px', lineHeight: '20px', color: '#fff' }}>
            &nbsp;&nbsp;发送
          </span>
        </Button>
      </div>
      {openType && (
        <OpenFileDialog
          rootKey={'disk'}
          accepts={[openType]}
          allowInherited
          currentKey={props.chat.target.directory.key}
          onCancel={() => setOpenType(undefined)}
          onOk={async (files) => {
            if (files.length > 0) {
              switch (openType) {
                case '文件': {
                  const file = files[0] as ISysFileInfo;
                  let msgType = MessageType.File;
                  if (file.groupTags.includes('图片')) {
                    msgType = MessageType.Image;
                  } else if (file.groupTags.includes('视频')) {
                    msgType = MessageType.Video;
                  }
                  const share = JSON.stringify(file.shareInfo());
                  await props.chat.sendMessage(msgType, share, []);
                  break;
                }
                case '任务': {
                  const file = files[0] as IDistributionTask;
                  setTask(file);
                  setCreateTask(true);
                  break;
                }
              }
            }
            setOpenType(undefined);
          }}
        />
      )}
      {createTask && (
        <CreateTaskModal
          task={task!}
          onCancel={() => setCreateTask(false)}
          onFinished={async (dist) => {
            await props.chat.sendMessage(MessageType.Task, JSON.stringify(dist), []);
            setCreateTask(false);
          }}
        />
      )}
      {historyMessageModal && (
        <HistoryMessage chat={props.chat} handleCallBack={handleHistoryMessage} />
      )}
    </div>
  );
};

export default GroupInputBox;
