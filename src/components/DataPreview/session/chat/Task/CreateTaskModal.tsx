import { ReportContentForm } from '@/executor/operate/entityForm/taskContent/ReportContentForm';
import { XDistribution } from '@/ts/base/schema';
import { DistributionContent, TaskContentType } from '@/ts/base/model';
import { IDistributionTask } from '@/ts/core/thing/standard/distributiontask';
import { Modal, message } from 'antd';
import _ from 'lodash';
import React, { useEffect, useState } from 'react';
import { ReportTree } from '@/ts/core/thing/standard/reporttree';

interface Props {
  task: IDistributionTask;
  onFinished?: (dist: XDistribution) => void;
  onCancel?: () => void;
}

export function CreateTaskModal(props: Props) {
  const [distribution, setDistribution] = useState<XDistribution>(null!);
  const [content, setContent] = useState<DistributionContent>(null!);
  const [loading, setLoading] = useState(false);
  const [hasStart, setHasStart] = useState(false);
  const [msg, setMsg] = useState('');

  async function setDataAsync() {
    const dist: XDistribution = _.omit(_.cloneDeep(props.task.metadata), [
      'id',
      'typeName',
      'directoryId',
      'belongId',
      'createUser',
      'createTime',
      'updateUser',
      'updateTime',
    ]) as any;
    dist.typeName = '分发任务';
    dist.taskId = props.task.metadata.id;
    let resWork = await props.task.directory.target.resource.findEntityById(
      dist.content.workId!,
      '办事',
    );
    dist.content.workName = resWork?.name;
    let resReportTree = await props.task.directory.target.resource.findEntityById(
      dist.content.treeId!,
      '报表树',
    );
    dist.content.treeName = resReportTree?.name;
    setDistribution(dist);
    setContent(dist.content as DistributionContent);
  }

  useEffect(() => {
    setDataAsync();
  }, []);

  async function newDistribution() {
    distribution.period = content.period;
    delete content['period'];
    distribution.content = content;
    setHasStart(true);
    try {
      setLoading(true);
      setMsg('正在创建任务分发');
      const d = await props.task.create(distribution);
      if (d) {
        if (distribution.content.type == TaskContentType.Report) {
          setMsg('正在创建任务树形');

          const [metadata] = await props.task.directory.resource.reportTreeColl.loadSpace({
            options: {
              match: {
                id: d.content.treeId,
              },
            },
          });
          if (!metadata) {
            setMsg('找不到报表树形');
            return;
          }

          const tree = new ReportTree(metadata, props.task.directory);
          if (!await tree.createTaskTree(d)) {
            setMsg('创建任务树形失败');
            return;
          }
        }

        message.success('创建成功');
        props.onFinished?.(d);
        return;
      }
      setMsg('创建失败');
    } catch (error) {
      if (error instanceof Error) {
        setMsg(error.message);
        message.error('创建失败');
      }
    } finally {
      setLoading(false);
    }
  }
  return (
    <Modal
      open
      title="分发任务"
      onCancel={props.onCancel}
      onOk={newDistribution}
      confirmLoading={loading}>
      <div>
        {hasStart ? (
          <div style={{ padding: '16px' }}>
            <div style={{ textAlign: 'center' }}>{msg}</div>
          </div>
        ) : content ? (
          <ReportContentForm
            task={props.task}
            distribute
            value={content}
            onChange={(v) => setContent(v as DistributionContent)}
            directory={props.task.directory}
          />
        ) : (
          <></>
        )}
      </div>
    </Modal>
  );
}
