import { IFinancial, IPeriod } from '@/ts/core';
import { Button } from 'antd';
import React, { useEffect } from 'react';
import { FullScreen } from '..';
import message from '@/utils/message';

interface TemplateProps {
    periods: IPeriod[];
    financial: IFinancial;
    onFinished?: () => void;
    onCancel?: () => void;
}

export const HandlerPeriodTemplate: React.FC<TemplateProps> = (props) => {
    const init_periods = props.financial.initialized
    const init_year = (init_periods ? Number(init_periods.slice(0, 4)) : 0)
    const init_month = (init_periods ? Number(init_periods.slice(5)) : 0)


    const financial = props.financial
    const periods = props.periods

    const [handler_year, setYear] = React.useState('');
    const [handler_month, setMonth] = React.useState('');

    const year_change = async (e:any) => {
        await setYear(e)
    };
    const month_change = async (e:any) => {
        await setMonth(e)
    };

    //增加账期
    const add = async (month: string, offsetPeriod: number) => {
        const next = financial.getOffsetPeriod(month, offsetPeriod);
        console.log('offset===', next)
        await financial.createSnapshots(next);
        await financial.createPeriod(next);
    }

    const ok = () => {
        if (periods) {
            const date = new Date()
            const month = date.getMonth() + 1
            const year = date.getFullYear()
            const period_month = Number(periods[0].period.slice(5))
            const period_year = Number(periods[0].period.slice(0, 4))
            //判断是否有月结账
            if (periods.length > 0) {
                if (Number(handler_year) > 0 && Number(handler_month) > 0) {
                    if ((Number(handler_year) > year && Number(handler_month) > month) || (Number(handler_year) >= year && Number(handler_month) > month) || Number(handler_year) < init_year || (Number(handler_year) == init_year && Number(handler_month) < init_month)) {
                        message.error('日期超出最大或最小范围')
                    }
                    else if ((Number(handler_year) <= year && Number(handler_month) <= month) || (Number(handler_year) >= init_year && Number(handler_month) >= init_month)) {
                        if ((Number(handler_year) === period_year) && (period_year === year) && (Number(handler_month) === period_month) && (period_month === month)) {
                            message.error('最新账期已存在')
                        }
                        else {
                            let judge_date = true
                            let judge_year = false
                            periods.map((i) => {
                                const history_year = Number(i.period.slice(0, 4))
                                const history_month = Number(i.period.slice(5))
                                if (Number(handler_year) !== history_year) {
                                    judge_year = true
                                }
                                else {
                                    if (Number(handler_year) === history_year && Number(handler_month) === history_month) {
                                        judge_date = false
                                    }
                                }
                            })
                            if (judge_date || judge_year) {
                                const num_month = Number(handler_month)
                                if (num_month === 12) {
                                    const final_date = `${String(Number(handler_year) + 1)}-01}`
                                    add(final_date, 0)
                                    message.info(`${final_date}的账期添加成功`)
                                }
                                else if (num_month < 12 && num_month >= 10) {
                                    const final_date = `${handler_year}-${String(Number(handler_month))}`
                                    add(final_date, 0)
                                    message.info(`${final_date}的账期添加成功`)
                                }
                                else {
                                    const final_date = `${handler_year}-0${String(Number(handler_month))}`
                                    add(final_date, 0)
                                    message.info(`${final_date}的账期添加成功`)
                                }
                            }
                            else {
                                message.error('要添加的账期已存在')
                            }
                        }
                    }
                }
            }
        }
        else {
            // console.log('没有账期')
        }
    }

    useEffect(() => {
    }, []);
    return (
        <>
            <FullScreen
                title={'生成指定月份账期'}
                onFinished={props.onFinished}
                onCancel={props.onCancel}>
                <div style={{ width: '100%', textAlign: 'center', marginTop: '50px' }}>
                    <input placeholder='请输入要生成的年份' onChange={(a) => year_change(a.target.value)}></input><br /><br />
                    <input placeholder='请输入要生成的月份' onChange={(a) => month_change(a.target.value)}></input><br /><br />
                    <Button onClick={() => { ok() }}>确定</Button>
                </div>
            </FullScreen>
        </>
    );
};
