import { nextTick } from '@/ts/base/common/timer';
import { getAllNodes } from '@/ts/base/common/tree';
import { ReportTaskTreeNodeView } from '@/ts/base/model';
import { IReportDistribution } from '@/ts/core/work/assign/distribution/report';
import { getStatus } from '@/ts/core/work/assign/reception/status';
import { Button, Modal, Progress, message } from 'antd';
import _ from 'lodash';
import React, { useState } from 'react';

interface Props {
  distribution: IReportDistribution;
  treeNode: ReportTaskTreeNodeView;
  visible: boolean;
  onClose: () => void;
}

export function RefreshStatusModal(props: Props) {
  const [progress, setProgress] = useState(0);
  const [isStart, setIsStart] = useState(false);

  async function startSync() {
    try {
      const nodes = getAllNodes([props.treeNode]);
      const total = nodes.length;
      let current = 0;

      const chunks = _.chunk(nodes, 1000);
      for (const chunk of chunks) {
        const receptions = await props.distribution.findReportReceptions(
          chunk.map((node) => node.id),
        );

        await nextTick();

        const newNodes = chunk.map((node) => {
          const newNode = _.cloneDeep(node);
          delete newNode.reception;
          delete (newNode as any).children;
          newNode.taskStatus = getStatus(receptions[node.id]);
          return newNode;
        });

        const ret = await props.distribution.holder.tree!.nodeColl.replaceMany(newNodes);
        console.warn(ret);

        current += chunk.length;
        setProgress(parseFloat(((current / total) * 100).toFixed(2)));
      }
      message.success('更新完成');
    } catch (error) {
      message.error(error instanceof Error ? error.message : String(error));
    }
  }

  return (
    <Modal
      open={props.visible}
      title="更新状态"
      width={640}
      onOk={props.onClose}
      onCancel={props.onClose}>
      <div className="flex flex-col" style={{ height: '40vh' }}>
        <div style={{ padding: '8px', marginBottom: '8px' }}>
          将新的上报状态更新到当前空间
        </div>
        {isStart ? (
          <div className="flex flex-col flex-auto justify-center items-center">
            <Progress percent={progress} style={{ width: '75%' }} />
          </div>
        ) : (
          <Button
            type="primary"
            onClick={() => {
              setIsStart(true);
              startSync();
            }}>
            开始更新
          </Button>
        )}
      </div>
    </Modal>
  );
}
