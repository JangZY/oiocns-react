import { schema } from '@/ts/base';
import { IBelong } from '../../target/base/belong';
import { XCollection } from '../../public/collection';
import { ISession } from '../../chat/session';
import { ITarget } from '../..';
import { Emitter } from '@/ts/base/common';

export interface IOrderManager {
  /** 操作空间 */
  space: IBelong;
  /** 创建订单 */
  create(item: schema.XOrder): Promise<schema.XOrder | undefined>;
  /** 创建临时会话 */
  session(platform: ITarget): Promise<ISession>;
}

export class OrderManager extends Emitter implements IOrderManager {
  constructor(space: IBelong) {
    super();
    this.space = space;
    this.coll = space.resource.genColl('transaction-order');
    this.coll.subscribe([this.space.id + '-order'], (data) => {
      switch (data.operate) {
        case 'refresh':
          this.changCallback();
          break;
      }
    });
  }
  space: IBelong;
  coll: XCollection<schema.XOrder>;
  async create(item: schema.XOrder): Promise<schema.XOrder | undefined> {
    const result = await this.coll.insert(item);
    if (result) {
      await this.coll.notity({ operate: 'refresh' });
    }
    return result;
  }
  session(platform: ITarget): Promise<ISession> {
    throw new Error('Method not implemented.');
  }
}
