import { logger } from '@/ts/base/common';
import { IWork } from '.';
import { schema, model, kernel } from '../../base';
import { PageAll, TaskStatus, entityOperates } from '../public';
import { IBelong } from '../target/base/belong';
import { DataProvider } from '../provider';
import { IWorkApply } from './apply';
import { FileInfo, IFile } from '../thing/fileinfo';
import { Acquire } from './executor/acquire';
import { IExecutor } from './executor';
import { FieldsChange } from './executor/change';
import { Webhook } from './executor/webhook';
import message from '@/utils/message';
import { AcquireExecutor } from '@/ts/base/model';
import { ReceptionChange } from './executor/reception';
export type TaskTypeName = '待办' | '已办' | '抄送' | '已发起' | '已完结' | '草稿';

export interface IWorkTask extends IFile {
  /** 内容 */
  comment: string;
  /** 当前用户 */
  user: DataProvider;
  /** 归属空间 */
  belong: IBelong;
  /** 任务元数据 */
  taskdata: schema.XWorkTask;
  /** 流程实例 */
  instance: schema.XWorkInstance | undefined;
  /** 实例携带的数据 */
  instanceData: model.InstanceDataModel | undefined;
  /** 加用户任务信息 */
  targets: schema.XTarget[];
  /** 是否为历史对象 */
  isHistory: boolean;
  /** 是否为指定的任务类型 */
  isTaskType(type: TaskTypeName): boolean;
  /** 是否满足条件 */
  isMatch(filter: string): boolean;
  /** 任务更新 */
  updated(_metadata: schema.XWorkTask): Promise<boolean>;
  /** 加载流程实例数据 */
  loadInstance(reload?: boolean): Promise<boolean>;
  /** 撤回任务 */
  recallApply(): Promise<boolean>;
  /** 创建申请(子流程) */
  createApply(): Promise<IWorkApply | undefined>;
  /** 任务审批 */
  approvalTask(
    status: number,
    comment?: string,
    fromData?: Map<string, model.FormEditData>,
    gatewayInfo?: Map<string, string[]>,
  ): Promise<boolean>;
  /** 获取办事 */
  findWorkById(wrokId: string): Promise<IWork | undefined>;
  /** 加载执行器 */
  loadExecutors(node: model.WorkNodeModel): IExecutor[];
  /** 获取该任务下一步节点 */
  loadNextNodes(): Promise<model.ResultType<model.PageResult<schema.XWorkNode>>>;
}

export class WorkTask extends FileInfo<schema.XEntity> implements IWorkTask {
  private history: boolean;
  constructor(
    _metadata: schema.XWorkTask,
    _user: DataProvider,
    history: boolean = false,
  ) {
    super(_metadata as any, _user.user!.directory);
    this.taskdata = _metadata;
    this.user = _user;
    this.history = history;
  }
  user: DataProvider;
  cacheFlag: string = 'worktask';
  taskdata: schema.XWorkTask;
  instance: schema.XWorkInstance | undefined;
  instanceData: model.InstanceDataModel | undefined;
  get isHistory(): boolean {
    return this.history;
  }
  get groupTags(): string[] {
    return [this.belong.name, this.taskdata.taskType, this.taskdata.approveType];
  }
  get metadata(): schema.XEntity {
    let typeName = this.taskdata.taskType;
    if (
      ['子流程', '网关'].includes(this.taskdata.approveType) &&
      this.taskdata.identityId &&
      this.taskdata.identityId.length > 5
    ) {
      typeName = '子流程';
    }
    if (this.targets.length === 2) {
      typeName = '加' + this.targets[1].typeName;
    }
    return {
      ...this.taskdata,
      icon: '',
      typeName,
      belong: undefined,
      name: this.taskdata.title,
      code: this.taskdata.instanceId,
      remark: this.comment || '暂无信息',
    };
  }
  delete(): Promise<boolean> {
    throw new Error('Method not implemented.');
  }
  operates(): model.OperateModel[] {
    return [entityOperates.Open, entityOperates.Remark, entityOperates.QrCode];
  }
  get comment(): string {
    if (this.targets.length === 2) {
      return `${this.targets[0].name}[${this.targets[0].typeName}]申请加入${this.targets[1].name}[${this.targets[1].typeName}]`;
    }
    return this.taskdata.content;
  }
  get belong(): IBelong {
    for (const company of this.user.user!.companys) {
      if (company.id === this.taskdata.belongId) {
        return company;
      }
    }
    return this.user.user!;
  }
  get targets(): schema.XTarget[] {
    if (this.taskdata.taskType == '加用户') {
      try {
        return JSON.parse(this.taskdata.content) || [];
      } catch (ex) {
        logger.error(ex as Error);
      }
    }
    return [];
  }
  isMatch(filter: string): boolean {
    return JSON.stringify(this.taskdata).includes(filter);
  }
  isTaskType(type: TaskTypeName): boolean {
    switch (type) {
      case '已办':
        return this.taskdata.status >= TaskStatus.ApprovalStart;
      case '已发起':
      case '已完结':
        return this.taskdata.createUser == this.userId;
      case '待办':
        return this.taskdata.status < TaskStatus.ApprovalStart;
      case '抄送':
        return this.taskdata.approveType === '抄送';
      case '草稿':
        return false;
    }
  }
  async updated(_metadata: schema.XWorkTask): Promise<boolean> {
    if (this.taskdata.id === _metadata.id) {
      this.taskdata = _metadata;
      await this.loadInstance(true);
      return true;
    }
    return false;
  }
  async loadInstance(reload: boolean = false): Promise<boolean> {
    if (this.instanceData !== undefined && !reload) return true;
    const data = await kernel.findInstance(
      this.taskdata.shareId,
      this.taskdata.belongId,
      this.taskdata.instanceId,
    );
    if (data) {
      try {
        this.instance = data;
        this.instanceData = eval(`(${data.data})`);
        return this.instanceData !== undefined;
      } catch (ex) {
        logger.error(ex as Error);
      }
    }
    return false;
  }
  loadExecutors(node: model.WorkNodeModel) {
    let executors: IExecutor[] = [];
    if (node.executors) {
      for (const item of node.executors) {
        switch (item.funcName) {
          case '资产领用':
          case '数据申领':
            executors.push(new Acquire(item as AcquireExecutor, this));
            break;
          case '字段变更':
            executors.push(new FieldsChange(item, this));
            break;
          case 'Webhook':
            executors.push(new Webhook(item, this));
            break;
          case '任务状态变更':
            executors.push(new ReceptionChange(item, this));
            break;
          default:
            break;
        }
      }
    }
    return executors;
  }
  async recallApply(): Promise<boolean> {
    if ((await this.loadInstance()) && this.instance) {
      if (this.instance.createUser === this.belong.userId) {
        if (await kernel.recallWorkInstance({ id: this.instance.id })) {
          return true;
        }
      }
    }
    return false;
  }

  async createApply(): Promise<IWorkApply | undefined> {
    if (this.taskdata.approveType == '子流程' || this.taskdata.approveType == '网关') {
      await this.loadInstance();
      var define = await this.findWorkById(this.taskdata.defineId);
      if (define) {
        return await define.createApply(this.id, this.instanceData);
      }
    }
  }

  async findWorkById(wrokId: string): Promise<IWork | undefined> {
    const res = await kernel.queryWorkDefine({
      id: wrokId,
      belongId: '0',
      page: PageAll,
    });
    if (res.success && res.data && res.data.result && res.data.result.length > 0) {
      var define = res.data.result[0];
      var target = this.user.targets.find((i) => i.id === define.shareId);
      if (target) {
        for (var app of await target.directory.loadAllApplication()) {
          const work = await app.findWork(wrokId, define.applicationId);
          if (work) {
            return work;
          }
        }
      } else {
        message.warn('未找到办事对应组织!');
      }
    }
  }

  async approvalTask(
    status: number,
    comment: string,
    fromData: Map<string, model.FormEditData>,
    gateways?: Map<string, string[]>,
  ): Promise<boolean> {
    if (this.taskdata.status < TaskStatus.ApprovalStart) {
      if (status === -1) {
        return await this.recallApply();
      }
      if (this.taskdata.taskType === '加用户') {
        return this.approvalJoinTask(status, comment);
      } else {
        fromData?.forEach((data, k) => {
          if (this.instanceData) {
            if (this.instanceData.data[k]) {
              this.instanceData.data[k].push(
                ...this.instanceData.data[k].filter((s) => s.nodeId != data.nodeId),
                data,
              );
            }
            this.instanceData.data[k] = [data];
          }
        });
        var gatewayInfos: model.WorkGatewayInfoModel[] = [];
        gateways?.forEach((v, k) => {
          gatewayInfos.push({
            nodeId: k,
            targetIds: v,
          });
        });
        const res = await kernel.approvalTask({
          id: this.taskdata.id,
          status: status,
          comment: comment,
          data: JSON.stringify(this.instanceData),
          gateways: JSON.stringify(gatewayInfos),
        });
        return res.data === true;
      }
    }
    return false;
  }

  async loadNextNodes(): Promise<model.ResultType<model.PageResult<schema.XWorkNode>>> {
    return await kernel.queryNextNodes({ id: this._metadata.id });
  }

  // 申请加用户审批
  private async approvalJoinTask(status: number, comment: string): Promise<boolean> {
    if (this.targets && this.targets.length === 2) {
      if (status < TaskStatus.RefuseStart) {
        const target = this.user.targets.find((a) => a.id == this.targets[1].id);
        if (target) {
          target.pullMembers([this.targets[0]]);
        } else {
          message.warn('组织加载中，请等待加载完成后再进行该任务审批');
          return false;
        }
      }
      const res = await kernel.approvalTask({
        id: this.taskdata.id,
        status: status,
        comment: comment,
        data: JSON.stringify(this.instanceData),
        gateways: '',
      });
      return res.success;
    }
    return false;
  }
}
