import { kernel, model, schema } from '../../base';
import { IApplication } from '../thing/standard/application';
import { IForm, Form } from '../thing/standard/form';
import { FileInfo, IFile, IFileInfo } from '../thing/fileinfo';
import { IDirectory } from '../thing/directory';
import { IWorkApply, WorkApply } from './apply';
import { TargetType, entityOperates, fileOperates } from '../public';
import { getUuid } from '@/utils/tools';
import { getEndNode, isHasApprovalNode } from '@/utils/work';
import { deepClone, formatDate } from '@/ts/base/common';
import { workOperates } from '../public/operates';
import { IPrint, Print } from '../thing/standard/print';
import _ from 'lodash';
import { FormInfo } from '@/ts/base/model';
import { setDefaultField } from '@/ts/scripting/core/services/FormService';
import { ITarget } from '..';

export interface IWork extends IFileInfo<schema.XWorkDefine> {
  /** 我的办事 */
  isMyWork: boolean;
  /** 主表 */
  primaryForms: IForm[];
  /** 打印模板 */
  primaryPrints: IPrint[];
  /** 所有表单 */
  readonly forms: IForm[];
  /** 子表 */
  detailForms: IForm[];
  /** 应用 */
  application: IApplication;
  /** 成员节点绑定信息 */
  gatewayInfo: schema.XWorkGateway[];
  /** 流程节点 */
  node: model.WorkNodeModel | undefined;
  /** 更新办事定义 */
  update(req: model.WorkDefineModel): Promise<boolean>;
  /** 加载事项定义节点 */
  loadNode(reload?: boolean): Promise<model.WorkNodeModel | undefined>;
  /** 加载成员节点信息 */
  loadGatewayInfo(reload?: boolean): Promise<schema.XWorkGateway[]>;
  /** 删除绑定 */
  deleteGateway(id: string): Promise<boolean>;
  /** 分发办事 */
  distribute(destination: IDirectory): Promise<boolean>;
  /** 绑定成员节点 */
  bingdingGateway(
    nodeId: string,
    identity: schema.XIdentity,
    define: schema.XWorkDefine,
  ): Promise<schema.XWorkGateway | undefined>;
  /** 生成办事申请单 */
  createApply(
    taskId?: string,
    pdata?: model.InstanceDataModel,
    reload?: boolean,
  ): Promise<IWorkApply | undefined>;
  /** 创建默认物 */
  createThing(data: model.InstanceDataModel): Promise<void>;
  /** 通知变更 */
  notify(operate: string, data: any): void;
  /** 接收通知 */
  receive(operate: string, data: schema.XWorkDefine): boolean;
  /** 商城申领数据 */
  applyData(
    node: model.WorkNodeModel,
    items: schema.XProduct[],
  ): Promise<model.InstanceDataModel>;
}

export const fullDefineRule = (data: schema.XWorkDefine, target: ITarget) => {
  data.hasGateway = false;
  if (data.rule && data.rule.includes('{') && data.rule.includes('}')) {
    const rule = JSON.parse(data.rule);
    data.hasGateway = rule.hasGateway;
    data.applyType = rule.applyType;
    data.allowInitiate =
      rule.allowInitiate ?? ![TargetType.Group].includes(target.typeName as TargetType);
  }
  data.typeName = '办事';
  return data;
};

export class Work extends FileInfo<schema.XWorkDefine> implements IWork {
  constructor(_metadata: schema.XWorkDefine, _application: IApplication) {
    super(fullDefineRule(_metadata, _application.target), _application.directory);
    this.application = _application;
  }
  canDesign: boolean = true;
  primaryForms: IForm[] = [];
  primaryPrints: IPrint[] = [];
  detailForms: IForm[] = [];
  application: IApplication;
  node: model.WorkNodeModel | undefined;
  gatewayInfo: schema.XWorkGateway[] = [];
  get locationKey(): string {
    return this.application.key;
  }
  get cacheFlag(): string {
    return 'works';
  }
  get forms(): IForm[] {
    return [...this.primaryForms, ...this.detailForms];
  }
  get prints(): IPrint[] {
    return [...this.primaryPrints];
  }
  get superior(): IFile {
    return this.application;
  }
  get groupTags(): string[] {
    const tags = [this.target.space.name];
    if (this.target.id != this.target.spaceId) {
      tags.push(this.target.name);
    }
    return [...tags, ...super.groupTags];
  }
  get isMyWork(): boolean {
    return this._metadata.applyAuth
      ? this.target.hasAuthoritys([this._metadata.applyAuth])
      : true;
  }
  allowCopy(destination: IDirectory): boolean {
    return ['应用', '模块'].includes(destination.typeName);
  }
  allowDistribute(destination: IDirectory): boolean {
    return (
      ['应用', '模块'].includes(destination.typeName) &&
      this.directory.target.id !== destination.target.id
    );
  }
  allowMove(destination: IDirectory): boolean {
    return (
      ['应用', '模块'].includes(destination.typeName) &&
      destination.id !== this.metadata.applicationId &&
      destination.target.belongId == this.target.belongId
    );
  }
  async delete(_notity: boolean = false): Promise<boolean> {
    if (this.application) {
      const res = await kernel.deleteWorkDefine({
        id: this.id,
      });
      if (res.success) {
        await this.recorder.remove({
          coll: this.directory.resource.workDefineColl,
          next: this.metadata,
        });
        this.application.works = this.application.works.filter((a) => a.id != this.id);
        this.notify('remove', this.metadata);
        this.application.changCallback();
      }
      return res.success;
    }
    return false;
  }
  hardDelete(_notity: boolean = false): Promise<boolean> {
    return this.delete(_notity);
  }
  async rename(_name: string): Promise<boolean> {
    const node = await this.loadNode();
    return await this.update({
      ...this.metadata,
      name: _name,
      resource: node,
    });
  }
  async copy(destination: IDirectory): Promise<boolean> {
    if (this.allowCopy(destination)) {
      const app = destination as unknown as IApplication;
      const isSameTarget = this.directory.target.id === destination.target.id;
      var node = deepClone(await this.loadNode());
      var entNode = getEndNode(node);
      if (node && node.code) {
        delete node.children;
        delete node.branches;
      } else {
        node = {
          code: `node_${getUuid()}`,
          type: '起始',
          name: '发起',
          num: 1,
          forms: [],
          executors: [],
          formRules: [],
          primaryPrints: [],
          primaryForms: [],
          detailForms: [],
        } as unknown as model.WorkNodeModel;
      }
      node.children = entNode;
      const data = {
        ...this.metadata,
        sourceId: this.metadata.sourceId ?? this.id,
        resource: node && node.code ? node : undefined,
        id: '0',
      };
      if (isSameTarget) {
        const uuid = getUuid();
        data.code = `${this.metadata.code}-${uuid}`;
        data.name = `${this.metadata.name} - 副本${uuid}`;
      }
      const res = await app.createWork(data);
      return res != undefined;
    }
    return false;
  }
  async move(destination: IDirectory): Promise<boolean> {
    if (this.allowMove(destination)) {
      const app = destination as unknown as IApplication;
      const work = await app.createWork({
        ...this.metadata,
        resource: undefined,
      });
      if (work) {
        this.notify('workRemove', this.metadata);
        return true;
      }
    }
    return false;
  }
  async distribute(destination: IDirectory): Promise<boolean> {
    if (this.allowDistribute(destination)) {
      const app = destination as unknown as IApplication;
      var node = deepClone(await this.loadNode());
      var hasApprovalNode = isHasApprovalNode(node);
      var entNode = getEndNode(node);
      if (node && node.code) {
        delete node.children;
        delete node.branches;
      } else {
        node = {
          code: `node_${getUuid()}`,
          type: '起始',
          name: '发起',
          num: 1,
          forms: [],
          executors: [],
          formRules: [],
          primaryPrints: [],
          primaryForms: [],
          detailForms: [],
        } as unknown as model.WorkNodeModel;
      }
      node.children = {
        id: '0',
        num: 0,
        type: hasApprovalNode ? '审批' : '抄送',
        destType: '其他办事',
        destName: `[${this.target.name}]${this.name}`,
        defineId: '0',
        belongId: '0',
        code: 'JGNODE' + getUuid(),
        name: '监管办事',
        destId: this.metadata.id,
        resource: '{}',
        children: entNode,
        branches: undefined,
        primaryPrints: [],
        primaryForms: [],
        detailForms: [],
        formRules: [],
        forms: [],
        executors: [],
        encodes: [],
        print: [],
        splitRules: [],
        printData: { attributes: [], type: '' },
      };
      const data = {
        ...this.metadata,
        sourceId: this.metadata.sourceId ?? this.id,
        resource: node && node.code ? node : undefined,
        id: '0',
      };
      const res = await app.createWork(data);
      return res != undefined;
    }
    return false;
  }
  content(): IFile[] {
    if (this.node) {
      const ids = this.node.forms?.map((i) => i.id) ?? [];
      return this.forms.filter((a) => ids.includes(a.id));
    }
    return [];
  }
  async loadContent(_reload: boolean = false): Promise<boolean> {
    await this.loadNode(_reload);
    await this.loadGatewayInfo(true);
    return this.forms.length > 0;
  }
  async update(data: model.WorkDefineModel): Promise<boolean> {
    data.id = this.id;
    data.applicationId = this.metadata.applicationId;
    data.shareId = this.directory.target.id;
    const res = await kernel.createWorkDefine(data);
    if (res.success && res.data.id) {
      this.notify('replace', res.data);
    }
    return res.success;
  }
  async loadGatewayInfo(reload: boolean = false): Promise<schema.XWorkGateway[]> {
    if (this.gatewayInfo.length == 0 || reload) {
      const destId = this.canDesign
        ? this.directory.target.id
        : this.directory.target.spaceId;
      const res = await kernel.queryWorkGateways({
        defineId: this.id,
        targetId: destId,
      });
      if (res.success && res.data) {
        this.gatewayInfo = res.data.result || [];
      }
    }
    return this.gatewayInfo;
  }
  async deleteGateway(id: string): Promise<boolean> {
    const res = await kernel.deleteWorkGateway({ id });
    if (res.success) {
      this.gatewayInfo = this.gatewayInfo.filter((a) => a.id != id);
    }
    return res.success;
  }
  async bingdingGateway(
    nodeId: string,
    identity: schema.XIdentity,
    define: schema.XWorkDefine,
  ): Promise<schema.XWorkGateway | undefined> {
    const res = await kernel.createWorkGeteway({
      nodeId: nodeId,
      defineId: define.id,
      identityId: identity.id,
      targetId: this.directory.target.spaceId,
    });
    if (res.success) {
      this.gatewayInfo = this.gatewayInfo.filter((a) => a.nodeId != nodeId);
      this.gatewayInfo.push({ ...res.data, define, identity });
    }
    return res.data;
  }
  async loadNode(reload: boolean = false): Promise<model.WorkNodeModel | undefined> {
    if (this.node === undefined || reload) {
      const res = await kernel.queryWorkNodes({ id: this.id });
      if (res.success) {
        this.node = res.data;
        this.primaryPrints = [];
        this.primaryForms = [];
        this.detailForms = [];
        await this.recursionForms(this.node);
      }
    }
    return this.node;
  }
  async createApply(
    taskId: string = '0',
    pdata?: model.InstanceDataModel,
    _reload: boolean = true,
  ): Promise<IWorkApply | undefined> {
    await this.loadNode(_reload);
    if (this.node && this.forms.length > 0) {
      const data: model.InstanceDataModel = {
        data: {},
        fields: {},
        primary: {},
        node: this.node,
        rules: [],
        reception: pdata?.reception,
      };
      if (pdata?.primary) {
        Object.keys(pdata.primary).forEach((k) => {
          data.primary[k] = pdata.primary[k];
        });
      }
      this.forms.forEach((form) => {
        data.fields[form.id] = form.fields;
        if (pdata && pdata.data[form.id]) {
          const after = pdata.data[form.id]?.at(-1);
          if (after) {
            data.data[form.id] = [{ ...after, nodeId: this.node!.id }];
          }
        }
      });
      await this.createThing(data);
      return new WorkApply(
        {
          hook: '',
          taskId: taskId,
          title: this.name,
          defineId: this.id,
        } as model.WorkInstanceModel,
        data,
        this.directory.target,
        this.primaryPrints,
        this.primaryForms,
        this.detailForms,
        this.metadata.applyType,
        this.metadata,
      );
    }
  }
  async createThing(data: model.InstanceDataModel): Promise<void> {
    await this.loadNode();
    if (this.node) {
      for (const form of this.primaryForms) {
        if (!data.data[form.id]) {
          const res = await kernel.createThing(this.directory.spaceId, [], form.name);
          if (res.success && Array.isArray(res.data) && res.data.length > 0) {
            const item = res.data[0];
            await setDefaultField(item, form.fields, this.directory.target.space);
            data.data[form.id] = [
              {
                before: [],
                after: [item],
                rules: [],
                formName: form.name,
                creator: this.directory.userId,
                createTime: formatDate(new Date(), 'yyyy-MM-dd HH:mm:ss.S'),
                nodeId: this.node.id,
              },
            ];
          }
        }
      }
    }
  }

  async applyData(
    node: model.WorkNodeModel,
    items: schema.XProduct[],
  ): Promise<model.InstanceDataModel> {
    const instance: model.InstanceDataModel = {
      data: {},
      node: node,
      fields: {},
      primary: {},
      rules: [],
    };
    const fields = [
      'id',
      'code',
      'name',
      'chainId',
      'belongId',
      'createUser',
      'updateUser',
      'updateUser',
      'updateTime',
    ];
    for (const form of this.detailForms) {
      instance.fields[form.id] = await form.loadFields();
      instance.data[form.id] = [
        {
          nodeId: node.id,
          formName: form.name,
          before: [],
          after: items.map((item) => {
            const data: any = {};
            for (const field of fields) {
              if (item[field]) {
                data[field] = item[field];
              }
            }
            for (const field of form.fields) {
              if (item[field.code]) {
                data[field.id] = item[field.code];
              }
            }
            return data;
          }),
          creator: this.userId,
          createTime: formatDate(new Date(), 'yyyy-MM-dd HH:mm:ss.S'),
          rules: [],
        },
      ];
    }
    return instance;
  }
  override operates(): model.OperateModel[] {
    const operates = [workOperates.Distribute, ...super.operates()];
    if (this.isInherited) {
      operates.push({ sort: 3, cmd: 'workForm', label: '查看表单', iconType: 'newForm' });
    }
    if (this.userId != this.spaceId && this.target.space.hasRelationAuth()) {
      if (this.companyCache.tags?.includes('常用')) {
        operates.unshift(fileOperates.DelCompanyCommon);
      } else {
        operates.unshift(fileOperates.SetCompanyCommon);
      }
    }
    if (this.cache.tags?.includes('常用')) {
      operates.unshift(fileOperates.DelCommon);
    } else {
      operates.unshift(fileOperates.SetCommon);
    }
    if (this.metadata.hasGateway) {
      operates.push({
        sort: 4,
        cmd: 'fillWork',
        label: '关联我的办事',
        iconType: '办事',
      });
    }
    if (operates.includes(entityOperates.Delete)) {
      operates.push(entityOperates.HardDelete);
    }
    return operates
      .filter((i) => i != fileOperates.Download)
      .filter((i) => i != entityOperates.Delete);
  }
  private async recursionForms(node: model.WorkNodeModel) {
    if (node.resource) {
      const resource = JSON.parse(node.resource);
      if (Array.isArray(resource)) {
        node.forms = resource;
        node.formRules = [];
        node.executors = [];
      } else {
        node.forms = resource.forms ?? [];
        node.print = resource.print ?? [];
        node.formRules = resource.formRules ?? [];
        node.executors = resource.executors ?? [];
        node.buttons = resource.buttons ?? [];
      }
    }

    const formMap = (node.forms || []).reduce<Dictionary<FormInfo>>((a, v) => {
      a[v.id] = v;
      return a;
    }, {});
    node.detailForms = await this.directory.resource.formColl.find(
      node.forms?.filter((a) => a.typeName == '子表').map((s) => s.id),
    );
    node.detailForms = _.orderBy(node.detailForms, (a) => formMap[a.id].order);
    node.primaryForms = await this.directory.resource.formColl.find(
      node.forms?.filter((a) => a.typeName == '主表').map((s) => s.id),
    );
    node.primaryPrints = await this.directory.resource.printColl.find(
      node.print?.map((s) => s.id),
    );
    for (const a of node.primaryPrints) {
      const print = new Print({ ...a, id: a.id + '_' }, this.directory);
      await print.loadFields();
      this.primaryPrints.push(print);
    }
    node.primaryForms = _.orderBy(node.primaryForms, (a) => formMap[a.id].order);

    for (const a of node.primaryForms) {
      const form = new Form({ ...a, id: a.id + '_' }, this.directory);
      await form.loadFields();
      this.primaryForms.push(form);
    }
    for (const a of node.detailForms) {
      const form = new Form({ ...a, id: a.id + '_' }, this.directory);
      await form.loadFields();
      this.detailForms.push(form);
    }
    if (node.children) {
      await this.recursionForms(node.children);
    }
    if (node.branches) {
      for (const branch of node.branches) {
        if (branch.children) {
          await this.recursionForms(branch.children);
        }
      }
    }
  }
  notify(operate: string, data: any): void {
    this.application.notify(operate, {
      ...data,
      typeName: '办事',
      applicationId: this.application.metadata.id,
      directoryId: this.application.metadata.directoryId,
    });
  }
  receive(operate: string, data: schema.XWorkDefine): boolean {
    if (operate === 'replace' && data && data.id === this.id) {
      this.setMetadata(fullDefineRule(data, this.application.target));
      this.loadContent(true).then(() => {
        this.changCallback();
      });
    }
    return true;
  }
}
