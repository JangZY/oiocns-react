import { ReceptionChangeExecutor } from '@/ts/base/model';
import { Executor } from '.';
import { IReception, Reception } from '../assign/reception';
import { IBelong } from '../..';
import { XReception } from '@/ts/base/schema';

export class ReceptionChange extends Executor<ReceptionChangeExecutor> {

  findSpace() {
    const belongs: IBelong[] = [this.task.user.user!, ...this.task.user.user!.companys];
    return belongs.find(t => t.id == this.task.taskdata.applyId);
  }

  async createReception(reception: XReception, belong: IBelong): Promise<IReception | undefined> {
    let svc = new Reception(reception, belong);
    // 找出最新的reception，流程实例里面的是旧的
    let [newReception] = await svc.publicReceptionColl?.find([reception.id]) ?? [];
    if (!newReception) {
      console.warn('找不到最新的任务接收，可能已被删除');
    } else {
      svc = new Reception(newReception, belong);
    }
    return svc;
  }

  async execute(): Promise<boolean> {
    const { reception, data } = this.task.instanceData!;
    if (!reception) {
      return false;
    }
    const belong = this.findSpace();
    if (belong) {
      const svc = await this.createReception(reception, belong);
      if (!svc) {
        return false;
      }

      const thingId: Dictionary<string[]> = {};
      for (const [formId, value] of Object.entries(data)) {
        thingId[formId] = value.at(-1)!.after.map((d) => d.id);
      }
      await svc.complete(thingId);
      return true;
    }
    return false;
  }

  async reject(): Promise<boolean> {
    const { reception } = this.task.instanceData!;
    if (!reception) {
      return false;
    }
    const belong = this.findSpace();
    if (belong) {
      const svc = await this.createReception(reception, belong);
      if (!svc) {
        return false;
      }

      await svc.reject();
      return true;
    }
    return false;
  }
}
