import _ from 'lodash';
import { ITarget, XCollection } from '../../..';
import { model, schema } from '../../../../base';
import { WithChildren } from '../../../../base/common';
import { ReportTreeTypes } from '../../../../base/enum';
import { IDirectory } from '../../directory';
import { IStandardFileInfo, StandardFileInfo } from '../../fileinfo';
import { ReportTreeGenerator } from './ReportTreeGenerator';
import { getTreeNodeType, treeTypeNames } from './consts';
import { IReceptionProvider } from '@/ts/core/work/assign/reception/IReceptionProvider';
import { getAllNodes } from '@/ts/base/common/tree';
import { TaskSnapshotInfo } from '@/ts/base/model';
import {
  ReportTaskTreeSummary,
  getEmptySummary,
  getStatus,
} from '@/ts/core/work/assign/reception/status';
import { nextTick } from '@/ts/base/common/timer';
import { formatDate } from '@/utils';
import {
  XDistribution,
  XReportTaskTree,
  XReportTaskTreeNode,
  XReportTree,
  XReportTreeNode,
} from '@/ts/base/schema';

export interface IReportTree extends IStandardFileInfo<XReportTree> {
  /** 报表树节点 */
  nodes: XReportTreeNode[];
  /** 报表树类型 */
  treeType: ReportTreeTypes;
  readonly nodeColl: XCollection<XReportTreeNode>;
  /** 加载节点 */
  loadNodes(reload?: boolean): Promise<XReportTreeNode[]>;
  /** 新增节点 */
  createNode(data: XReportTreeNode): Promise<XReportTreeNode | undefined>;
  /** 删除节点 */
  deleteNode(node: XReportTreeNode): Promise<boolean>;
  /** 硬删除节点 */
  hardDeleteNode(node: XReportTreeNode, recursive?: boolean): Promise<boolean>;
  /** 清空所有节点 */
  clearAllNodes(): Promise<boolean>;
  /** 更新节点 */
  updateNode(prev: XReportTreeNode, next: XReportTreeNode): Promise<boolean>;
  /** 根据组织分类生成节点 */
  generateNodes(
    target: ITarget,
    progress?: (value: number | Error) => void,
  ): Promise<void>;
  /** 导入生成节点 */
  importNodes(
    data: WithChildren<model.ReportTreeNode>[],
    total: number,
    progress?: (value: number | Error) => void,
  ): Promise<void>;
  /** 克隆（可能是其它树的）节点 */
  cloneNodes(
    data: WithChildren<XReportTreeNode>[],
    total: number,
    progress?: (value: number | Error) => void,
  ): Promise<void>;
  /** 加载子树 */
  loadSubTree(rootNode: XReportTreeNode): Promise<model.ReportTreeNodeView[]>;
  /** 加载任务分发树 */
  loadDistributionTree(
    rootNode: XReportTreeNode,
    dist: IReceptionProvider,
    onlyDirectChildren?: boolean,
  ): Promise<[model.ReportTaskTreeNodeView[], ReportTaskTreeSummary]>;
  /** 判断有没有下级 */
  hasChildren(node: XReportTreeNode): Promise<boolean>;
}

abstract class ReportTreeBase<T extends XReportTree, N extends XReportTreeNode>
  extends StandardFileInfo<T>
  implements IReportTree
{
  constructor(_metadata: T, _directory: IDirectory, coll: XCollection<T>) {
    super(
      {
        ..._metadata,
        typeName: '报表树',
      },
      _directory,
      coll,
    );
  }

  canDesign = true;
  nodes: N[] = [];
  protected _itemLoaded: boolean = false;
  get cacheFlag(): string {
    return 'reporttrees';
  }
  get treeType() {
    return this.metadata.treeType;
  }

  abstract get nodeColl(): XCollection<N>;

  get groupTags(): string[] {
    const tags = [...super.groupTags];
    const treeTypeName = treeTypeNames[this.metadata.treeType];
    if (treeTypeName) {
      tags.push(treeTypeName);
    } else {
      tags.push(`未知类型 ${this.metadata.treeType || ''} 树`);
    }
    return tags;
  }
  async loadContent(reload: boolean = false): Promise<boolean> {
    await this.loadNodes(reload);
    return true;
  }
  async loadNodes(reload: boolean = false): Promise<N[]> {
    if (!this._itemLoaded || reload) {
      const res: N[] = [];
      while (true) {
        const data = await this.nodeColl.loadSpace({
          options: {
            match: {
              treeId: this.id,
            },
            sort: {
              code: 1,
            },
          },
          skip: res.length,
          take: 5000,
        });
        if (data.length == 0) {
          break;
        }
        res.push(...data);
      }
      this._itemLoaded = true;
      this.nodes = res || [];
    }
    return this.nodes;
  }
  async createNode(data: N): Promise<N | undefined> {
    data = _.omit(data, ['parent', 'children']) as any;
    const res = await this.nodeColl.insert({
      ...data,
      treeId: this.id,
      typeName: '报表树节点',
      nodeType: getTreeNodeType(data.nodeTypeName),
    });
    if (res) {
      await this.recorder.creating({
        coll: this.nodeColl,
        next: res,
      });
      this.nodes.push(res);
    }
    return res;
  }
  async deleteNode(node: N): Promise<boolean> {
    const success = await this.nodeColl.delete(node);
    if (success) {
      await this.recorder.updating({
        coll: this.nodeColl,
        prev: node,
        next: { ...node, isDeleted: true } as any,
      });
      this.nodes = this.nodes.filter((i) => i.id != node.id);
    }
    return success;
  }
  async hardDeleteNode(node: N, recursive = false): Promise<boolean> {
    const recursiveDelete = async (nodes: WithChildren<N>[]) => {
      let count = nodes.length;
      await this.nodeColl.removeMany(nodes);
      for (const node of nodes) {
        await recursiveDelete(node.children);
        count += node.children.length;
      }
      return count;
    };

    if (recursive) {
      const tree = await this.loadSubTree(node);
      const count = await recursiveDelete(tree);
      await this.loadNodes(true);
      return count > 0;
    } else {
      const success = await this.nodeColl.remove(node);
      if (success) {
        await this.recorder.deleting({
          coll: this.nodeColl,
          next: node,
          notify: true,
        });
        this.nodes = this.nodes.filter((i) => i.id != node.id);
      }
      return success;
    }
  }

  async clearAllNodes() {
    const nodes = await this.loadNodes(true);
    const success = await this.directory.resource.reportTreeNodeColl.removeMatch({
      treeId: this.id,
    });
    if (success) {
      await this.recorder.batchDeleting({
        coll: this.nodeColl,
        next: nodes,
        notify: true,
      });
      this.nodes = [];
    }
    return success;
  }
  async updateNode(prev: N, next: N): Promise<boolean> {
    next = _.omit(next, ['parent', 'children']) as any;
    next.nodeType = getTreeNodeType(next.nodeTypeName);
    const res = await this.nodeColl.replace({
      ...next,
      treeId: this.id,
    });
    if (res) {
      await this.recorder.updating({ coll: this.nodeColl, prev: prev, next: res });
      const index = this.nodes.findIndex((i) => i.id === next.id);
      if (index > -1) {
        this.nodes[index] = res;
      }
      return true;
    }
    return false;
  }

  override async copy(destination: IDirectory): Promise<boolean> {
    if (this.isSameBelong(destination)) {
      const newMetaData = {
        ...this.metadata,
        directoryId: destination.id,
        sourceId: this.metadata.sourceId || this.id,
      };

      const uuid = formatDate(new Date(), 'yyyyMM');
      newMetaData.name = this.metadata.name + `-${uuid}`;
      newMetaData.code = this.metadata.code + uuid;
      return await this.duplicate(newMetaData, destination);
    }
    await super.copyTo(
      destination.id,
      destination.resource.reportTreeColl as XCollection<T>,
    );
    const items = await this.nodeColl.loadSpace({
      options: {
        match: {
          treeId: this.id,
        },
      },
    });
    await destination.resource.reportTreeNodeColl.replaceMany(items);
    return true;
  }

  async duplicate(newMetaData: T, destination: IDirectory): Promise<boolean> {
    newMetaData.id = 'snowId()';

    const data = await destination.resource.reportTreeColl.replace(newMetaData);
    if (!data) {
      return false;
    }

    // 递归克隆树节点
    await this.loadNodes(true);
    const root = this.nodes.find((n) => n.id == this.metadata.rootNodeId);
    if (!root) {
      return false;
    }
    const tree = await this.loadSubTree(root);

    const newTree = new ReportTree(data, destination);
    await newTree.cloneNodes(tree, this.nodes.length, () => {});

    // 设置根节点
    const [newRoot] = await destination.resource.reportTreeNodeColl.loadSpace({
      options: {
        match: {
          treeId: newTree.id,
          _or_: [{ parentId: '' }, { parentId: null }, { parentId: { _exists_: false } }],
        },
      },
    });
    newTree.metadata.rootNodeId = newRoot?.id;
    await destination.resource.reportTreeColl.replace(newTree.metadata);

    return await destination.resource.reportTreeColl.notity({
      data: newTree.metadata,
      operate: 'insert',
    });
  }

  async createTaskTree(dist: XDistribution, destination?: IDirectory): Promise<boolean> {
    destination ||= this.directory;
    const newMetaData: XReportTaskTree = {
      ...(_.omit(this.metadata, [
        'id',
        'name',
        'code',
        'createUser',
        'createTime',
        'updateUser',
        'updateTime',
      ]) as any),
      taskId: dist.taskId,
      period: dist.period,
      distId: dist.id,
      code: this.metadata.code + dist.period.replace('-', ''),
      name: this.metadata.name + ' ' + dist.period,
    };
    newMetaData.id = 'snowId()';

    const treeColl = destination.resource.genTargetColl<XReportTaskTree>(
      ReportTaskTree.getCollName('报表树'),
    );

    const data = await treeColl.replace(newMetaData);
    if (!data) {
      return false;
    }

    console.warn(data);

    // 递归克隆树节点
    await this.loadNodes(true);
    const root = this.nodes.find((n) => n.id == this.metadata.rootNodeId);
    if (!root) {
      return false;
    }
    const tree = await this.loadSubTree(root);

    const newTree = new ReportTaskTree(data, destination);
    await newTree.cloneNodes(tree, this.nodes.length, () => {});

    // 设置根节点
    const [newRoot] = await newTree.nodeColl.loadSpace({
      options: {
        match: {
          treeId: newTree.id,
          _or_: [{ parentId: '' }, { parentId: null }, { parentId: { _exists_: false } }],
        },
      },
    });
    console.warn(newRoot);
    newTree.metadata.rootNodeId = newRoot?.id;
    await treeColl.replace(newTree.metadata);

    return true;
  }

  override async move(destination: IDirectory): Promise<boolean> {
    if (this.allowMove(destination)) {
      return await super.moveTo(
        destination,
        destination.resource.reportTreeColl as XCollection<T>,
      );
    }
    return false;
  }

  async generateNodes(target: ITarget, progress?: (value: number | Error) => void) {
    const g = new ReportTreeGenerator(this);
    const rootNodeId = await g.generateByTarget(target, progress);
    if (rootNodeId) {
      const tree = { ...this.metadata };
      tree.rootNodeId = rootNodeId;
      await this.update(tree);
    }
  }

  async importNodes(
    data: WithChildren<model.ReportTreeNode>[],
    total: number,
    progress?: (value: number | Error) => void,
  ) {
    const g = new ReportTreeGenerator(this);
    const rootNodeId = await g.importNodes(data, total, progress);
    if (rootNodeId) {
      const tree = { ...this.metadata };
      tree.rootNodeId = rootNodeId;
      await this.update(tree);
    }
  }
  async cloneNodes(
    data: WithChildren<XReportTreeNode>[],
    total: number,
    progress?: (value: number | Error) => void,
  ) {
    const g = new ReportTreeGenerator(this);
    await g.cloneNodes(data, total, progress);
  }

  async loadSubTree(node: N): Promise<WithChildren<N>[]> {
    const nodes = await this.loadNodes();
    const nodeMap = nodes.reduce<Dictionary<WithChildren<N>>>((a, v) => {
      a[v.id] = { ...v, children: [] };
      return a;
    }, {});

    const root = nodeMap[node.id];
    if (!root) {
      console.warn(`找不到节点 ${node.id}(${node.name})`);
      return [];
    }

    const pool = Object.values(nodeMap);
    for (const node of pool) {
      const parent = nodeMap[node.parentId!];
      if (!parent) {
        continue;
      }
      parent.children.push(node);
    }
    return [root];
  }
  async hardDelete(notify?: boolean): Promise<boolean> {
    await this.clearAllNodes();
    return await super.hardDelete(notify);
  }
  async loadDistributionTree(
    parentNode: N,
    dist: IReceptionProvider,
    onlyDirectChildren = false,
  ): Promise<[model.ReportTaskTreeNodeView[], ReportTaskTreeSummary]> {
    const summary: ReportTaskTreeSummary = getEmptySummary();

    let nodes: N[] = await this.nodeColl.loadSpace({
      options: {
        match: { parentId: parentNode.id },
      },
    });

    if (!onlyDirectChildren && nodes.length > 0) {
      nodes = await this.loadNodes();
    } else {
      nodes.unshift(parentNode);
    }

    const nodeMap = nodes.reduce<Dictionary<model.ReportTaskTreeNodeView>>((a, v) => {
      a[v.id] = {
        ...v,
        children: [],
        count: 0,
        isLeaf: true,
      };
      return a;
    }, {});

    const root = nodeMap[parentNode.id];
    if (!root) {
      console.warn(`找不到节点 ${parentNode.id}(${parentNode.name})`);
      return [[], summary];
    }

    // 仅查找没有status的reception
    const noStatusNodeIds = nodes
      .filter((n: XReportTaskTreeNode) => !n.taskStatus)
      .map((n) => n.id);
    const receptionMap = await dist.findReportReceptions(noStatusNodeIds);

    const pool = Object.values(nodeMap);
    for (const node of pool) {
      node.reception = receptionMap[node.id];

      const parent = nodeMap[node.parentId!];
      if (!parent) {
        continue;
      }
      parent.children.push(node);
    }

    await nextTick();
    // 筛选子树的情况
    const subNodes = getAllNodes([root]);
    for (const node of subNodes) {
      node.isLeaf = node.children.length == 0;
      summary.total++;
      const status = node.taskStatus || getStatus(node.reception);
      summary[status]++;
    }
    return [[root], summary];
  }

  async hasChildren(node: N): Promise<boolean> {
    const count = await this.nodeColl.count({
      options: {
        match: { parentId: node.id },
      },
    });
    return count > 0;
  }
}

export class ReportTree extends ReportTreeBase<XReportTree, XReportTreeNode> {
  constructor(_metadata: XReportTree, _directory: IDirectory) {
    super(_metadata, _directory, _directory.resource.reportTreeColl);
  }

  get nodeColl() {
    return this.directory.resource.reportTreeNodeColl;
  }
}

export class ReportTaskTree extends ReportTreeBase<XReportTaskTree, XReportTaskTreeNode> {
  static getCollName(typeName: '报表树节点' | '报表树') {
    if (typeName == '报表树节点') {
      return 'work-report-task-tree-node';
    } else {
      return 'work-report-task-tree';
    }
  }

  constructor(_metadata: XReportTaskTree, _directory: IDirectory) {
    const treeColl = _directory.resource.genTargetColl<XReportTaskTree>(
      ReportTaskTree.getCollName('报表树'),
    );
    super(_metadata, _directory, treeColl);
    this._nodeColl = _directory.resource.genTargetColl<XReportTaskTreeNode>(
      ReportTaskTree.getCollName('报表树节点'),
    );
    this.info = {
      taskId: _metadata.taskId,
      period: _metadata.period,
    };
  }

  readonly info: TaskSnapshotInfo;
  private readonly _nodeColl: XCollection<XReportTaskTreeNode>;
  get nodeColl() {
    return this._nodeColl;
  }
}
