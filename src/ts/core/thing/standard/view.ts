import { IForm, Form } from './form';
enum ViewType {
  Form = '表单',
  Chart = '图表',
}
export interface IView extends IForm {
  //打开方式
  viewType: ViewType;
}
//TODO:从系统文件类型继承 存储进入 视图数据集
// export class View extends StandardFileInfo<schema.XForm> implements IView
export class View extends Form implements IView {
  public get typeName(): string {
    return '视图';
  }

  get viewType(): ViewType {
    return this.metadata.viewType ?? '表单';
  }

  updateViewType(type: ViewType): void {
    super.update({ ...this.metadata, viewType: type });
  }
}
