import {
  IApplication,
  IPrint,
  IFile,
  IPerson,
  ISession,
  ITarget,
  TargetType,
  DataProvider,
} from '@/ts/core';
import { common } from '@/ts/base';
import { IWorkProvider } from '../core/work/provider';
import { IPageTemplate } from '../core/thing/standard/page';
import { AuthProvider } from '../core/provider/auth';
import { IReception } from '../core/work/assign/reception';
import { HomeProvider } from '../core/provider/home';
import { XPrint } from '@/ts/base/schema';
/** 控制器基类 */
export class Controller extends common.Emitter {
  public currentKey: string;
  constructor(key: string) {
    super();
    this.currentKey = key;
  }
}
/**
 * 设置控制器
 */
class IndexController extends Controller {
  static _data: DataProvider;
  constructor() {
    super('');
  }
  /** 是否已登录 */
  get logined(): boolean {
    return this.data.user != undefined;
  }
  /** 数据提供者 */
  get data(): DataProvider {
    if (IndexController._data === undefined) {
      IndexController._data = new DataProvider(this);
    }
    return IndexController._data;
  }
  /** 授权方法 */
  get auth(): AuthProvider {
    return this.data.auth;
  }
  /** 当前用户 */
  get user(): IPerson {
    return this.data.user!;
  }
  /** 办事提供者 */
  get work(): IWorkProvider {
    return this.data.work!;
  }
  /** 主页提供者 */
  get home(): HomeProvider {
    return this.data.home!;
  }
  /** 所有相关的用户 */
  get targets(): ITarget[] {
    return this.data.targets;
  }
  /** 退出 */
  exit(): void {
    sessionStorage.clear();
    IndexController._data = new DataProvider(this);
  }
  async loadApplications(): Promise<IApplication[]> {
    const apps: IApplication[] = [];
    for (const directory of this.targets
      .filter((i) => i.session.isMyChat && i.typeName != TargetType.Group)
      .map((a) => a.directory)) {
      apps.push(...(await directory.loadAllApplication()));
    }
    return apps;
  }
  async loadPrint(): Promise<XPrint[]> {
    const apps: XPrint[] = [];
    for (const directory of this.targets
      .filter((i) => i.session.isMyChat && i.typeName != TargetType.Group)
      .map((a) => a.directory)) {
      apps.push(...(await directory.directory.resource.printColl.all()));
    }
    return apps;
  }
  async loadAllIPrint(): Promise<IPrint[]> {
    const apps: IPrint[] = [];
    for (const directory of this.targets
      .filter((i) => i.session.isMyChat && i.typeName != TargetType.Group)
      .map((a) => a.directory)) {
      apps.push(...(await directory.loadAllPrints()));
    }
    return apps;
  }
  async loadAllDefaultIPrint(belongId: string): Promise<string[]> {
    const ids = [];
    for (const directory of this.targets
      .filter((i) => i.session.isMyChat && i.typeName != TargetType.Group)
      .map((a) => a.directory)) {
      if (directory.belongId == belongId) {
        const defaultPrint = directory.children.filter(
          (item) =>
            !item.groupTags.includes('已删除') && item.name.includes('预算一体化'),
        );
        const defaultPrintContent = defaultPrint[0].children.filter((item) =>
          item.name.includes('打印模板'),
        );
        ids.push(defaultPrint[0].id, defaultPrintContent[0].id);
      }
    }
    return ids && ids;
  }
  /** 加载所有常用 */
  async loadCommons(): Promise<IFile[]> {
    if (this.home) {
      return this.home.loadCommons(this.user);
    }
    return [];
  }
  /** 所有相关会话 */
  get chats(): ISession[] {
    const chats: ISession[] = [];
    if (this.data.user) {
      chats.push(...this.data.user.chats);
      for (const company of this.data.user.companys) {
        chats.push(...company.chats);
      }
    }
    return chats;
  }
  /** 所有相关页面 */
  async loadPages(): Promise<IPageTemplate[]> {
    const pages: IPageTemplate[] = [];
    for (const directory of this.targets.map((t) => t.directory)) {
      const templates = await directory.loadAllTemplate();
      pages.push(...templates.filter((item) => item.metadata.public));
    }
    return pages;
  }
  /** 加载所有接收任务 */
  async loadReceptions(): Promise<IReception[]> {
    const tasks: IReception[] = [];
    for (const belong of [this.user, ...this.user.companys]) {
      tasks.push(...(await belong.loadTasks()));
    }
    return tasks;
  }
}

export default new IndexController();
