import { ValidateRefGraph } from '../graph';
import { GraphNode } from '../graph/node';
import FormServiceBase from '../services/FormServiceBase';
import { ValidateResult } from '../types/rule';
import { IService } from '../types/service';
import {
  RequiredValidateError,
  RuleValidateError,
  ValidateErrorInfo,
} from '@/ts/base/model';
import { AssertResult } from '../util/AssertResult';

export default class ValidateHandler implements IService {
  readonly service: FormServiceBase;

  readonly graph: ValidateRefGraph;

  constructor(service: FormServiceBase, allowEdit = true) {
    this.service = service;
    this.graph = new ValidateRefGraph(service, allowEdit);
  }

  dispose() {
    this.graph.nodeList = {};
  }

  init() {
    try {
      this.graph.init();
      return true;
    } catch (error) {
      (this as any).graph = new ValidateRefGraph(this.service);
      return error as Error;
    }
  }

  /**
   * 全部校验
   * @returns 校验结果
   */
  validateAll() {
    let errors: ValidateErrorInfo[] = this.validateRequired();
    for (const node of this.graph) {
      try {
        const result = this.validateNode(node);
        if (result.length > 0) {
          errors.push(...result);
        }
      } catch (error) {
        console.error(error);
        const message = error instanceof Error ? error.message : String(error);
        errors.push({
          errorLevel: 'error',
          errorCode: 'ERR_FAILED',
          message: '校验执行失败：' + message,
          position: node.label,
        });
      }
    }
    return errors;
  }

  validateNode(node: GraphNode) {
    if (node.type != 'validate') {
      return [];
    }

    let errors: ValidateErrorInfo[] = [];

    const ctx = {
      ...this.service.createContext(node, this.graph),
      ...this.service.createFormContext(),
    };
    const value = this.service.evalExpression<ValidateResult>(node.rule.formula, ctx);

    if (typeof value === 'boolean') {
      if (value === false) {
        errors.push({
          errorLevel: node.rule.errorLevel,
          errorCode: `ERR_VALIDATE_${node.formId || ''}_${node.name}`,
          message: node.rule.message,
          position: node.label,
          rule: node.rule,
          formId: node.formId,
        } as RuleValidateError);
      }
    } else if (typeof value === 'string') {
      if (value) {
        errors.push({
          errorLevel: node.rule.errorLevel,
          errorCode: `ERR_VALIDATE_${node.formId || ''}_${node.name}`,
          message: value,
          position: node.label,
          rule: node.rule,
          formId: node.formId,
        } as RuleValidateError);
      }
    } else if (Array.isArray(value)) {
      errors = value;
    } else if (value instanceof AssertResult) {
      if (!value.success) {
        errors.push({
          errorLevel: node.rule.errorLevel,
          errorCode: `ERR_VALIDATE_${node.formId || ''}_${node.name}`,
          message: node.rule.message + value.message,
          position: node.label,
          rule: node.rule,
          formId: node.formId,
          expected: value.expected,
          actual: value.actual,
        } as RuleValidateError);        
      }
    } else if (value?.message) {
      errors.push(value);
    } else {
      console.warn('非法的校验结果：', value);
    }
    return errors;
  }

  private getHideForms = () => {
    return this.service.model.rules
      .filter((a) => a.typeName == 'visible' && !a.value && a.formId == a.destId)
      .map((a) => a.destId);
  };
  private validateRequired() {
    const valueIsNull = (value: any) => {
      return (
        value === null ||
        value === undefined ||
        (typeof value === 'string' && (value == '[]' || value.length < 1))
      );
    };
    const errors: RequiredValidateError[] = [];
    const hides = this.getHideForms();
    const forms = [
      ...this.service.model.node.primaryForms,
      ...this.service.model.node.detailForms,
    ];
    for (const formId of Object.keys(this.service.model.fields).filter(
      (a) => !hides.includes(a),
    )) {
      const formInfo = forms.find((item) => item.id == formId);
      const formData = this.service.model.data[formId]?.at(-1);
      const data: any = formData?.after.at(-1) ?? {};
      for (const item of this.service.model.fields[formId]) {
        var isRequired = item.options?.isRequired;
        if (formData?.rules && Array.isArray(formData?.rules)) {
          const rules = formData?.rules.filter((a) => a.destId == item.id);
          if (rules) {
            for (const rule of rules.filter((item) => item.typeName == 'isRequired')) {
              isRequired = rule.value;
            }
            for (const rule of rules.filter((item) => item.typeName == 'visible')) {
              isRequired = isRequired && rule.value;
            }
          }
        }
        if (isRequired && valueIsNull(data[item.id])) {
          errors.push({
            errorLevel: 'error',
            errorCode: `ERR_REQUIRED_${formId}_${item.id}`,
            formId,
            position: `${formInfo?.name || ''} — ${item.name}`,
            message: `字段 ${item.name} 必填`,
            field: item,
          });
        }
      }
    }
    return errors;
  }
}
