import { XForm, XReception } from '@/ts/base/schema';
import { FormContextData } from './rule';
import { FiledLookup } from '@/ts/base/model';

export interface FormInfo {
  id: string;
  code: string;
  typeName: string;
  isPrimaryForm: boolean;
  form: XForm;
}

export interface IFormDataHost {
  readonly formCodeMap: Dictionary<string>;
  readonly formData: FormContextData;
  readonly formInfo: Dictionary<FormInfo>;
  readonly speciesMap: Dictionary<FiledLookup[]>;
  readonly reception?: XReception;
}

export type FormType = '主表' | '子表';
export type DetailOperationType =
  | 'add'
  | 'update'
  | 'remove'
  | 'detail'
  | 'printEntity'
  | 'copy';

export interface IService {
  init(): boolean | Error;
  dispose(): void;
}
