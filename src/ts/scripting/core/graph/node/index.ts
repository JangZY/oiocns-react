import AttributeRefNode from './AttributeRefNode';
import CalcRuleNode from './CalcRuleNode';
import CodeRuleNode from './CodeRuleNode';
import FormRefNode from './FormRefNode';
import ValidateRuleNode from './ValidateRuleNode';

interface TypeMap {
  calc: CalcRuleNode;
  attr: AttributeRefNode;
  code: CodeRuleNode;
  validate: ValidateRuleNode;
  form: FormRefNode;
}

export type NodeType = keyof TypeMap;
export type GraphNode = TypeMap[NodeType];
