import _ from 'lodash';

export function min(array: number[]) {
  return _.min(array.filter((n) => typeof n === 'number'));
}
export function max(array: number[]) {
  return _.max(array.filter((n) => typeof n === 'number'));
}
export function sum(array: number[]) {
  return _.sum(array.filter((n) => typeof n === 'number'));
}
export function average(array: number[]) {
  return array.length === 0 ? 0 : _.sum(array) / array.length;
}
