/**
 * 求数字数组的最小值，如果数组为空则不存在
 * @param array 数字数组
 */
declare function min(array: number[]): number | undefined;
/**
 * 求数字数组的最大值，如果数组为空则不存在
 * @param array 数字数组
 */
declare function max(array: number[]): number | undefined;
/**
 * 求数字数组的和
 * @param array 数字数组
 */
declare function sum(array: number[]): number;
/**
 * 求数字数组的平均数
 * @param array 数字数组
 */
declare function average(array: number[]): number;
