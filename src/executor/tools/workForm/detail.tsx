import { command, kernel, model, parseAvatar, schema } from '../../../ts/base';
import { useEffect, useRef, useState } from 'react';
import React from 'react';
import { InputNumber, Modal, Tabs, message } from 'antd';
import { EditModal } from '../editModal';
import GenerateThingTable from '../generate/thingTable';
import { Uploader, generating, showErrors, matching } from '../uploadTemplate';
import * as el from '@/utils/excel';
import { XThing } from '@/ts/base/schema';
import { AiFillCopy, AiFillEdit, AiFillRest } from 'react-icons/ai';
import { deepClone, getAsyncTime } from '@/ts/base/common';
import WorkFormService from '@/ts/scripting/core/services/WorkFormService';
import { DetailOperationType } from '@/ts/scripting/core/types/service';
import _ from 'lodash';
import { Form } from '@/ts/core/thing/standard/form';
import OpenFileDialog from '@/components/OpenFileDialog';
import { Theme } from '@/config/theme';
import { MdPrint } from 'react-icons/md';
import { Workbook } from 'exceljs';
import saveAs from 'file-saver';
import { IBelong } from '@/utils/excel';
import { Sequence } from '@/ts/core/thing/standard/sequence';
import { FormChangeEvent } from '@/ts/scripting/core/types/rule';
interface FormProps {
  allowEdit: boolean;
  form: schema.XForm;
  info: model.FormInfo;
  isDone?: boolean;
  service: WorkFormService;
  node: model.WorkNodeModel;
}

const DetailTable: React.FC<FormProps> = (props) => {
  const form = props.form;
  const info = props.info;
  if (!props.service.model.fields[form.id]) return <></>;
  const lock = useRef(false);
  const fields = props.service.model.fields[form.id];
  const operateRule = {
    allowAdd: info?.allowAdd ?? true,
    allowEdit: info?.allowEdit ?? true,
    allowSelect: info?.allowSelect ?? true,
    allowSelectFile: info?.allowSelectFile ?? false,
  };
  const [formData, setFormData] = useState(
    props.service.getFormData(props.form, props.node.id, '子表'),
  );
  const [selectKeys, setSelectKeys] = useState<string[]>([]);
  const [differences, setDifferences] = useState<any[]>([]);
  const [center, setCenter] = useState(<></>);

  useEffect(() => {
    var after = formData?.after.at(-1);
    if (after) {
      after.name = props.form.name;
    }
    const model = props.service.model;
    const formId = props.form.id;
    if (model.data[formId]) {
      const temp = model.data[formId]?.filter((a) => a.nodeId != formData.nodeId) ?? {};
      model.data[formId] = [...temp, formData];
    } else {
      model.data[formId] = [formData];
    }
    props.service.onDetailDataChanged(props.form.id, 'all', model.data[formId][0].after);
    if (info?.allowShowChangeData) {
      if (
        (formData.after as any[])?.length > 0 &&
        (formData.before as any[])?.length > 0
      ) {
        let arr: any[] = [
          ...differences,
          ...findDifferentValues(formData.after as any[], formData.before as any[]),
        ];
        if (arr.length > 0) {
          let newArr = info.showChangeData?.map((item: any) => item.id) ?? [];
          newArr.filter((item) => !arr.includes(item));
          arr.push(...newArr);
        }

        setDifferences(arr);
      }
    }
  }, [formData]);

  useEffect(() => {
    const id = props.service?.command.subscribe((type, cmd, args) => {
      if (args.formId == props.form?.id) {
        switch (type) {
          case 'change':
            switch (cmd) {
              case 'result':
                switch (args.type) {
                  case 'add':
                    formData.after.push(args.value);
                    setFormData({ ...formData });
                    break;
                  case 'update':
                    formData.after = formData.after.map((item) => {
                      if (item.id === args.destId) {
                        return args.value;
                      } else {
                        return item;
                      }
                    });
                    setFormData({ ...formData });
                    break;
                }
                break;
              case 'combination':
                formData.after = args.data;
                setFormData({ ...formData });
                break;
              case 'assignment':
                formData.after = args.data;
                formData.before = args.data;
                setFormData({ ...formData });
                break;
            }
            break;
        }
      }
    });
    return () => {
      props.service?.command.unsubscribe(id!);
    };
  }, []);

  const findDifferentValues = (arr1: any, arr2: any) => {
    const result: any[] = []; // 存储结果的数组
    arr1.forEach((obj1: any) => {
      arr2.forEach((obj2: any) => {
        if (obj1.id === obj2.id) {
          const allKeys = new Set(Object.keys(obj1).concat(Object.keys(obj2)));
          allKeys.forEach((key) => {
            if (obj1[key] !== obj2[key] && key != 'name') {
              if (!result.includes(key)) {
                result.push(key);
              }
            }
          });
        }
      });
    });
    return result;
  };

  if (!formData) {
    return <></>;
  }

  async function addOrUpdate(type: 'add' | 'update', data?: XThing) {
    try {
      const formDataClone = _.cloneDeep(formData);
      const values = await EditModal.showFormEdit({
        form: form,
        fields: fields,
        belong: props.service.belong,
        create: type === 'add',
        initialValues: data,
        formData: formDataClone,
      });

      if (type === 'update') {
        const index = formData.after.findIndex((a) => a.id == values.id);
        console.assert(index >= 0, '找不到修改前的数据 ' + values.id);
        formData.after[index] = values;
      } else {
        formData.after.push(values);
      }
      setFormData({ ...formData });
    } catch (error) {
      if (error != 'close') {
        console.error(error);
      }
    }
  }

  async function setCopyData(
    thing: schema.XThing,
    fields: model.FieldModel[],
    belong: IBelong,
  ) {
    const changes: Omit<FormChangeEvent, 'formId'>[] = [];

    function onChange(destId: string, value: any) {
      changes.push({ destId, value });
    }
    for (const field of fields) {
      if (field.options?.asyncGeneateConditions?.length) {
        let ret = '';
        for (const rule of field.options.asyncGeneateConditions.sort(
          (pre: { order: any }, next: { order: any }) =>
            Number(pre.order) - Number(next.order),
        )) {
          switch (rule.encodeType.label) {
            case '常量':
              ret += rule.encodeValue;
              break;
            case '时间':
              ret += getAsyncTime(rule.dimensionalAccuracy.label);
              break;
            case '流水':
              var se = await new Sequence(rule.sequence, belong.directory).genValue();
              if (se == '') {
                console.error('生成序号失败!');
              }
              ret += se;
              break;
            default:
              break;
          }
        }
        thing[field.id] = ret;
        onChange(field.id, ret);
      }
    }
  }

  async function copyModal(data: XThing) {
    let num = 0;
    Modal.confirm({
      icon: <></>,
      okText: `确认`,
      cancelText: '关闭',
      onCancel: () => {
        close();
        num = 0;
      },
      content: (
        <div
          style={{
            maxHeight: '20vh',
            width: '100%',
          }}>
          复制数量：
          <InputNumber
            min={0}
            precision={0}
            max={100}
            placeholder="Outlined"
            style={{ width: 200 }}
            onChange={(e) => {
              num = parseInt(e);
            }}
            defaultValue={num}
          />
        </div>
      ),
      onOk: async () => {
        if (num > 0) {
          const res = await kernel.createThing(data?.belongId, [], data.name, num);
          res.data.forEach(async (item: XThing) => {
            await setCopyData(item, fields, props.service.belong);
            formData.before.unshift({ ...data, ...item });
            formData.after.unshift({ ...data, ...item });
            setFormData({ ...formData });
          });
          num = 0;
        }
      },
    });
  }

  async function updateModal(info: model.operationButtonInfo) {
    const metaForm = new Form(info.form?.metadata, props.service.belong.directory);
    const fields = await metaForm.loadFields();
    let FormProps = {
      form: info.form?.metadata,
      fields: fields,
      belong: props.service.belong,
      onSave: (values: schema.XThing[]) => {
        values.forEach((item) => {
          if (formData.after.every((i) => i.id !== item.id)) {
            formData.after.unshift(item);
          }
          if (formData.before.every((i) => i.id !== item.id)) {
            formData.before.unshift({ ...item });
          }
        });
        setFormData({ ...formData });
      },
    };
    if (info.type === 'choice') {
      EditModal.showFormSelect(FormProps);
    } else {
      EditModal.SummaryFormModal(FormProps);
    }
  }

  const showBtn = () => {
    let arr: any[] = [];
    if (info.operationButton && info.operationButton.length > 0) {
      info.operationButton.forEach((item) => {
        if (item?.scene !== 'mobile') {
          arr.push({
            name: 'add',
            location: 'after',
            widget: 'dxButton',
            options: {
              text: item.name,
              icon: 'add',
              onClick: async () => {
                await updateModal(item);
              },
            },
            visible: props.allowEdit,
          });
        }
      });
    }
    return arr;
  };

  const loadMenus = () => {
    if (props.allowEdit) {
      var items = [
        {
          key: 'remove',
          label: '移除',
          icon: <AiFillRest fontSize={22} />,
        },
      ];
      if (form.isCopy) {
        items.unshift({
          key: 'copy',
          label: '复制数据',
          icon: <AiFillCopy fontSize={22} />,
        });
      }
      if (operateRule.allowEdit) {
        items.unshift({
          key: 'update',
          label: '更新',
          icon: <AiFillEdit fontSize={22} />,
        });
      }
      return {
        items: items,
        async onMenuClick(key: string, data: XThing) {
          switch (key as DetailOperationType) {
            case 'update':
              addOrUpdate('update', data);
              break;
            case 'copy':
              copyModal(data);
              break;
            case 'remove':
              formData.before = formData.before.filter((i) => i.id != data.id);
              formData.after = formData.after.filter((i) => i.id != data.id);
              setSelectKeys([]);
              setFormData({ ...formData });
              break;
          }
        },
      };
    } else {
      return {
        items: [
          {
            key: 'detail',
            label: '详情',
            icon: <AiFillEdit fontSize={22} />,
          },
          {
            key: 'printEntity',
            label: '标签打印',
            icon: <MdPrint fontSize={22} color={Theme.FocusColor} />,
            hide: !props.isDone || !form?.print ? true : form.print?.config?.hide,
          },
        ],
        onMenuClick(key: string, data: XThing) {
          switch (key as DetailOperationType) {
            case 'detail':
              command.emitter(
                'executor',
                'open',
                {
                  ...data,
                  fields: fields,
                  formId: form.id,
                  typeName: '物详情',
                  key: data.id,
                },
                'preview',
              );
              break;
            case 'printEntity':
              command.emitter(
                'executor',
                'printEntity',
                { metadata: form, fields },
                'multiple',
                [data],
              );
              break;
          }
        },
      };
    }
  };
  return (
    <>
      <GenerateThingTable
        form={props.form}
        fields={fields}
        height={500}
        dataIndex={'attribute'}
        selection={
          props.allowEdit
            ? {
                mode: 'multiple',
                allowSelectAll: true,
                selectAllMode: 'allPages',
                showCheckBoxesMode: 'always',
              }
            : undefined
        }
        differences={differences}
        onSelectionChanged={(e) => setSelectKeys(e.selectedRowKeys)}
        onExporting={async (e) => {
          if (lock.current) {
            message.error('正在导出数据，请稍后再试!');
            return;
          }
          lock.current = true;
          try {
            if (e.format === 'xlsx') {
              e.component.beginCustomLoading('正在导出数据...');
              const options = e.component.getDataSource().loadOptions();
              const workbook = new Workbook();
              const worksheet = workbook.addWorksheet(form.name);
              const columns = e.component.getVisibleColumns();
              const filter = e.component.getCombinedFilter();
              worksheet.columns = e.component.getVisibleColumns().map((column) => {
                return { header: column.caption, key: column.dataField };
              });
              let skip = 0;
              let take = 500;
              let pass = true;
              while (pass) {
                const result: model.LoadResult<any> = (await e.component
                  .getDataSource()
                  .store()
                  .load({ skip, take, options, filter })) as any;
                if (!Array.isArray(result) || result.length == 0) {
                  pass = false;
                  break;
                } else {
                  let data: any = result;
                  if (e.selectedRowsOnly) {
                    const selectedRowKeys: string[] = e.component.getSelectedRowKeys();
                    data = data.filter((item) => selectedRowKeys.includes(item.id));
                  }
                  data.forEach((item: any) => {
                    for (const column of columns) {
                      const valueMap = (column.lookup as any)?.valueMap;
                      if (valueMap && column.dataField) {
                        item[column.dataField] = valueMap[item[column.dataField]];
                      }
                    }
                    worksheet.addRow(item);
                  });
                  skip += data.length;
                  if (data.length < take) {
                    pass = false;
                  }
                }
              }
              workbook.xlsx.writeBuffer().then((buffer) => {
                saveAs(
                  new Blob([buffer], { type: 'application/octet-stream' }),
                  form.name + '.xlsx',
                );
              });
              e.component.endCustomLoading();
            }
          } finally {
            lock.current = false;
          }
        }}
        toolbar={{
          visible: true,
          items: [
            ...showBtn(),
            {
              name: 'add',
              location: 'after',
              widget: 'dxButton',
              options: {
                text: '新增',
                icon: 'add',
                onClick: () => {
                  addOrUpdate('add');
                },
              },
              visible: props.allowEdit && operateRule['allowAdd'],
            },
            {
              name: 'import',
              location: 'after',
              widget: 'dxButton',
              options: {
                text: '导入匹配',
                icon: 'add',
                onClick: async () => {
                  const values = deepClone(fields);
                  values.unshift({
                    id: 'id',
                    name: '唯一标识',
                    code: 'id',
                    valueType: '描述型',
                    remark: '唯一标识',
                  });
                  const excel = new el.Excel(
                    props.service.belong,
                    el.getAnythingSheets(
                      form,
                      values.filter((i) => i.name === form.matchImport || ''),
                      'code',
                    ),
                  );
                  const modal = Modal.info({
                    icon: <></>,
                    okText: '关闭',
                    width: 610,
                    title: form.name + '导入',
                    className: 'uploader-model',
                    maskClosable: true,
                    content: (
                      <Uploader
                        templateName={form.name}
                        excel={excel}
                        fields={fields}
                        finished={(_, errors) => {
                          modal.destroy();
                          if (errors.length > 0) {
                            showErrors(errors);
                            return;
                          }
                          matching(
                            props.service.belong,
                            form,
                            excel.handlers[0].sheet.data,
                            formData,
                            fields,
                            () => setFormData({ ...formData }),
                          );
                        }}
                      />
                    ),
                  });
                },
              },
              visible:
                props.allowEdit &&
                operateRule['allowSelect'] &&
                fields.filter((i) => i.name === form.matchImport || '').length > 0,
            },
            {
              name: 'selectFile',
              location: 'after',
              widget: 'dxButton',
              options: {
                text: '选择文件',
                icon: 'add',
                onClick: async () => {
                  setCenter(
                    <OpenFileDialog
                      accepts={['目录']}
                      rootKey={props.service.belong.directory.key}
                      multiple
                      onOk={(files) => {
                        files.forEach((item) => {
                          let thing = item.metadata as any as schema.XThing;
                          if (formData.after.every((i) => i.id !== item.id)) {
                            thing = { ...thing };
                            let avatar = parseAvatar(thing.icon);
                            if (avatar) {
                              thing.icons = JSON.stringify([avatar]);
                            }
                            for (const key of Object.keys(thing)) {
                              if (Array.isArray(thing[key])) {
                                thing[key] = undefined;
                              }
                            }
                            console.log(thing);
                            formData.after.unshift({ ...thing });
                          }
                        });
                        setFormData({ ...formData });
                        setCenter(<></>);
                      }}
                      onCancel={() => setCenter(<></>)}
                    />,
                  );
                },
              },
              visible: props.allowEdit && operateRule.allowSelectFile,
            },
            {
              name: 'import',
              location: 'after',
              widget: 'dxButton',
              options: {
                text: '导入新增',
                icon: 'add',
                onClick: async () => {
                  const values = deepClone(fields);
                  values.unshift({
                    id: 'id',
                    name: '唯一标识',
                    code: 'id',
                    valueType: '描述型',
                    remark: '唯一标识',
                  });
                  const excel = new el.Excel(
                    props.service.belong,
                    el.getAnythingSheets(form, values, 'id'),
                  );
                  const modal = Modal.info({
                    icon: <></>,
                    okText: '关闭',
                    width: 610,
                    className: 'uploader-model',
                    title: form.name + '导入',
                    maskClosable: true,
                    content: (
                      <Uploader
                        templateName={form.name}
                        excel={excel}
                        belondId={form.shareId}
                        finished={(_, errors, conditions) => {
                          modal.destroy();
                          if (errors.length > 0) {
                            showErrors(errors);
                            return;
                          }
                          generating(
                            props.service.belong,
                            form.name,
                            fields,
                            excel.handlers[0].sheet.data,
                            formData,
                            () => setFormData({ ...formData }),
                            conditions,
                          );
                        }}
                        type="add"
                      />
                    ),
                  });
                },
              },
              visible: props.allowEdit && operateRule['allowAdd'],
            },
            {
              name: 'edit',
              location: 'after',
              widget: 'dxButton',
              options: {
                text: '变更',
                icon: 'edit',
                onClick: async () => {
                  const values = await EditModal.showFormEdit({
                    form: form,
                    fields: fields,
                    belong: props.service.belong,
                    create: false,
                  });

                  formData.after = formData.after.map((item) => {
                    if (selectKeys.includes(item.id)) {
                      Object.keys(values).forEach((k) => {
                        item[k] = values[k];
                      });
                    }
                    return item;
                  });
                  setFormData({ ...formData });
                },
              },
              visible:
                props.allowEdit && operateRule['allowEdit'] && selectKeys.length > 0,
            },
            {
              name: 'select',
              location: 'after',
              widget: 'dxButton',
              options: {
                text: '选择',
                icon: 'bulletlist',
                onClick: () => {
                  EditModal.showFormSelect({
                    form: form,
                    fields: fields,
                    belong: props.service.belong,
                    onSave: (values) => {
                      values.forEach((item) => {
                        if (formData.after.every((i) => i.id !== item.id)) {
                          formData.after.unshift(item);
                        }
                        if (formData.before.every((i) => i.id !== item.id)) {
                          formData.before.unshift({ ...item });
                        }
                      });
                      setFormData({ ...formData });
                    },
                  });
                },
              },
              visible: props.allowEdit && operateRule['allowSelect'],
            },
            {
              name: 'remove',
              location: 'after',
              widget: 'dxButton',
              options: {
                text: '移除',
                icon: 'remove',
                onClick: () => {
                  formData.before = formData.before.filter(
                    (i) => !selectKeys.includes(i.id),
                  );
                  formData.after = formData.after.filter(
                    (i) => !selectKeys.includes(i.id),
                  );
                  setSelectKeys([]);
                  setFormData({ ...formData });
                },
              },
              visible: props.allowEdit && selectKeys.length > 0,
            },
            {
              name: 'exportButton',
              location: 'after',
            },
            {
              name: 'print',
              location: 'after',
              widget: 'dxButton',
              visible:
                props.isDone &&
                form?.print &&
                (form.print.config?.hide === void 0 || form.print.config?.hide === false),
              options: {
                icon: 'print',
                onClick: () => {
                  if (!form.print) return message.error('请先配置打印模板');
                  if (formData.after.length === 0)
                    return message.error('请选择需要打印的数据');
                  command.emitter(
                    'executor',
                    'printEntity',
                    { metadata: form, fields },
                    'multiple',
                    formData.after,
                  );
                },
              },
            },
            {
              name: 'columnChooserButton',
              location: 'after',
            },
            {
              name: 'searchPanel',
              location: 'after',
            },
          ],
        }}
        dataMenus={loadMenus()}
        dataSource={formData.after}
        beforeSource={formData.before}
      />
      {center}
    </>
  );
};

interface IProps {
  service: WorkFormService;
  node: model.WorkNodeModel;
  isDone?: boolean;
  splitDetailFormId?: string;
}

const DetailForms: React.FC<IProps> = (props) => {
  if (props.node.detailForms.length < 1) return <></>;
  const [activeTabKey, setActiveTabKey] = useState(
    props.splitDetailFormId || props.node.detailForms[0].id,
  );
  const loadItems = () => {
    const items = [];
    const detailForms = props.splitDetailFormId
      ? props.node.detailForms.filter((i) => i.id === props.splitDetailFormId)
      : props.node.detailForms;
    for (const form of detailForms) {
      let info =
        props.node.forms.find((item) => item.id == form.id) ?? ({} as model.FormInfo);
      if (
        props.service.model.rules?.find(
          (a) => a.destId == form.id && a.typeName == 'visible' && !a.value,
        )
      ) {
        continue;
      }
      items.push({
        key: form.id,
        forceRender: true,
        label: form.name,
        children: (
          <DetailTable
            allowEdit={props.service.allowEdit}
            info={info}
            form={form}
            isDone={props.isDone}
            node={props.node}
            service={props.service}
          />
        ),
      });
    }
    return items;
  };
  return (
    <Tabs
      items={loadItems()}
      activeKey={activeTabKey}
      onChange={(key) => setActiveTabKey(key)}
    />
  );
};

export default DetailForms;
