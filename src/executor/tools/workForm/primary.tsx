import WorkReportViewer from '@/components/DataStandard/ReportForm/Viewer';
import WorkFormViewer from '@/components/DataStandard/WorkForm/Viewer';
import FormService from '@/ts/scripting/core/services/FormService';
import WorkFormService from '@/ts/scripting/core/services/WorkFormService';
import { Empty, Tabs } from 'antd';
import React, { useMemo, useRef, useState } from 'react';
import { model, schema } from '../../../ts/base';
import { useRefInit } from '@/hooks/useRefInit';

interface PrimaryForm {
  allowEdit: boolean;
  form: schema.XForm;
  info: model.FormInfo;
  service: WorkFormService;
  node: model.WorkNodeModel;
}

export const PrimaryForm: React.FC<PrimaryForm> = (props) => {
  const service = useRefInit(() => new FormService(props.form, props.service));
  const getData = () => {
    let d = service.current.formData.after.at(-1);
    if (d) {
      props.service.updatePrimaryData(props.form.id, d);
      service.current.initRules(d);
    }
    return d;
  };
  const data = useRef(getData());
  if (!data.current) {
    return <Empty>数据加载失败</Empty>;
  }
  switch (props.form.typeName) {
    case '表单':
    case '主表':
      return (
        <WorkFormViewer
          allowEdit={props.allowEdit}
          info={props.info}
          data={data.current}
          service={service.current}
        />
      );
    case '报表':
      return (
        <WorkReportViewer
          allowEdit={props.allowEdit}
          form={props.form}
          info={props.info}
          fields={service.current.fields}
          data={data.current}
          formData={service.current.formData}
          rules={props.service.model.rules}
          belong={service.current.belong}
          readonly={!props.allowEdit}
          service={props.service}
          onValuesChange={(field, value) => {
            service.current.onValueChange(field, value, data.current);
          }}
        />
      );
    default:
      return <>未维护该类型表单打开方式</>;
  }
};

interface IProps {
  service: WorkFormService;
  node: model.WorkNodeModel;
}

const PrimaryForms: React.FC<IProps> = (props) => {
  if (props.node.primaryForms.length < 1) return <></>;
  const [activeTabKey, setActiveTabKey] = useState(props.node.primaryForms[0].id);
  const loadItems = () => {
    const items = [];
    for (const form of props.node.primaryForms) {
      let info =
        props.node.forms.find((item) => item.id == form.id) ?? ({} as model.FormInfo);
      if (
        props.service.model.rules?.find(
          (a) => a.destId == form.id && a.typeName == 'visible' && !a.value,
        )
      ) {
        continue;
      }
      items.push({
        key: form.id,
        label: form.name,
        forceRender: true,
        children: (
          <PrimaryForm
            allowEdit={props.service.allowEdit}
            info={info}
            form={form}
            service={props.service}
            node={props.node}
          />
        ),
      });
    }
    return items;
  };
  return (
    <Tabs
      className="ogo-tabs-primary"
      items={loadItems()}
      activeKey={activeTabKey}
      onChange={(key) => setActiveTabKey(key)}
    />
  );
};

export default PrimaryForms;
