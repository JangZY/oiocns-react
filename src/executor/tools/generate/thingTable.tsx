import React, { MutableRefObject, useRef } from 'react';
import { model, schema } from '@/ts/base';
import { GenerateColumn } from './columns';
import {
  Column,
  DataGrid,
  IDataGridOptions,
  Selection,
  Export,
  Summary,
} from 'devextreme-react/data-grid';
import { Dropdown, message } from 'antd';
import { FullThingColumns } from '@/config/column';
import { ItemType } from 'antd/lib/menu/hooks/useItems';
import { AiOutlineEllipsis } from 'react-icons/ai';
import { Workbook } from 'exceljs';
import orgCtrl from '@/ts/controller';
import saveAs from 'file-saver';
import { isUndefined } from 'lodash';

interface IProps extends IDataGridOptions {
  form?: schema.XForm;
  beforeSource?: schema.XThing[];
  fields: model.FieldModel[];
  dataIndex?: 'attribute' | 'property';
  dataMenus?: {
    items: Array<ItemType & { hide?: boolean }>;
    onMenuClick: (key: string, data: any) => void;
  };
  select?: boolean;
  differences?: any[];
  reference?: MutableRefObject<DataGrid<schema.XThing, string> | null>;
}

/** 使用form生成表单 */
const GenerateThingTable = (props: IProps) => {
  const lock = useRef(false);
  const fields = FullThingColumns(props.fields, props.form?.typeName);
  const needHideFieldIds = fields
    .filter((i) => i.options?.visible)
    .map((i) => i.id)
    .slice(18);
  fields.forEach((item) => {
    if (needHideFieldIds.includes(item.id)) {
      item.options!.visible = false;
    }
  });

  // 整行渲染设置
  const onRowPrepared = (e: any) => {
    if (e.data?.redRowFlag) {
      // 整行字体标红
      e.rowElement.style.color = 'red';
    }
  };
  return (
    <DataGrid<schema.XThing, string>
      keyExpr="id"
      width="100%"
      ref={props.reference}
      height={props.height ?? '100%'}
      columnMinWidth={props.columnMinWidth ?? 80}
      focusedRowEnabled
      allowColumnReordering
      allowColumnResizing
      columnAutoWidth
      showColumnLines
      onRowPrepared={onRowPrepared}
      showRowLines
      rowAlternationEnabled
      hoverStateEnabled
      columnResizingMode={'widget'}
      headerFilter={{ visible: true }}
      onExporting={async (e) => {
        if (lock.current) {
          message.error('正在导出数据，请稍后再试!');
          return;
        }
        lock.current = true;
        try {
          if (e.format === 'xlsx') {
            e.component.beginCustomLoading('正在导出数据...');
            const options = e.component.getDataSource().loadOptions();
            const workbook = new Workbook();
            const worksheet = workbook.addWorksheet(props.form?.name);
            const columns = e.component.getVisibleColumns();
            const filter = e.component.getCombinedFilter();
            worksheet.columns = e.component.getVisibleColumns().map((column) => {
              return { header: column.caption, key: column.dataField };
            });
            let skip = 0;
            let take = 500;
            let pass = true;
            while (pass) {
              let result: model.LoadResult<any> = {
                data: [],
                groupCount: 0,
                summary: [],
                totalCount: 0,
                msg: '',
                success: true,
                code: 0,
              };
              if (!e.selectedRowsOnly) {
                result = (await e.component
                  .getDataSource()
                  .store()
                  .load({ skip, take, ...options, filter })) as any;
              } else {
                result.data = e.component.getSelectedRowsData();
              }
              if (result.data.length == 0 || !Array.isArray(result?.data)) {
                pass = false;
                break;
              } else {
                let data = result.data;
                const userTypeList = fields.filter((i) => i.valueType === '用户型');
                data.forEach((item: any, rowIndex) => {
                  for (const column of columns) {
                    const valueMap = (column.lookup as any)?.valueMap;
                    if (valueMap && column.dataField) {
                      item[column.dataField] = valueMap[item[column.dataField]];
                    }
                    const user =
                      userTypeList.find((its) => its.code === column.dataField) ??
                      undefined;
                    const regex = /^.*[\u4e00-\u9fa5]+.*$/;
                    if (user && column.dataField && !regex.test(item[user.code])) {
                      item[user.code] =
                        orgCtrl.user.findMetadata<schema.XEntity>(item[user.code])
                          ?.name ?? item[user.code];
                    }
                  }
                  worksheet.addRow(item);
                  if (item.redRowFlag) {
                    // 在这里添加条件判断并设置字体颜色
                    for (let colIndex = 0; colIndex < columns.length; colIndex++) {
                      const cell = worksheet.getCell(rowIndex + 2, colIndex + 1);
                      cell.font = { color: { argb: 'FF0000' } };
                    }
                  }
                });
                skip += result.data.length;
                if (e.selectedRowsOnly) {
                  pass = false;
                }
              }
            }
            workbook.xlsx.writeBuffer().then((buffer) => {
              saveAs(
                new Blob([buffer], { type: 'application/octet-stream' }),
                (props.form?.name ?? '表单') + '.xlsx',
              );
            });
            e.component.endCustomLoading();
          }
        } finally {
          lock.current = false;
        }
      }}
      filterRow={{
        visible: Array.isArray(props.dataSource) ? !!props.dataSource?.length : true,
      }}
      columnFixing={{ enabled: true }}
      scrolling={{
        showScrollbar: 'onHover',
        mode: 'standard',
      }}
      columnChooser={{
        enabled: true,
        mode: 'select',
        height: 500,
        search: {
          enabled: true,
        },
      }}
      paging={{ pageSize: 20, enabled: true }}
      pager={{
        visible: true,
        showInfo: true,
        showNavigationButtons: true,
        allowedPageSizes: [10, 30, 40, 50, 200],
      }}
      searchPanel={{ width: 300, highlightCaseSensitive: true, visible: true }}
      showBorders={true}
      {...props}
      onSelectionChanged={(e) => {
        const info = { ...e };
        info.selectedRowsData = e.selectedRowsData.map((data) => {
          const newData: any = {};
          fields.forEach((c) => {
            if (props.dataIndex === 'attribute') {
              if (!isUndefined(data[c.id])) {
                newData[c.id] = data[c.id];
              }
            } else {
              if (!isUndefined(data[c.code])) {
                newData[c.id] = data[c.code];
              }
            }
          });
          return newData;
        });
        props.onSelectionChanged?.apply(this, [info]);
      }}
      onRowDblClick={
        props.onRowDblClick ||
        ((e) => {
          const existUpdateMenu = props.dataMenus?.items?.find(
            (item) => item?.key === 'update',
          );
          const existDetailMenu = props.dataMenus?.items?.find(
            (item) => item?.key === 'detail',
          );
          (existUpdateMenu || existDetailMenu) &&
            props.dataMenus?.onMenuClick(existUpdateMenu ? 'update' : 'detail', e.data);
        })
      }>
      {fields.map((field) => {
        if (props.differences && props.differences.length > 0) {
          if (props.differences.includes(field.id) || field.id === 'id') {
            return GenerateColumn(field, props.beforeSource, props.dataIndex);
          }
        } else {
          return GenerateColumn(field, props.beforeSource, props.dataIndex);
        }
      })}
      {props.dataMenus && (
        <Column
          dataField="操作"
          type={'buttons'}
          width={30}
          cellRender={(row) => {
            return (
              <Dropdown
                menu={{
                  items: props.dataMenus?.items?.filter((item) => !item.hide),
                  onClick: (info) => props.dataMenus?.onMenuClick(info.key, row.data),
                }}
                placement="bottom">
                <div
                  style={{
                    cursor: 'pointer',
                    width: '50px',
                    display: 'flex',
                    alignItems: 'center',
                  }}>
                  <AiOutlineEllipsis fontSize={18} rotate={90} />
                  更多
                </div>
              </Dropdown>
            );
          }}></Column>
      )}
      {props.select && <Selection mode="multiple" showCheckBoxesMode={'always'} />}
      <Export
        enabled
        allowExportSelectedData={props.selection ? true : false}
        formats={['xlsx']}
      />
      <Summary {...props.summary} />
    </DataGrid>
  );
};

export default GenerateThingTable;
