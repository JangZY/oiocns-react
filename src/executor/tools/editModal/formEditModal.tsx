import { Modal } from 'antd';
import React from 'react';
import { kernel, model, schema } from '@/ts/base';
import { IBelong } from '@/ts/core';
import WorkFormViewer from '@/components/DataStandard/WorkForm/Viewer';
import FormService, { setDefaultField } from '@/ts/scripting/core/services/FormService';
import WorkFormService from '@/ts/scripting/core/services/WorkFormService';
import { XThing } from '@/ts/base/schema';

interface IFormEditProps {
  form: schema.XForm;
  fields: model.FieldModel[];
  belong: IBelong;
  create: boolean;
  initialValues?: XThing;
  formData?: any;
}

const FormEditModal = async ({
  form,
  fields,
  belong,
  create,
  initialValues,
}: IFormEditProps) => {
  if (create) {
    const res = await kernel.createThing(belong.id, [], '');
    if (res.success && Array.isArray(res.data) && res.data.length > 0) {
      await setDefaultField(res.data[0], fields, belong);
      initialValues = res.data[0];
    }
  }
  const editData = { ...initialValues } as XThing;
  const work = WorkFormService.createStandalone(belong, form, fields, true);
  const server = new FormService(form, work);

  return await new Promise<XThing>((s, e) => {
    function close() {
      modal.destroy();
      work.dispose();
    }

    const modal = Modal.confirm({
      icon: <></>,
      width: '80vw',
      className: 'edit-modal',
      okText: `确认${create ? '新增' : '变更'}`,
      cancelText: '关闭',
      onCancel: () => {
        close();
        e('close');
      },
      content: (
        <div
          style={{
            maxHeight: '70vh',
            width: '100%',
            overflowY: 'scroll',
            minHeight: 600,
          }}>
          <WorkFormViewer allowEdit={true} data={editData} service={server} />
        </div>
      ),
      onOk: async () => {
        let errors = work.validateAll();
        if (errors.length === 0) {
          close();
          s(editData);
        } else {
          throw new Error('校验失败');
        }
      },
    });
  });
};

export default FormEditModal;
