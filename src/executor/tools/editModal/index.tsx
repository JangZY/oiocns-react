import FormEditModal from './formEditModal';
import FormSelectModal from './formSelectModal';
import SummaryFormModal from './summaryFormModal';

export const EditModal = {
  showFormEdit: FormEditModal,
  showFormSelect: FormSelectModal,
  SummaryFormModal: SummaryFormModal,
};
