import {
  Card,
  Collapse,
  Empty,
  Spin,
  Tabs,
  Timeline,
  Button,
  Popover,
  List,
  Skeleton,
} from 'antd';
import React, { useEffect, useRef, useState } from 'react';
import cls from './index.module.less';
import { IWorkTask, IIdentity, IWork } from '@/ts/core';
import orgCtrl from '@/ts/controller';
import EntityIcon from '@/components/Common/GlobalComps/entityIcon';
import WorkForm from '@/executor/tools/workForm';
import ProcessTree from '@/components/Common/FlowDesign/ProcessTree';
import { WorkNodeDisplayModel, loadResource } from '@/utils/work';
import TaskDrawer from './drawer';
import useAsyncLoad from '@/hooks/useAsyncLoad';
import { correctWorkNode, getNodeByNodeId } from '@/utils/work';
import TaskApproval from './approval';
import { model, schema } from '@/ts/base';
import { generateUuid } from '@/utils/excel';
import { Controller } from '@/ts/controller';
import { Executors } from './executor';
import { createRoot } from 'react-dom/client';
import PrintTemplate from '@/components/Common/FlowDesign/Config/Components/PrintNode/printTemplate';
import PrintConfigModal from '@/components/Common/FlowDesign/Config/Components/PrintNode/PrintModal';
import { SelectBox } from 'devextreme-react';
import OpenFileDialog from '@/components/OpenFileDialog';
import { CloseOutlined } from '@ant-design/icons';
import InfiniteScroll from 'react-infinite-scroll-component';
export interface TaskDetailType {
  current: IWorkTask;
  finished: () => void;
}

const TaskContent: React.FC<TaskDetailType> = ({ current, finished }) => {
  const [selectNode, setSelectNode] = useState<WorkNodeDisplayModel>();
  const [printModalCreate, setPrintModalCreate] = useState(false);
  const [loaded] = useAsyncLoad(() => current.loadInstance());
  const formData = new Map<string, model.FormEditData>();
  const command = useRef(new Controller('TaskContent'));
  const [printType, setPrintType] = useState<string>('');
  const [print, setPrint] = useState<any>([]);
  const [printModal, setPrintModal] = useState(false);
  const [printLoaded, setPrintLoaded] = useState<boolean>(false);
  const removePrintIframe = () => {
    const oldIframe = document.getElementById('printedIframe');
    if (oldIframe) {
      oldIframe.remove();
    }
  };
  useEffect(() => {
    const fetchData = async () => {
      try {
        const work = (await current.findWorkById(
          current.taskdata.defineId,
        )) as unknown as IWork;
        if (work.application.prints.length == 0 && current.instanceData) {
          work.application.prints = JSON.parse(current.instanceData!.node.resource).print;
        }
        const printsData = await work.application.loadAllPrint();
        if (printsData && !printLoaded) {
          printsData.forEach((item: any) => {
            if (current.instanceData && current.instanceData.node.print) {
              setPrintType(JSON.parse(current.instanceData.node.resource).printData.type);
              if (current.instanceData.node.print.length > 0) {
                current.instanceData.node.print.forEach((item2: any) => {
                  if (item.id === item2.id) {
                    item2 = item;
                  }
                });
                setPrint(current.instanceData.node.print);
                setPrintLoaded(true);
              }
            }
          });
        }
      } catch (error) {
        console.error('Error fetching data:', error);
      }
    };
    fetchData();
    return () => {
      removePrintIframe();
    };
  }, [current.instanceData]);
  const handleRemoveItem = (
    e: React.MouseEvent<HTMLSpanElement, MouseEvent>,
    data: { id: string },
  ) => {
    e.stopPropagation();
    const newPrintData = print.filter((option: { id: string }) => option.id !== data.id);
    //删除保存
    const parsedResource = JSON.parse(current.instanceData!.node.resource);
    let newAttributes = [...parsedResource.printData.attributes].filter(
      (option: { title: string }) => option.title !== data.id,
    );
    let newPrints = [...parsedResource.print].filter(
      (option: { id: string }) => option.id !== data.id,
    );
    parsedResource.printData.attributes = newAttributes;
    parsedResource.print = newPrints;
    const updatedResourceString = JSON.stringify(parsedResource);
    current.instanceData!.node.resource = updatedResourceString;
    setPrint(newPrintData);
  };
  const loadWillApprovalMems = async (
    iden: IIdentity,
  ): Promise<schema.XTarget[] | undefined> => {
    if (iden && iden.loadMembers) {
      const mems = await iden.loadMembers();
      return mems;
    }
    return;
  };
  /** 加载时间条 */
  const LoadTimeline: React.FC<{ current: IWorkTask }> = ({ current }) => {
    if (!current.instance) return <></>;
    const [notApprovalTasksNodeMems, setNotApprovalTasksNodeMems] = useState<{
      [key: string]: any;
    }>({});
    const [isScrolling, setIsScrolling] = useState(false);
    const notApprovalTasks = current.instance.tasks
      ?.filter((a) => a.status < 100)
      ?.sort((a, b) => (a.createTime < b.createTime ? -1 : 1));
    useEffect(() => {
      notApprovalTasks?.forEach(async (task) => {
        const identitys = orgCtrl.user.identitys;
        const givedIdentitys = [...identitys];
        orgCtrl.user.companys.forEach((companys) => {
          if (companys.identitys) {
            givedIdentitys.push(...companys.identitys);
          }
        });
        const lastIden = givedIdentitys.find((ide) => ide.id === task.identityId);
        if (!lastIden) return;
        const mems = await loadWillApprovalMems(lastIden);
        const res = current.directory.target.findMetadata<IIdentity>(task.identityId);
        if (res?.typeName === '角色' && mems) {
          setNotApprovalTasksNodeMems({
            ...notApprovalTasksNodeMems,
            [task.identityId]: [...mems],
          });
        }
      });
    }, []);
    const styleDisplay = { display: 'flex', alignItems: 'center' };
    const stylePaddding = { paddingRight: '24px' };
    const loadMoreData = () => {
      if (!isScrolling) {
        return;
      }
    };
    const loader = (content: string | any[]) => {
      if (content.length % 20 !== 0 || content.length === 0 || !isScrolling) {
        return <></>;
      } else {
        return <Skeleton avatar paragraph={{ rows: 1 }} active />;
      }
    };
    if (current.instance) {
      const resource = JSON.parse(current.instanceData?.node.resource ?? '{}');
      return (
        <Timeline>
          <Timeline.Item color={'green'}>
            <Card>
              <div style={{ justifyContent: 'space-between', ...styleDisplay }}>
                <div style={{ display: 'flex' }}>
                  <div style={{ ...stylePaddding }}>起始</div>
                  <div style={{ ...stylePaddding }}>
                    {current.instance.createTime.substring(
                      0,
                      current.instance.createTime.length - 4,
                    )}
                  </div>
                  <div style={{ ...stylePaddding }}>
                    发起人：
                    <EntityIcon entityId={current.instance.createUser} showName />
                  </div>
                  <div style={{ ...stylePaddding }}>{current.instance.content}</div>
                </div>
                {printLoaded && (
                  <a
                    onClick={() => {
                      setPrintModalCreate(true);
                    }}>
                    添加打印模板
                  </a>
                )}
                {printLoaded && (
                  <SelectBox
                    showClearButton
                    value={printType}
                    placeholder="请选择打印模板"
                    dataSource={print}
                    displayExpr={'name'}
                    valueExpr={'id'}
                    onFocusIn={() => {
                      setPrintType('');
                    }}
                    onValueChange={(e) => {
                      if (!e) return false;
                      setPrintType(e);
                      //保存
                      const parsedResource = JSON.parse(
                        current.instanceData!.node.resource,
                      );
                      parsedResource.printData.type = e;
                      const updatedResourceString = JSON.stringify(parsedResource);
                      current.instanceData!.node.resource = updatedResourceString;
                      setPrintModal(true);
                      //e是拿到的id，放到一个数据里，保存掉。然后进入模版页面
                    }}
                    itemRender={(data) => (
                      <div style={{ display: 'flex', justifyContent: 'space-between' }}>
                        <span
                          style={{
                            whiteSpace: 'pre-wrap',
                            wordBreak: 'break-all',
                            wordWrap: 'break-word',
                          }}>
                          {data.name}
                        </span>
                        <CloseOutlined onClick={(e) => handleRemoveItem(e, data)} />
                      </div>
                    )}
                  />
                )}
                {resource.printData &&
                  resource.printData.attributes.length > 0 &&
                  printLoaded && (
                    <Button
                      onClick={() => {
                        removePrintIframe();
                        const iframe = document.createElement(
                          'IFRAME',
                        ) as HTMLIFrameElement;
                        iframe.setAttribute(
                          'style',
                          'position:fixed;width:100%;height:100%;left:0px;top:0px; z-index: 1000; background: rgba(0, 0, 0, 0); display: none;',
                        );
                        iframe.setAttribute('id', 'printedIframe');
                        iframe.onload = () => {
                          let doc = iframe.contentWindow?.document;
                          const loading = () => {
                            setTimeout(() => {
                              iframe.contentWindow?.focus();
                              iframe.contentWindow?.print();
                            }, 1000);
                            if (navigator.userAgent.indexOf('MSIE') > 0) {
                              document.body.removeChild(iframe);
                            }
                          };
                          createRoot(doc as unknown as Element | DocumentFragment).render(
                            <PrintTemplate
                              printData={resource.printData}
                              print={print}
                              current={current}
                              loading={loading}
                            />,
                          );
                        };
                        document.body.appendChild(iframe);
                      }}>
                      打印
                    </Button>
                  )}
              </div>
              <Collapse ghost>
                {current.instanceData && (
                  <WorkForm
                    allowEdit={false}
                    belong={current.belong}
                    nodeId={current.instanceData.node.id}
                    data={current.instanceData}
                    isDone={current.metadata.status >= 100}
                  />
                )}
              </Collapse>
            </Card>
          </Timeline.Item>
          {current.instance.tasks
            ?.filter((a) => a.status >= 100)
            ?.sort((a, b) => (a.createTime < b.createTime ? -1 : 1))
            .map((item, index) => {
              return (
                <div key={`${item.id}_100_${index}`}>
                  {item.records?.map((record) => {
                    var isPass = record.status < 200;
                    return (
                      <Timeline.Item key={`${record.id}_${index}`} color={'green'}>
                        <Card>
                          <div style={{ display: 'flex' }}>
                            <div style={{ ...stylePaddding }}>{item.node?.nodeType}</div>
                            <div style={{ ...stylePaddding }}>
                              {item.createTime.substring(0, item.createTime.length - 4)}
                            </div>
                            <div style={{ ...stylePaddding }}>
                              审批人：
                              <EntityIcon entityId={record.createUser} showName />
                            </div>
                            <div
                              style={{
                                ...stylePaddding,
                                color: `${isPass ? '' : 'red'}`,
                              }}>
                              审批结果：{isPass ? '通过' : '拒绝'}
                            </div>
                            <div>
                              {record.comment && <div>审批意见：{record.comment}</div>}
                            </div>
                          </div>
                          <Collapse ghost>
                            {current.instanceData && (
                              <WorkForm
                                allowEdit={false}
                                belong={current.belong}
                                nodeId={item.nodeId}
                                data={current.instanceData}
                              />
                            )}
                          </Collapse>
                          <Executors
                            nodeId={item.nodeId}
                            trigger={'after'}
                            current={current}
                            formData={formData}
                            command={command.current}
                          />
                        </Card>
                      </Timeline.Item>
                    );
                  })}
                </div>
              );
            })}
          {notApprovalTasks?.map((item, index) => {
            const res = current.directory.target.findMetadata<IIdentity>(item.identityId);
            return (
              <div key={`${item.id}_1_${index}`}>
                <Timeline.Item color={'red'}>
                  <Card>
                    <div style={{ display: 'flex', alignItems: 'center' }}>
                      <div style={{ ...stylePaddding }}>{item.node?.nodeType}</div>
                      <div style={{ ...stylePaddding }}>
                        {item.createTime.substring(0, item.createTime.length - 4)}
                      </div>
                      <div style={{ color: 'red', ...stylePaddding }}>待审批</div>
                      <div style={{ ...stylePaddding }}>审核节点：{item.approveType}</div>
                      {res?.typeName === '人员' && (
                        <div style={{ ...styleDisplay }}>
                          <div style={{ ...styleDisplay }}>
                            审批人：{<EntityIcon entityId={res?.id} showName />}
                          </div>
                          <div style={{ marginLeft: '10px' }}>
                            {res?.code ? `联系方式：${res.code}` : ''}
                          </div>
                        </div>
                      )}
                      {res?.typeName === '角色' && (
                        <div>
                          <Popover
                            title="角色人员"
                            content={() => {
                              return (
                                <div
                                  id="scrollableDiv"
                                  style={{
                                    height: 300,
                                    overflow: 'auto',
                                    padding: '10px 16px',
                                  }}>
                                  <InfiniteScroll
                                    dataLength={notApprovalTasksNodeMems[item.identityId]}
                                    next={loadMoreData}
                                    hasMore={
                                      notApprovalTasksNodeMems[item.identityId].length <
                                      20
                                    }
                                    loader={loader(
                                      notApprovalTasksNodeMems[item.identityId],
                                    )}
                                    endMessage={<></>}
                                    scrollableTarget="scrollableDiv">
                                    <List
                                      itemLayout="horizontal"
                                      dataSource={
                                        notApprovalTasksNodeMems[item.identityId]
                                      }
                                      renderItem={(mem: any) => (
                                        <List.Item>
                                          <div>
                                            {<EntityIcon entityId={mem?.id} showName />}
                                          </div>
                                          <div style={{ marginLeft: '10px' }}>
                                            {mem?.code ? `联系方式：${mem.code}` : ''}
                                          </div>
                                        </List.Item>
                                      )}
                                    />
                                  </InfiniteScroll>
                                </div>
                              );
                            }}>
                            <Button style={{ padding: 0 }} type="link">
                              审批角色：{res.name}
                            </Button>
                          </Popover>
                        </div>
                      )}
                    </div>
                    <Executors
                      nodeId={item.nodeId}
                      trigger={'before'}
                      current={current}
                      formData={formData}
                      command={command.current}
                    />
                  </Card>
                </Timeline.Item>
              </div>
            );
          })}
          {printModalCreate && (
            <OpenFileDialog
              multiple
              title={`选择打印模板`}
              accepts={['打印模板']}
              rootKey={''}
              excludeIds={print.filter((i: any) => i.id).map((file: any) => file.id)}
              onCancel={() => setPrintModalCreate(false)}
              onOk={(files) => {
                //保存到办事上
                setPrint([...print, ...files]);
                //保存
                /* const parsedResource = JSON.parse(current.instanceData!.node.resource);
                let newAttributes = [...parsedResource.printData.attributes];
                let newPrints = [...parsedResource.print];
                files.map((file) => {
                  newAttributes.push({ title: file.id });
                  newPrints.push(file);
                });
                const updatedPrintData = {
                  ...parsedResource.printData,
                  attributes: newAttributes,
                }; */
                setPrintModalCreate(false);
              }}
            />
          )}
          {printModal && (
            <PrintConfigModal
              refresh={(cur) => {
                current.instanceData!.node.resource = JSON.stringify(cur);
                setPrintModal(false);
              }}
              resource={JSON.parse(current.instanceData!.node.resource)}
              printType={printType}
              print={print}
              work={current}
              primaryForms={current.instanceData!.node.primaryForms}
              detailForms={current.instanceData!.node.detailForms}
            />
          )}
        </Timeline>
      );
    }
    return <></>;
  };

  const loadItems = () => {
    var forms = getNodeByNodeId(
      current.taskdata.nodeId,
      current.instanceData!.node,
    )?.primaryForms;
    var existForm = forms && forms.length > 0;
    /** tab标签页 */
    const items = [
      {
        key: '1',
        label: `办事详情`,
        children: (
          <>
            <div className={cls['content']}>
              {/** 时间轴 */}
              <LoadTimeline current={current} />
              {!existForm && current.typeName !== '子流程' && (
                <TaskApproval task={current} fromData={formData} finished={finished} />
              )}
            </div>
          </>
        ),
      },
      {
        key: '2',
        label: `流程跟踪`,
        children: (
          <ProcessTree
            isEdit={false}
            resource={loadResource(
              correctWorkNode(JSON.parse(current.instance?.data || '{}').node),
            )}
            onSelectedNode={(node) => setSelectNode(node)}
          />
        ),
      },
    ];
    if (existForm) {
      const Center: React.FC = () => {
        const [key, setKey] = useState(generateUuid());
        useEffect(() => {
          const id = command.current.subscribe(() => setKey(generateUuid()));
          return () => {
            return command.current.unsubscribe(id);
          };
        }, []);
        return (
          <>
            <WorkForm
              key={key}
              allowEdit={true}
              belong={current.belong}
              nodeId={current.taskdata.nodeId}
              data={current.instanceData!}
            />
            {current.typeName !== '子流程' && (
              <TaskApproval task={current} fromData={formData} finished={finished} />
            )}
          </>
        );
      };
      items.unshift({
        key: '3',
        label: `填写表单`,
        children: <Center></Center>,
      });
    }
    return items;
  };

  if (!loaded) {
    return (
      <Spin tip={'配置信息加载中...'}>
        <div style={{ width: '100%', height: '100%' }}></div>
      </Spin>
    );
  }
  if (current.instance && current.instanceData?.node) {
    return (
      <div className={cls['work-content']}>
        <Card className={cls['work-content-card']}>
          <Tabs items={loadItems()} />
        </Card>
        {selectNode && (
          <TaskDrawer
            current={selectNode}
            isOpen={selectNode != undefined}
            onClose={() => setSelectNode(undefined)}
            instance={current.instance!}
          />
        )}
      </div>
    );
  }
  return (
    <div style={{ width: '100%', height: '100%', textAlign: 'center' }}>
      <h3 style={{ padding: 20 }}>办事数据加载失败!</h3>
      <Empty />
      <TaskApproval task={current} fromData={formData} finished={finished} />
    </div>
  );
};

export default TaskContent;
