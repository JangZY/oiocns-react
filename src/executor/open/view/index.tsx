import React from 'react';
import { IDirectory } from '@/ts/core';
import FormView from './form';
import { schema } from '@/ts/base';

interface ViewContentType {
  form: schema.XForm;
  directory: IDirectory;
  finished: () => void;
}
const Index: React.FC<ViewContentType> = ({ form, directory, finished }) => {
  const opentType = form.viewType ?? '表单';
  switch (opentType) {
    case '表单':
      return <FormView form={form} directory={directory} finished={finished} />;
    case '图表':
    default:
      return <div>暂不支持视图类型： {opentType}</div>;
  }
};

export default Index;
