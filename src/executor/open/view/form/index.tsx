import EntityIcon from '@/components/Common/GlobalComps/entityIcon';
import FullScreenModal from '@/components/Common/fullScreen';
import { Theme } from '@/config/theme';
import GenerateThingTable from '@/executor/tools/generate/thingTable';
import useAsyncLoad from '@/hooks/useAsyncLoad';
import { command, model, schema } from '@/ts/base';
import { IDirectory, TargetType } from '@/ts/core';
import { Spin, message, Layout, Empty } from 'antd';
import CustomStore from 'devextreme/data/custom_store';
import React, { useRef, useState, createRef, useEffect, useCallback } from 'react';
import { ImCopy, ImHistory, ImProfile, ImShuffle, ImTicket } from 'react-icons/im';
import { MdPrint } from 'react-icons/md';
import { formatNumber } from '@/utils';
import { DataGrid, TreeView } from 'devextreme-react';
import { IForm } from '@/ts/core';
import { Form } from '@/ts/core/thing/standard/form';
import Sider from 'antd/lib/layout/Sider';
import { TreeViewTypes } from 'devextreme-react/cjs/tree-view';
import { HistoryFlowView } from '../../form/history/flow';
import { HistoryFileView } from '../../form/history/file';
import ThingView from '../../form/detail';
import InnerTree from './innerTree';
import '../../form/index.less';
import { deptItemType } from './treeDataUtils';

interface IProps {
  form: schema.XForm;
  directory: IDirectory;
  finished: () => void;
}

/** 表单查看--字典项过多 */
const DictFormView: React.FC<IProps> = ({ form, directory, finished }) => {
  const isGroup = directory.target.typeName === TargetType.Group;
  const [fields, setFields] = useState<model.FieldModel[]>([]);
  const metaForm: IForm = new Form(form, directory);
  const [loaded] = useAsyncLoad(async () => {
    setFields(await metaForm.loadFields());
  });
  const [select, setSelcet] = useState();
  const editData: { rows: schema.XThing[] } = { rows: [] };
  let ref = useRef<DataGrid<schema.XThing, string>>(null);
  const treeViewRef = createRef<TreeView<any>>();
  metaForm.fields = fields;
  const FormBrower: React.FC = () => {
    const [treeData, setTreeData] = useState<any>([]);
    const [selectMenu, setSelcetMenu] = useState<any>([]);
    const [treeMatchs, setTreeMatchs] = useState<Record<string, any>>({});
    if (directory.spaceId !== directory.target.belongId) {
      return (
        <Empty
          image="https://gw.alipayobjects.com/zos/antfincdn/ZHrcdLPrvN/empty.svg"
          imageStyle={{
            height: 300,
          }}
          description="该视图暂不支持非管理员查看"
        />
      );
    }
    const loadSpeciesItemMenu = () => {
      const speciesFields = fields.filter((i) => i.options?.species);
      const result: any[] = [];
      for (const filed of speciesFields) {
        const newFiled = filed;
        result.push({
          key: filed.id,
          item: newFiled,
          label: filed.name,
          hasItems: true,
          children: [],
        });
      }
      return result;
    };

    useEffect(() => {
      const result = loadSpeciesItemMenu();
      setTreeData(result);
      setSelcetMenu(result);
    }, []);

    const [center, setCenter] = useState(<></>);
    if (!selectMenu && !treeData) return <></>;

    const loadDeatil = () => {
      if (select) {
        return (
          <ThingView
            form={metaForm}
            thingData={select}
            onBack={() => setSelcet(undefined)}
          />
        );
      }
    };
    const defaultMatchObject = useCallback(() => {
      let matchObj: Record<string, any> = {
        isDeleted: false,
      };
      if (isGroup) {
        matchObj['belongId'] = directory.target.belongId;
      }
      return matchObj;
    }, [directory.target.typeName]);

    const loadContent = () => {
      return (
        <GenerateThingTable
          key={form.key}
          reference={ref}
          height={'100%'}
          selection={{
            mode: 'multiple',
            allowSelectAll: true,
            selectAllMode: 'page',
            showCheckBoxesMode: 'always',
          }}
          form={metaForm.metadata}
          fields={metaForm.fields}
          onSelectionChanged={(e) => {
            editData.rows = e.selectedRowsData;
          }}
          onRowDblClick={(e: any) => setSelcet(e.data)}
          dataSource={
            new CustomStore({
              key: 'id',
              async load(loadOptions: any) {
                loadOptions.filter = metaForm.parseFilter(loadOptions.filter);
                if (!loadOptions.options) {
                  loadOptions.options = {
                    match: {},
                  };
                }
                const classify = metaForm.parseClassify();
                if (loadOptions.filter.length == 0 && Object.keys(classify).length == 0) {
                  return { data: [], totalCount: 0 };
                }
                loadOptions.userData = [];
                if (selectMenu.item?.value) {
                  loadOptions.userData.push(selectMenu.item.value);
                } else if (selectMenu.item?.code) {
                  loadOptions.options = loadOptions.options || {};
                  loadOptions.options.match = loadOptions.options.match || {};
                  loadOptions.options.match[selectMenu.item.code] = { _exists_: true };
                }
                loadOptions.options.match = Object.assign(
                  loadOptions.options.match || {},
                  defaultMatchObject(),
                  treeMatchs,
                );
                //拦截不携带单位标识的请求
                if (isGroup && !JSON.stringify(loadOptions).includes('belongId')) {
                  console.error('未携带单位标识');
                  return { data: [], totalCount: 0 };
                }
                return await metaForm.loadThing(loadOptions);
              },
            })
          }
          remoteOperations={true}
          summary={{
            totalItems: metaForm.fields
              .filter((item) => item.options?.isSummary)
              .map((item) => {
                return {
                  column: item.code,
                  summaryType: 'sum',
                  customizeText: (info) => {
                    return '总计:' + formatNumber(info?.value?.toString() ?? 0, 2);
                  },
                };
              }),
          }}
          toolbar={{
            visible: true,
            items: [
              {
                name: 'print',
                location: 'after',
              },
              {
                name: 'print',
                location: 'after',
                widget: 'dxButton',
                options: {
                  icon: 'print',
                  onClick: () => {
                    if (!metaForm.metadata.print)
                      return message.error('请先配置打印模板');
                    if (editData.rows.length === 0)
                      return message.error('请选择需要打印的数据');
                    command.emitter(
                      'executor',
                      'printEntity',
                      metaForm,
                      'multiple',
                      editData.rows,
                    );
                  },
                },
              },
              {
                name: 'exportButton',
                location: 'after',
              },
              {
                name: 'columnChooserButton',
                location: 'after',
              },
              {
                name: 'searchPanel',
                location: 'after',
              },
            ],
          }}
          dataMenus={{
            items: [
              {
                key: 'createNFT',
                label: '生成存证',
                icon: <ImTicket fontSize={22} color={Theme.FocusColor} />,
                onClick: () => {
                  message.success('存证成功!');
                },
              },
              {
                key: 'copyBoard',
                label: '复制数据',
                icon: <ImCopy fontSize={22} color={Theme.FocusColor} />,
              },
              {
                key: 'startWork',
                label: '发起办事',
                icon: <ImShuffle fontSize={22} color={Theme.FocusColor} />,
              },
              {
                key: 'showHistory',
                label: '历史流程',
                icon: <ImHistory fontSize={22} color={Theme.FocusColor} />,
              },
              {
                key: 'filesManager',
                label: '历史附件',
                icon: <ImProfile fontSize={22} color={Theme.FocusColor} />,
              },
              {
                key: 'printEntity',
                label: '标签打印',
                hide: !form.metadata?.print || form.metadata?.print?.config?.hide,
                icon: <MdPrint fontSize={22} color={Theme.FocusColor} />,
              },
            ],
            onMenuClick(_key, _data) {
              switch (_key) {
                case 'showHistory':
                  setCenter(
                    <HistoryFlowView
                      form={metaForm}
                      thing={_data}
                      finished={() => setCenter(<></>)}
                    />,
                  );
                  break;
                case 'filesManager':
                  setCenter(
                    <HistoryFileView
                      form={metaForm}
                      thing={_data}
                      finished={() => setCenter(<></>)}
                    />,
                  );
                  break;
                case 'printEntity':
                  if (!metaForm.metadata.print) return message.error('请先配置打印模板');
                  command.emitter('executor', 'printEntity', metaForm, 'single', [_data]);
                  break;
              }
            },
          }}
        />
      );
    };

    const customTreeView = () => {
      const getItem = async (node: TreeViewTypes.Node, fileds: any) => {
        const arr = fileds.map((filed: any) => {
          return filed.id;
        });
        const children = await metaForm.loadItemsByParentId(
          [node.itemData?.item.speciesId],
          arr,
        );
        return children;
      };

      const getChildren = async (node: TreeViewTypes.Node) => {
        let result: any[] = [],
          children: any[] = [],
          items: any[] = [];
        if (node.parent) {
          children = await metaForm.loadItemsByParentId(
            [node.itemData?.item.speciesId],
            [node.key],
          );
          items = await getItem(node, children);
        } else {
          children = await metaForm.loadItemsByParentId(
            [node.itemData?.item.speciesId],
            [undefined],
          );
          items = await getItem(node, children);
        }
        for (const filed of children) {
          let arr: any[] = [];
          items.forEach((item: any) => {
            if (item.parentId === filed.id) {
              const newFiled: any = item;
              newFiled.value = 'S' + newFiled.id;
              arr.push({
                key: item.id,
                item: newFiled,
                label: item.name,
                parentId: item.parentId,
                hasItems: false,
                children: [],
              });
            }
          });
          const newFiled = filed;
          newFiled.value = 'S' + newFiled.id;
          result.push({
            key: filed.id,
            item: newFiled,
            label: filed.name,
            parentId: node.key,
            hasItems: arr.length > 0 ? true : false,
            children: arr,
          });
        }
        return result;
      };

      const createChildren = async (node: TreeViewTypes.Node) => {
        let result: any[] = [];
        if (node) {
          result = await getChildren(node);
        } else {
          result = loadSpeciesItemMenu();
        }
        return result;
      };

      return (
        <div style={{ height: 'calc(100vh - 150px)', overflow: 'auto' }}>
          <TreeView
            ref={treeViewRef}
            dataStructure="plain"
            dataSource={treeData}
            expandNodesRecursive={false}
            createChildren={createChildren}
            keyExpr="key"
            parentIdExpr="parentId"
            selectionMode="single"
            selectNodesRecursive={false}
            displayExpr="label"
            selectByClick={true}
            searchEnabled
            onItemClick={(item) => {
              setSelcetMenu(item.itemData);
            }}
          />
        </div>
      );
    };
    const creatFilterMatchs = (items: deptItemType[]) => {
      const viewDataRange: any = form.options?.viewDataRange || { company: 'belongId' };
      const result: any[] = [];
      const filterData: Record<string, any> = {};
      items.forEach((item) => {
        let filterKey: string = '';
        switch (item.typeName) {
          case TargetType.Person:
            filterKey = viewDataRange['person'];
            break;
          case TargetType.Company:
            filterKey = viewDataRange['company'] ?? 'belongId';
            break;
          case TargetType.Department:
          default:
            filterKey = viewDataRange['department'];
            break;
        }

        if (filterKey) {
          filterData[filterKey] ??= [];
          filterData[filterKey].push(item.id);
        }
      });
      for (const [key, value] of Object.entries(filterData)) {
        const isStartWithEn = /^[A-Za-z]/.test(key as string);
        result.push({ [isStartWithEn ? key : `T${key}`]: { _in_: value } });
      }
      if (isGroup) {
        setTreeMatchs(result[0]);
      } else {
        setTreeMatchs(result.length > 0 ? { _or_: result } : {});
      }
    };

    return (
      <React.Fragment>
        {select ? (
          <>{loadDeatil()}</>
        ) : (
          <Layout className={'main_layout'}>
            <Layout className={'body'}>
              <Sider className={'sider'} width={250}>
                <InnerTree
                  target={directory.target}
                  onSelectChanged={creatFilterMatchs}
                />
                <div className={'container'} id="templateMenu">
                  {customTreeView()}
                </div>
              </Sider>
              <div className={'content'}>{loadContent()}</div>
            </Layout>
          </Layout>
        )}
        {center}
      </React.Fragment>
    );
  };
  return (
    <FullScreenModal
      centered
      open={true}
      fullScreen
      width={'80vw'}
      title={metaForm.name}
      bodyHeight={'80vh'}
      icon={<EntityIcon entityId={metaForm.id} />}
      destroyOnClose
      onCancel={() => finished()}>
      {loaded ? (
        <FormBrower />
      ) : (
        <Spin tip={'配置信息加载中...'}>
          <div style={{ width: '100%', height: '100%' }}></div>
        </Spin>
      )}
    </FullScreenModal>
  );
};

export default DictFormView;
