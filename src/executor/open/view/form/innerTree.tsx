import React, { useEffect, useMemo, useState, useCallback } from 'react';
import { ITarget } from '@/ts/core';
import { DropDownBox } from 'devextreme-react';
import { Input, Switch, Tree } from 'antd';
import { AiOutlineAppstore, AiOutlineBorder } from 'react-icons/ai';
import cls from './index.module.less';
import { deptItemType, getTreeDatas } from './treeDataUtils';
import useAsyncLoad from '@/hooks/useAsyncLoad';
interface InnerTreeType<T> {
  target: ITarget;
  onSelectChanged(items: T[]): void;
}

const { Search } = Input;

const InnerTree = <T extends deptItemType>({
  target,
  onSelectChanged,
}: InnerTreeType<T>) => {
  const [selectedTarget, setSelectedTarget] = useState<T>();
  const [isLinkChildren, setIsLinkChildren] = useState(false);
  const [selectedKeys, setSelectedKeys] = useState<React.Key[]>([]);
  const [expandedKeys, setExpandedKeys] = useState<React.Key[]>([]);
  const [searchValue, setSearchValue] = useState<string>('');
  const [_loaded, dataSource] = useAsyncLoad(async () => {
    return await getTreeDatas<T>(target);
  });

  useEffect(() => {
    if (dataSource && dataSource.length > 0) {
      setSelectedTarget(dataSource[0]);
      onSelectChanged([dataSource[0]]);
    }
  }, [dataSource]);

  const handleItemSelected = useCallback(
    (item: any) => {
      const { node, selected } = item;
      if (selected && node.key !== selectedTarget?.key) {
        const result: T[] = [node];
        if (isLinkChildren && node.hasItems) getSelectedNodes(node, result);
        setSelectedTarget(node);
        onSelectChanged(result);
      }
    },
    [isLinkChildren, onSelectChanged, selectedTarget],
  );

  const getSelectedNodes = useCallback((item: T, result: T[]) => {
    item.children.forEach((child: any) => {
      if (!child.disabled) result.push(child);
      if (child.hasItems) getSelectedNodes(child, result);
    });
  }, []);

  const titleRender = useCallback(
    (node: T) => (
      <div className={cls['tree-title-wrapper']}>
        <div className={cls['tree-title-icon']}>
          {node.hasItems ? (
            <AiOutlineAppstore />
          ) : (
            <AiOutlineBorder style={{ backgroundColor: '#DCDCDC' }} />
          )}
        </div>
        <div className={cls['tree-title']}>{node.label}</div>
      </div>
    ),
    [],
  );
  const treeData = useMemo(() => {
    if (!searchValue) return dataSource ?? [];
    const result: T[] = [];
    const loop = (items: T[]) => {
      items.forEach((item) => {
        if (item.label.includes(searchValue)) result.push(item);
        if (item.children) loop(item.children as T[]);
      });
    };
    loop(dataSource as T[]);
    return result;
  }, [dataSource, searchValue]);

  return (
    <DropDownBox
      displayExpr="label"
      valueExpr="key"
      width="100%"
      dataSource={[selectedTarget]}
      value={selectedTarget?.key || ''}
      placeholder="请选择"
      contentRender={() => (
        <>
          <div className="flex justify-between" style={{ paddingBottom: '10px' }}>
            <div className="flex-auto">关联子级:</div>
            <Switch size="small" onChange={setIsLinkChildren} />
          </div>
          <Search
            onSearch={setSearchValue}
            onChange={(e) => !e.target.value && setSearchValue('')}
            placeholder="请输入"
            enterButton
          />
          <Tree<T>
            fieldNames={{ title: 'label', key: 'key', children: 'children' }}
            treeData={treeData as any}
            titleRender={titleRender}
            expandedKeys={expandedKeys}
            height={400}
            onExpand={setExpandedKeys}
            selectedKeys={selectedKeys}
            onSelect={(keys, info) => {
              setSelectedKeys(keys);
              handleItemSelected(info);
            }}
          />
        </>
      )}
    />
  );
};

export default React.memo(InnerTree);
