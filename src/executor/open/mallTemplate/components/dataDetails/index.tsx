import React, { memo, useCallback, useState, useEffect } from 'react';
import { Modal, Image, Tabs, Carousel, Skeleton } from 'antd';
import type { schema, model } from '@/ts/base';
import { IMallTemplate } from '@/ts/core/thing/standard/page/mallTemplate';
import cls from './index.module.less';

interface IDataDetails {
  data: schema.XProduct;
  current: IMallTemplate;
  onCancel: () => void;
}

const DataDetails = ({ current, data, onCancel }: IDataDetails) => {
  const [staging, setStaging] = useState(
    current.shoppingCar.products.some((a) => a.id == data.id),
  );
  const getImage = useCallback((type: string, num?: number) => {
    let images: model.FileItemShare[] = [];
    images = JSON.parse(data[type] || '[]');
    return num ? images.splice(0, num) : images;
  }, []);
  useEffect(() => {
    const id = current.shoppingCar.subscribe(() => {
      setStaging(current.shoppingCar.products.some((a) => a.id == data.id));
    });
    return () => current.shoppingCar.unsubscribe(id);
  }, []);
  const evaluateData = [
    {
      key: 1,
      evaluateTitle: '推荐',
      Score: 5,
      evaluateContent:
        '非常好用！里面的插画非常好看，小熊画的超级可爱！里面还有日记提醒，想得很周全，最好的是还不许付费，写完日记还有奖励贴纸。总提来说非常不错！加油！强烈推荐',
      date: '2月12日',
      evaluatePeople: 'aswegrtyui',
    },
    {
      key: 2,
      evaluateTitle: '推荐',
      Score: 5,
      evaluateContent:
        '非常好用！里面的插画非常好看，小熊画的超级可爱！里面还有日记提醒，想得很周全，最好的是还不许付费，写完日记还有奖励贴纸。总提来说非常不错！加油！强烈推荐',
      date: '2月12日',
      evaluatePeople: 'aswegrtyui',
    },
    {
      key: 3,
      evaluateTitle: '推荐',
      Score: 5,
      evaluateContent:
        '非常好用！里面的插画非常好看，小熊画的超级可爱！里面还有日记提醒，想得很周全，最好的是还不许付费，写完日记还有奖励贴纸。总提来说非常不错！加油！强烈推荐',
      date: '2月12日',
      evaluatePeople: 'aswegrtyui',
    },
    {
      key: 4,
      evaluateTitle: '推荐',
      Score: 5,
      evaluateContent:
        '非常好用！里面的插画非常好看，小熊画的超级可爱！里面还有日记提醒，想得很周全，最好的是还不许付费，写完日记还有奖励贴纸。总提来说非常不错！加油！强烈推荐',
      date: '2月12日',
      evaluatePeople: 'aswegrtyui',
    },
    {
      key: 5,
      evaluateTitle: '推荐',
      Score: 5,
      evaluateContent:
        '非常好用！里面的插画非常好看，小熊画的超级可爱！里面还有日记提醒，想得很周全，最好的是还不许付费，写完日记还有奖励贴纸。总提来说非常不错！加油！强烈推荐',
      date: '2月12日',
      evaluatePeople: 'aswegrtyui',
    },
    {
      key: 6,
      evaluateTitle: '推荐',
      Score: 5,
      evaluateContent:
        '非常好用！里面的插画非常好看，小熊画的超级可爱！里面还有日记提醒，想得很周全，最好的是还不许付费，写完日记还有奖励贴纸。总提来说非常不错！加油！强烈推荐',
      date: '2月12日',
      evaluatePeople: 'aswegrtyui',
    },
  ];
  const loadMoreData = useCallback(() => {
    console.log(loadMoreData, 'loadMoreData');
  }, []);

  const tabs = [
    {
      key: '1',
      label: '商品概括',
      children: (
        <>
          <div className={cls.productImage}>
            {getImage('introduceImage', 2).map((item, index) => {
              return (
                <Image
                  key={index}
                  src={item.shareLink}
                  alt=""
                  preview={false}
                  className={cls.image}
                  placeholder={<Skeleton.Image className={cls.image} />}
                />
              );
            })}
          </div>
          <div className={cls.productDescription}>{data.introduceInfo}</div>
        </>
      ),
    },
    {
      key: '2',
      label: '功能介绍',
      children: (
        <>
          <div className={cls.productDescription}>{data.useInfo}</div>
          <div className={cls.footer}>
            {!!getImage('useInfoImage', 1).length && (
              <Image
                src={getImage('useInfoImage', 1)[0]?.shareLink}
                preview={false}
                placeholder={<Skeleton.Image className={cls.featureImage} />}
                className={cls.featureImage}
              />
            )}
          </div>
        </>
      ),
    },
    // {
    //   key: '3',
    //   label: '评价',
    //   children: (
    //     <>
    //       <div className={cls.evaluate}>
    //         <div className={cls.evaluateInfo}>
    //           <div className={cls.evaluationScore}>4.9</div>
    //           <div>满分 5 分</div>
    //           <div>432 个评价</div>
    //         </div>
    //         <InfiniteScroll
    //           dataLength={evaluateData.length}
    //           next={loadMoreData}
    //           hasMore={true}
    //           loader={<></>}
    //           scrollableTarget="scrollableDiv">
    //           <List
    //             dataSource={evaluateData}
    //             renderItem={(i) => {
    //               return (
    //                 <List.Item key={i.key} className={cls.evaluateItem}>
    //                   <div className={cls.evaluateTitle}>{i.evaluateTitle}</div>
    //                   <Rate disabled allowHalf value={i.Score} />
    //                   <div>{i.evaluateContent}</div>
    //                   <div className={cls.evaluateFooter}>
    //                     <div>{i.date}</div>
    //                     <div>{i.evaluatePeople}</div>
    //                   </div>
    //                 </List.Item>
    //               );
    //             }}
    //           />
    //         </InfiniteScroll>
    //       </div>
    //     </>
    //   ),
    // },
  ];
  const onChange = useCallback(() => {}, []);
  return (
    <Modal width={800} open={true} title={data.title} footer={null} onCancel={onCancel}>
      <div className={cls.header}>
        {current.metadata.template === 'dataTemplate' ? (
          <Image
            src={getImage('icon', 1)[0]?.shareLink}
            preview={false}
            placeholder={<Skeleton.Image className={cls.productImg} />}
            className={cls.productImg}
          />
        ) : (
          <Carousel dots={false} className={cls.carousel}>
            {getImage('images').map((item, index) => {
              return (
                <Image
                  key={index}
                  src={item.shareLink}
                  alt=""
                  preview={false}
                  className={cls.carousel}
                  placeholder={<Skeleton.Image className={cls.carousel} />}
                />
              );
            })}
          </Carousel>
        )}
        <div
          className={cls.left}
          style={{
            flexDirection:
              current.metadata.template === 'dataTemplate' ? 'row' : 'column',
          }}>
          <div className={cls.info}>
            <div className={cls.title}>{data.title || '[未设置名称]'}</div>
            <div className={cls.introduce}>{data.remark}</div>
            {current.metadata.mode !== 'sharing' && (
              <div className={cls.buyerFooter}>
                <div className={cls.price}>￥{data.price || 0}</div>
                <div>{0}人已买</div>
              </div>
            )}
          </div>
          <div className={cls.controls}>
            <div className={cls.purchaseNow}>
              {current.metadata.mode === 'sharing' ? (
                <span>立即申领</span>
              ) : (
                <span onClick={() => {}}>立即购买</span>
              )}
            </div>
            <div
              className={cls.purchaseCar}
              onClick={() => {
                if (staging) {
                  current.shoppingCar.remove(data);
                } else {
                  current.shoppingCar.create(data);
                }
              }}>
              <span>加入购物车</span>
            </div>
          </div>
        </div>
      </div>
      <Tabs defaultActiveKey="1" items={tabs} onChange={onChange} />
    </Modal>
  );
};

export default memo(DataDetails);
