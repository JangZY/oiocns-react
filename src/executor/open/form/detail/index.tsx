import { Card, Tabs } from 'antd';
import React, { useState } from 'react';
import { IForm } from '@/ts/core';
import { schema } from '@/ts/base';
import { ImUndo2 } from 'react-icons/im';
import CardView from './CardView';
import MatterView from './MatterView';
import FormView from './FormView';

interface IProps {
  form: IForm;
  thingData: schema.XThing;
  onBack: () => void;
}

/**
 * 物-查看
 * @returns
 */
const ThingView: React.FC<IProps> = (props) => {
  const ThingActive = localStorage.getItem('activeView');
  const [activeTabKey, setActiveTabKey] = useState(ThingActive ? ThingActive : 'card');

  const onTabsChange = (e: string) => {
    setActiveTabKey(e);
    localStorage.setItem('activeView', e);
  };

  /** 加载每一项 */
  const loadItems = () => {
    const items = [
      {
        key: 'card',
        label: '卡片视图',
        children: <CardView form={props.form} thingData={props.thingData} />,
      },
      {
        key: 'matter',
        label: '事项视图',
        children: <MatterView form={props.form} thingData={props.thingData} />,
      },
      {
        key: 'form',
        label: '归档痕迹视图',
        children: <FormView form={props.form} thingData={props.thingData} />,
      },
    ];
    return items;
  };

  return (
    <Card>
      <Tabs
        activeKey={activeTabKey}
        onChange={onTabsChange}
        items={loadItems()}
        tabBarExtraContent={
          <div
            style={{ display: 'flex', cursor: 'pointer' }}
            onClick={() => {
              props.onBack();
            }}>
            <a style={{ paddingTop: '2px' }}>
              <ImUndo2 />
            </a>
            <a style={{ paddingLeft: '6px' }}>返回</a>
          </div>
        }
      />
    </Card>
  );
};

export default ThingView;
