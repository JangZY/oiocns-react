import EntityIcon from '@/components/Common/GlobalComps/entityIcon';
import FullScreenModal from '@/components/Common/fullScreen';
import WorkForm from '@/components/DataStandard/WorkForm';
import MinLayout from '@/components/MainLayout/minLayout';
import { Theme } from '@/config/theme';
import GenerateThingTable from '@/executor/tools/generate/thingTable';
import useAsyncLoad from '@/hooks/useAsyncLoad';
import useMenuUpdate from '@/hooks/useMenuUpdate';
import { command, schema } from '@/ts/base';
import { Controller } from '@/ts/controller';
import { IForm } from '@/ts/core';
import { Spin, message } from 'antd';
import CustomStore from 'devextreme/data/custom_store';
import React, { useState, useEffect } from 'react';
import { ImCopy, ImHistory, ImProfile, ImShuffle, ImTicket } from 'react-icons/im';
import { MdPrint, MdOutlinePrint } from 'react-icons/md';
import * as config from './config';
import ThingView from './detail';
import { HistoryFileView } from './history/file';
import { HistoryFlowView } from './history/flow';
import { formatNumber } from '@/utils';
import orgCtrl from '@/ts/controller';
import './index.less';
import { createRoot } from 'react-dom/client';
import PrintTemplate from './printTemplate';
import { TableModel } from '@/ts/base/model';
import { userFormatFilter } from '@/utils/tools';

interface IProps {
  form: IForm;
  finished: () => void;
}

/** 表单查看 */
const FormView: React.FC<IProps> = ({ form, finished }) => {
  useEffect(() => {
    if (form.metadata.primaryPrints) {
      //所有的打印模板拿到后做自动更新
      orgCtrl.loadPrint().then((result) => {
        result.forEach((item) => {
          form.metadata.primaryPrints.forEach((primaryPrint: { id: string; name: string; table: TableModel[]; }) => {
            if (item.id == primaryPrint.id) {
              primaryPrint.name = item.name;
              primaryPrint.table = item.table;
            }
          });
        });
      });
    }
  }, []);
  const [select, setSelcet] = useState();
  const editData: { rows: schema.XThing[] } = { rows: [] };
  const [loaded] = useAsyncLoad(() => form.loadContent());
  const removePrintIframe = () => {
    const oldIframe = document.getElementById('printedIframe');
    if (oldIframe) {
      oldIframe.remove();
    }
  };
  const FormBrower: React.FC = () => {
    const [, rootMenu, selectMenu, setSelectMenu] = useMenuUpdate(
      () => config.loadSpeciesItemMenu(form),
      new Controller(form.key),
    );
    const [center, setCenter] = useState(<></>);
    if (!selectMenu || !rootMenu) return <></>;

    const loadDeatil = () => {
      if (select) {
        return (
          <ThingView form={form} thingData={select} onBack={() => setSelcet(undefined)} />
        );
      }
    };

    const loadContent = () => {
      return (
        <GenerateThingTable
          key={form.key}
          height={'100%'}
          selection={{
            mode: 'multiple',
            allowSelectAll: true,
            selectAllMode: 'page',
            showCheckBoxesMode: 'always',
          }}
          form={form.metadata}
          fields={form.fields}
          onSelectionChanged={(e) => {
            editData.rows = e.selectedRowsData;
          }}
          onRowDblClick={(e: any) => setSelcet(e.data)}
          dataSource={
            new CustomStore({
              key: 'id',
              async load(loadOptions: any) {
                loadOptions.filter = await userFormatFilter(loadOptions.filter, form);
                loadOptions.filter = form.parseFilter(loadOptions.filter);
                const classify = form.parseClassify();
                if (loadOptions.filter.length == 0 && Object.keys(classify).length == 0) {
                  return { data: [], totalCount: 0 };
                }
                loadOptions.userData = [];
                if (selectMenu.item?.value) {
                  loadOptions.userData.push(selectMenu.item.value);
                } else if (selectMenu.item?.code) {
                  loadOptions.options = loadOptions.options || {};
                  loadOptions.options.match = loadOptions.options.match || {};
                  loadOptions.options.match[selectMenu.item.code] = { _exists_: true };
                }
                return await form.loadThing(loadOptions);
              },
            })
          }
          remoteOperations={true}
          summary={{
            totalItems: form.fields
              .filter((item) => item.options?.isSummary)
              .map((item) => {
                return {
                  column: item.code,
                  summaryType: 'sum',
                  customizeText: (info) => {
                    return '总计:' + formatNumber(info?.value?.toString() ?? 0, 2);
                  },
                };
              }),
          }}
          toolbar={{
            visible: true,
            items: [
              {
                name: 'print',
                location: 'after',
              },
              {
                name: 'print',
                location: 'after',
                widget: 'dxButton',
                options: {
                  icon: 'print',
                  onClick: () => {
                    if (!form.metadata.primaryPrints)
                      return message.error('请先配置打印模板');
                    if (form.metadata.primaryPrints.length === 0)
                      return message.error('请先配置打印模板');
                    if (editData.rows.length === 0)
                      return message.error('请选择需要打印的数据');
                    removePrintIframe();
                    const iframe = document.createElement('IFRAME') as HTMLIFrameElement;
                    iframe.setAttribute(
                      'style',
                      'position:fixed;width:100%;height:100%;left:0px;top:0px; z-index: 1000; background: rgba(0, 0, 0, 0); display: none;',
                    );
                    iframe.setAttribute('id', 'printedIframe');
                    iframe.onload = () => {
                      let doc = iframe.contentWindow?.document;
                      const loading = () => {
                        setTimeout(() => {
                          iframe.contentWindow?.focus();
                          iframe.contentWindow?.print();
                        }, 1000);
                        if (navigator.userAgent.indexOf('MSIE') > 0) {
                          document.body.removeChild(iframe);
                        }
                      };
                      createRoot(doc as unknown as Element | DocumentFragment).render(
                        <PrintTemplate
                          printData={form.metadata.printData}
                          print={form.metadata.primaryPrints}
                          current={editData.rows}
                          loading={loading}
                        />,
                      );
                    };
                    document.body.appendChild(iframe);
                  },
                },
              },
              {
                name: 'print',
                location: 'after',
                widget: 'dxButton',
                options: {
                  icon: 'print',
                  onClick: () => {
                    if (!form.metadata.print) return message.error('请先配置打印模板');
                    if (editData.rows.length === 0)
                      return message.error('请选择需要打印的数据');
                    command.emitter(
                      'executor',
                      'printEntity',
                      form,
                      'multiple',
                      editData.rows,
                    );
                  },
                },
              },
              {
                name: 'exportButton',
                location: 'after',
              },
              {
                name: 'columnChooserButton',
                location: 'after',
              },
              {
                name: 'searchPanel',
                location: 'after',
              },
            ],
          }}
          dataMenus={{
            items: [
              {
                key: 'createNFT',
                label: '生成存证',
                icon: <ImTicket fontSize={22} color={Theme.FocusColor} />,
                onClick: () => {
                  message.success('存证成功!');
                },
              },
              {
                key: 'copyBoard',
                label: '复制数据',
                icon: <ImCopy fontSize={22} color={Theme.FocusColor} />,
              },
              {
                key: 'startWork',
                label: '发起办事',
                icon: <ImShuffle fontSize={22} color={Theme.FocusColor} />,
              },
              {
                key: 'showHistory',
                label: '历史流程',
                icon: <ImHistory fontSize={22} color={Theme.FocusColor} />,
              },
              {
                key: 'filesManager',
                label: '历史附件',
                icon: <ImProfile fontSize={22} color={Theme.FocusColor} />,
              },
              {
                key: 'printEntity',
                label: '标签打印',
                hide: !form.metadata?.print || form.metadata?.print?.config?.hide,
                icon: <MdPrint fontSize={22} color={Theme.FocusColor} />,
              },
              {
                key: 'formPrint',
                label: '表单打印',
                icon: <MdOutlinePrint fontSize={22} color={Theme.FocusColor} />,
              },
            ],
            onMenuClick(_key, _data) {
              let obj: any = {};
              const pattern = /^T\d+$/; // 定义正则表达式模式，以"T"开头，后面跟一串数字
              const iframe = document.createElement('IFRAME') as HTMLIFrameElement;
              switch (_key) {
                case 'showHistory':
                  setCenter(
                    <HistoryFlowView
                      form={form}
                      thing={_data}
                      finished={() => setCenter(<></>)}
                    />,
                  );
                  break;
                case 'filesManager':
                  setCenter(
                    <HistoryFileView
                      form={form}
                      thing={_data}
                      finished={() => setCenter(<></>)}
                    />,
                  );
                  break;
                case 'printEntity':
                  if (!form.metadata.print) return message.error('请先配置打印模板');
                  command.emitter('executor', 'printEntity', form, 'single', [_data]);
                  break;
                case 'formPrint':
                  Object.entries(_data).forEach(([_key, value]) => {
                    if (pattern.test(_key)) {
                      _key = _key.replace('T', '');
                    }
                    obj[_key] = value;
                  });
                  if (!form.metadata.primaryPrints)
                    return message.error('请先配置打印模板');
                  if (form.metadata.primaryPrints.length == 0)
                    return message.error('请先配置打印模板');
                  removePrintIframe();
                  iframe.setAttribute(
                    'style',
                    'position:fixed;width:100%;height:100%;left:0px;top:0px; z-index: 1000; background: rgba(0, 0, 0, 0); display: none;',
                  );
                  iframe.setAttribute('id', 'printedIframe');
                  iframe.onload = () => {
                    let doc = iframe.contentWindow?.document;
                    const loading = () => {
                      setTimeout(() => {
                        iframe.contentWindow?.focus();
                        iframe.contentWindow?.print();
                      }, 1000);
                      if (navigator.userAgent.indexOf('MSIE') > 0) {
                        document.body.removeChild(iframe);
                      }
                    };
                    createRoot(doc as unknown as Element | DocumentFragment).render(
                      <PrintTemplate
                        printData={form.metadata.printData}
                        print={form.metadata.primaryPrints}
                        current={[obj]}
                        loading={loading}
                      />,
                    );
                  };
                  document.body.appendChild(iframe);
                  break;
              }
            },
          }}
        />
      );
    };
    return (
      <React.Fragment>
        {select ? (
          <>{loadDeatil()}</>
        ) : (
          <MinLayout
            selectMenu={selectMenu}
            onSelect={(data) => {
              setSelectMenu(data);
            }}
            siderMenuData={rootMenu}>
            {loadContent()}
          </MinLayout>
        )}
        {center}
      </React.Fragment>
    );
  };
  return (
    <FullScreenModal
      centered
      open={true}
      fullScreen
      width={'80vw'}
      title={form.name}
      bodyHeight={'80vh'}
      icon={<EntityIcon entityId={form.id} />}
      destroyOnClose
      onCancel={() => finished()}>
      {loaded ? (
        form.canDesign ? (
          <FormBrower />
        ) : (
          <WorkForm form={form} />
        )
      ) : (
        <Spin tip={'配置信息加载中...'}>
          <div style={{ width: '100%', height: '100%' }}></div>
        </Spin>
      )}
    </FullScreenModal>
  );
};

export default FormView;
