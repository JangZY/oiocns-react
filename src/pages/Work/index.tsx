import React, { useCallback, useEffect, useRef, useState } from 'react';
import { IFile, IWork, IWorkTask, TaskStatus, TaskTypeName } from '@/ts/core';
import { command } from '@/ts/base';
import orgCtrl from '@/ts/controller';
import { Spin, Image } from 'antd';
import DirectoryViewer from '@/components/Directory/views';
import { loadFileMenus } from '@/executor/fileOperate';
import { cleanMenus, getUuid } from '@/utils/tools';
import useTimeoutHanlder from '@/hooks/useTimeoutHanlder';
import AppLayout from '@/components/MainLayout/appLayout';
import { useFlagCmdEmitter } from '@/hooks/useCtrlUpdate';
import SelectWork from './select';
import FullScreenModal from '@/components/Common/fullScreen';
import OrgIcons from '@/components/Common/GlobalComps/orgIcons';
import message from '@/utils/message';
/**
 * 办事-事项清单
 */
const WorkContent: React.FC = () => {
  const currentFile = useRef<IFile>();
  const currentTag = useRef<string>('待办');
  const [loaded, setLoaded] = useState(false);
  const [selectOpen, setSelectOpen] = useState(false);
  const [content, setContent] = useState<IFile[]>([]);
  const [focusFile, setFocusFile] = useState<IFile>();
  const [submitHanlder, clearHanlder] = useTimeoutHanlder();
  const [todoRefresh, setTodoRefresh] = useState<string>();
  const [autoApproval, setAutoApproval] = useState(false);
  const [autoName, setAutoName] = useState('-');
  const [, key] = useFlagCmdEmitter('_commons', () => {
    if (currentTag.current === '常用') {
      loadContent('常用');
    }
  });
  useEffect(() => {
    const id = orgCtrl.work.notity.subscribe((...args) => {
      const [_key, tag] = args;
      if (tag === undefined || tag === '待办') {
        if (currentTag.current === '待办') {
          loadContent('待办');
        } else {
          setTodoRefresh(getUuid());
        }
      } else if (tag === '草稿' && currentTag.current === tag) {
        currentTag.current = tag;
        loadContent(tag);
      }
    });
    return () => {
      orgCtrl.work.notity.unsubscribe(id);
    };
  }, []);

  useEffect(() => {
    if (
      focusFile &&
      autoApproval &&
      currentTag.current === '待办' &&
      (focusFile as IWorkTask).taskdata.approveType === '审批'
    ) {
      setTimeout(() => {
        (focusFile as IWorkTask).approvalTask(TaskStatus.ApprovalStart, '自动通过');
      }, 100);
    } else {
      if (autoApproval && focusFile) {
        message.warn("当前任务需要您手动审批!")
      }
      command.emitter('preview', 'work', focusFile);
    }
  }, [focusFile, autoApproval]);

  const contextMenu = (file?: IFile) => {
    return {
      items: cleanMenus(loadFileMenus(file)) || [],
      onClick: ({ key }: { key: string }) => {
        command.emitter('executor', key, file);
      },
    };
  };

  const focusHanlder = (file: IFile | undefined) => {
    if (file && file.key !== focusFile?.key) {
      setFocusFile(file);
      currentFile.current = file;
    }
  };

  const clickHanlder = (file: IFile | undefined, dblclick: boolean) => {
    if (dblclick && currentTag.current !== '草稿') {
      clearHanlder();
      if (file) {
        command.emitter('executor', 'open', file);
      }
    } else {
      submitHanlder(() => focusHanlder(file), 200);
    }
  };

  const getBadgeCount = useCallback(
    (tag: string) => {
      if (tag === '待办') {
        return orgCtrl.work.todos.length;
      }
      return 0;
    },
    [todoRefresh],
  );

  const loadContent = (tag: string) => {
    if (tag?.length < 2) return;
    if (tag === '常用') {
      loadCommons();
    } else if (tag === '任务') {
      loadReceptions();
    } else if (tag === '草稿') {
      loadDrafts();
    } else {
      loadTasks(tag, []);
    }
  };
  const loadTasks = (tag: string, args: IFile[]) => {
    setLoaded(false);
    let reload = false;
    if (tag === '重载待办') {
      tag = '待办';
      reload = true;
    }
    orgCtrl.work
      .loadContent(tag as TaskTypeName, args.length, reload)
      .then((tasks) => {
        const newTasks = [...args, ...tasks].sort((a, b) => {
          return (
            new Date(b.metadata.updateTime).getTime() -
            new Date(a.metadata.updateTime).getTime()
          );
        });
        setContent([...newTasks]);
        setLoaded(true);
        if (
          currentFile.current == undefined ||
          newTasks.every((a) => a.id !== currentFile.current!.id)
        ) {
          setFocusFile(undefined);
          guidanceChange(newTasks.length);
        }
      })
      .catch((reason) => {
        message.error(reason);
        setContent([]);
        guidanceChange(0);
        setLoaded(true);
      });
  };

  const loadCommons = () => {
    setLoaded(false);
    orgCtrl.loadCommons().then((value) => {
      setLoaded(true);
      const data = value.filter((i) => i.typeName === '办事');
      setContent(data);
    });
  };

  const loadReceptions = () => {
    setLoaded(false);
    orgCtrl.loadReceptions().then((value) => {
      setLoaded(true);
      setContent(value);
    });
  };

  const loadDrafts = () => {
    setLoaded(false);
    orgCtrl.work.loadDraft(true).then((value) => {
      setLoaded(true);
      setContent(value);
    });
  };

  const guidanceChange = (count: number) => {
    const empty = count === 0 ? true : false;
    command.emitter('preview', 'guidance', { empty, type: currentTag.current });
  };

  const renderMore = () => {
    switch (currentTag.current) {
      case '常用':
        return (
          <div className="chat-leftBar-search_more">
            <Image
              preview={false}
              height={24}
              width={24}
              onClick={() => {
                setSelectOpen(true);
              }}
              src={`/svg/operate/todoMore.svg?v=1.0.1`}
            />
          </div>
        );
      case '待办':
        return (
          <>
            <div className="chat-leftBar-search_more">
              <OrgIcons
                type="/operate/reload"
                size={22}
                notAvatar
                title="重载待办"
                onClick={() => loadContent('重载待办')}
              />
            </div>
            {autoName.length > 1 && (
              <div className="chat-leftBar-search_more">
                <OrgIcons
                  type={`/operate/${autoApproval ? 'setActive' : 'active'}`}
                  size={22}
                  notAvatar
                  title={`${autoApproval ? '停止自动审批' : '开启自动审批'}`}
                  onClick={() => setAutoApproval((pre) => !pre)}
                />
              </div>
            )}
          </>
        );
    }
    return <></>;
  };
  return (
    <AppLayout previewFlag={'work'}>
      <Spin spinning={!loaded} tip={'加载中...'}>
        <div key={key} style={{ marginLeft: 10, padding: 2, fontSize: 18 }}>
          <span style={{ paddingLeft: 10 }}>办事</span>
        </div>
        <DirectoryViewer
          isMenu
          extraTags={false}
          currentTag={currentTag.current}
          height={'calc(100% - 110px)'}
          selectFiles={[]}
          focusFile={focusFile}
          content={content}
          badgeCount={getBadgeCount}
          tagChanged={(t) => {
            currentTag.current = t;
            currentFile.current = undefined;
            loadContent(t);
          }}
          initTags={['常用', '草稿', '任务', '待办', '已办', '抄送', '已发起', '已完结']}
          fileOpen={(entity, dblclick) => clickHanlder(entity as IWorkTask, dblclick)}
          contextMenu={(entity) => contextMenu(entity as IWorkTask)}
          rightBars={renderMore()}
          onScrollEnd={() => {
            if (['已办', '抄送', '已发起', '已完结'].includes(currentTag.current)) {
              loadTasks(currentTag.current, content);
            }
          }}
          onFilter={(filter) => {
            setAutoName(filter);
            setAutoApproval(false);
          }}
          onFocusFile={(file) => setFocusFile(file as IFile)}
        />
        {selectOpen && (
          <FullScreenModal
            open
            title={'选择办事'}
            onCancel={() => {
              setSelectOpen(false);
            }}
            destroyOnClose
            width={1000}
            bodyHeight={'75vh'}>
            <SelectWork
              onSelected={(work: IWork) => {
                setSelectOpen(false);
                setTimeout(() => {
                  setFocusFile(work);
                }, 500);
              }}
            />
          </FullScreenModal>
        )}
      </Spin>
    </AppLayout>
  );
};
export default WorkContent;
