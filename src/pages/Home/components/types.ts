export interface NavigationItem {
  key: string;
  label: string;
  backgroundImageUrl: string;
  type: string;
  component: any;
  id?: string;
  belongId?: string;
  tag?: string[];
}
