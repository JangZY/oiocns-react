import { schema } from '@/ts/base';
import { Xbase } from '@/ts/base/schema';
import { IPerson, ICompany, XObject } from '@/ts/core';
import { Application } from '@/ts/core/thing/standard/application';

export interface IPortalTemplates {
  target: IPerson;
  portalTemplates: any[];
  newPortalTemplates: any[];
  startEdit(): void;
  update(spaceId: string, metadata: Xbase): Promise<boolean>;
  save(): Promise<boolean>;
}
interface ITabItem {
  id?: string; // 模板id
  name: string; //模板名称
  key: string;
  metadata: schema.XEntity;
  type: '1' | '5' | '6'; //1为应用，5为页面模板，6为商城模板
  tags: string[]; //模板标签类型
  belongId: string; //模板归属单位
  alias?: string; //模板别名
  sort?: number; //模板排序
  isDefault?: boolean; //是否默认模板
  extraJson?: string; //模板扩展信息
}

export class PortalTemplates implements IPortalTemplates {
  target: IPerson;
  tablist: ITabItem[] = [];
  new_tablist: ITabItem[] = [];
  _hasLoaded: boolean = false;
  cachePath: string = 'portalTemplate';
  /** 用户缓存对象 */
  cacheObj: XObject<schema.Xbase>;
  constructor(user: IPerson) {
    this.target = user;
    this.cacheObj = user.cacheObj;
    this.loadTabData(true);
  }
  //输出所有模板
  get portalTemplates(): any[] {
    return this.tablist;
  }
  get newPortalTemplates(): any[] {
    return this.new_tablist;
  }

  //设置新数据
  startEdit() {
    this.new_tablist = this.tablist;
  }

  // 获取数据
  async loadTabData(reload: boolean = false) {
    if (reload || !this._hasLoaded) {
      this.tablist = (await this.cacheObj.get(this.cachePath)) ?? [];
    }
  }
  // 新增/更新数据
  async update(spaceId: string, metadata: schema.XEntity) {
    if (spaceId && metadata) {
      const currentCompany = this.target.companys.find((comp) => comp.id === spaceId)!;
      const params: ITabItem = {
        key: 'home_Module' + `${spaceId}_${metadata.id}`,
        id: `${spaceId}_${metadata.id}`,
        metadata,
        name: metadata.name,
        type: metadata.typeName === '应用' ? '1' : metadata.typeName === '商城模板' ? '6' : '5',
        tags: [metadata.typeName === '应用' ? '应用模板' : metadata.typeName === '商城模板' ? '商城模板' : '定制模板'],
        belongId: spaceId,
      };
      if (currentCompany && currentCompany.name) {
        params.tags.push(currentCompany.name);
      }
      if (this.new_tablist.find((v) => v.id === params.id)) {
        this.new_tablist = this.new_tablist.filter((v) => v.id !== params.id);
      } else {
        this.new_tablist.push(params);
      }
    }
    return true;
  }
  //更新结果
  async save(notify: boolean = true): Promise<boolean> {
    const success = await this.cacheObj.set(this.cachePath, this.new_tablist);
    this.cacheObj.setValue(this.cachePath, this.new_tablist);
    this.tablist = this.new_tablist;
    if (success && notify) {
      await this.cacheObj.notity(this.cachePath, this.new_tablist, true, true);
    }
    return success;
  }
  // 创建类的实体
  createEntity(item: ITabItem) {
    const getApp = async (company: ICompany, metData: schema.XApplication) => {
      await company.directory.resource.applicationColl.all(true);
      let xApplications = company.resource.applicationColl.cache.filter(
        (i) => i.directoryId === metData.belongId,
      );
      let newApp = new Application(metData, company.directory, undefined, xApplications);
      return newApp;
    };
    const company = this.target.companys.find((comp) => comp.id === item.belongId);
    if (company) {
      switch (item.metadata.typeName) {
        case '应用':
          return getApp(company, item.metadata as schema.XApplication);
        case '模板':
          break;

        default:
          break;
      }
    }
  }
  // 获取其中一个tab的相关存储信息
  getTabItemInfo(id: string) { }

  setTabItemInfo() { }
}
