import React from 'react';
import orgCtrl from '@/ts/controller';
import cls from '../../index.module.less';
import SwitchSpace from '../../../../Common/switchSpace';
import { IBelong } from '@/ts/core';

interface IProps {
  onChange: () => void;
}

const ToggleSpace: React.FC<IProps> = ({onChange}) => {
  return (
    <div className="cardGroup">
      <div className={cls['select-space']}>
        <SwitchSpace
          content={[orgCtrl.user, ...orgCtrl.user.companys]}
          typeName={'工作台'}
          selectFiles={[orgCtrl.home.current]}
          searchText={'workbench'}
          selectType={1}
          onChange={(selects: IBelong[]) => {
            if (selects && selects.length > 0) {
              orgCtrl.home.switchSpace(selects[0]);
              onChange();
            }
          }}></SwitchSpace>
      </div>
    </div>
  );
};
export default ToggleSpace;
