import React, { useEffect, useState } from 'react';
import orgCtrl from '@/ts/controller';
import { ICompany, IWorkTask } from '@/ts/core';
import { useFlagCmdEmitter } from '@/hooks/useCtrlUpdate';
import FullScreenModal from '@/components/Common/fullScreen';
import cls from '../../index.module.less';
import HomeWork from '../../work';
import HomeChat from '../../chat';

interface IProps {
  company: ICompany;
}
// 事项详情
const MatterInfo: React.FC<IProps> = (props) => {
  const [workCount, setWorkCount] = useState(0);
  const [msgCount, setMsgCount] = useState(0);
  const [openType, setOpenType] = useState<number>(0);
  useFlagCmdEmitter('session', () => {
    setMessageCount();
  });

  const findTodo = (todos: IWorkTask[], groupName: string) => {
    return todos.filter((item: IWorkTask) => {
      return item.groupTags.includes(groupName);
    });
  };

  const setMessageCount = () => {
    props.company.loadGroups().then(async (res: any) => {
      let chats = [];
      for (let target of res) {
        chats.push(...target.chats);
      }
      const allMsgChats = chats.filter((i) => i.isMyChat);
      const allMsgCount = allMsgChats.reduce((sum, i) => sum + i.chatdata.noReadCount, 0);
      setMsgCount(allMsgCount);
    });
  };

  const setWorkCountFn = () => {
    const workId = orgCtrl.work.notity.subscribe(async () => {
      setWorkCount(findTodo(orgCtrl.work.todos, props.company.name).length);
    });
    orgCtrl.work.notity.unsubscribe(workId);
  };

  useEffect(() => {
    setMessageCount();
    setWorkCountFn();
  }, [props.company]);

  const renderCmdBtn = (cmd: string, title: string, number: number) => {
    return (
      <div className={cls['matter-item']} onClick={() => setOpenType(Number(cmd))}>
        <div>
          <img src={'/img/home/matt-' + cmd + '.png'} alt="" />
          <span>{title}</span>
        </div>
        <div className={cls['matter-number']}>{number}</div>
      </div>
    );
  };
  return (
    <>
      <div className="cardGroup">
        <div className="cardItem">
          <div className="cardItem-header">
            <span className="title">事项提醒</span>
          </div>
          <div style={{ width: '100%', minHeight: 60 }} className={cls['matter-content']}>
            {renderCmdBtn('4', '未读', msgCount)}
            {renderCmdBtn('2', '待办', workCount)}
            {renderCmdBtn('1', '提醒', 0)}
            {renderCmdBtn('3', '任务', msgCount)}
          </div>
          <FullScreenModal
            open={openType > 0}
            title={'事项'}
            width={'80vw'}
            bodyHeight={'70vh'}
            onCancel={() => setOpenType(0)}>
            {(openType === 3 || openType === 4) && (
              <HomeChat item={props.company} openType={openType}></HomeChat>
            )}
            {openType === 2 && (
              <HomeWork item={props.company} openType={openType}></HomeWork>
            )}
          </FullScreenModal>
        </div>
      </div>
    </>
  );
};
export default MatterInfo;
