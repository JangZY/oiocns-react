import { command } from '@/ts/base';
import { IBelong, IFile } from '@/ts/core';
import { Dropdown, Spin, message } from 'antd';
import React, { useEffect, useRef, useState } from 'react';
import EntityIcon from '@/components/Common/GlobalComps/entityIcon';
import { cleanMenus } from '@/utils/tools';
import { loadFileMenus } from '@/executor/fileOperate';
import OpenFileDialog from '@/components/OpenFileDialog';
import { loadSettingMenu } from '@/components/OpenFileDialog/config';
import orgCtrl from '@/ts/controller';
import { ImBooks } from 'react-icons/im';
import { useHistory } from 'react-router-dom';

interface IProps {
  space: IBelong;
}

interface IContent {
  groupName: string;
  content: IFile[];
}

const ContentCard: React.FC<IProps> = ({ space }) => {
  // 发送快捷命令
  const history = useHistory();
  const [loaded, setLoaded] = useState(false);
  const [content, setContent] = useState<IContent[]>([]);

  useEffect(() => {
    const id = command.subscribeByFlag(orgCtrl.home.commentsFlag(space), async () => {
      setLoaded(false);
      await loadComments();
      setLoaded(true);
    });
    return () => {
      command.unsubscribeByFlag(id);
    };
  }, [space]);

  const loadComments = async () => {
    const newContent: IContent[] = [];
    const comments = await orgCtrl.home.loadCommons(space);
    comments.forEach((item) => {
      var index = newContent.findIndex((i) => i.groupName === item.typeName);
      if (index > -1) {
        newContent[index].content.push(item);
      } else {
        newContent.push({
          groupName: item.typeName,
          content: [item],
        });
      }
    });
    setContent(newContent);
  };

  return (
    <div style={{ flexWrap: 'wrap' }} className="cardGroup">
      <div className="cardItem" style={{ minHeight: 80 }}>
        <div className="cardItem-header">
          <span className="title">常用</span>
          <span className="extraBtn" onClick={() => history.push('store')}>
            <ImBooks size={15} /> <span>去设常用</span>
          </span>
        </div>
        <div style={{ width: '100%', minHeight: 60 }} className="cardItem-viewer">
          <Spin spinning={!loaded} tip={'加载中...'}>
            {content.map((item) => {
              return (
                <LoadGroupCard
                  key={item.groupName}
                  groupName={item.groupName}
                  content={item.content}
                />
              );
            })}
          </Spin>
        </div>
      </div>
    </div>
  );
};

const LoadGroupCard = (props: { groupName: string; content: IFile[] }) => {
  const [expanded, setExpanded] = useState<boolean>(false);
  const [editMode, setEditMode] = useState<boolean>(false);
  const [showNum, setShowNum] = useState<number>(14);
  const containerRef = useRef(null);
  useEffect(() => {
    const resizeObserver = new ResizeObserver((entries) => {
      for (let entry of entries) {
        const { width } = entry.contentRect;
        const num = Math.floor(width / 190);
        setShowNum(num * 2);
      }
    });
    if (containerRef.current) {
      resizeObserver.observe(containerRef.current);
    }
    return () => {
      if (containerRef.current) {
        resizeObserver.unobserve(containerRef.current);
      }
    };
  }, [props.content]);

  const contextMenu = (file: IFile) => {
    return {
      items: cleanMenus(loadFileMenus(file)) || [],
      onClick: ({ key }: { key: string }) => {
        command.emitter('executor', key, file);
      },
    };
  };
  const loadCommonCard = (item: IFile) => (
    <Dropdown key={item.key} menu={contextMenu(item)} trigger={['contextMenu']}>
      <div
        className="appCard"
        onClick={() => {
          command.emitter('executor', 'open', item);
        }}>
        <EntityIcon entity={item.metadata} size={35} />
        <div className="appName">{item.name}</div>
        <div className="teamName">{item.directory.target.name}</div>
      </div>
    </Dropdown>
  );

  return (
    <div style={{ minHeight: 80 }} className="cardItem" ref={containerRef}>
      <span className="cardItem-header" style={{ display: 'block', height: '20px' }}>
        <span className="title">{props.groupName}</span>
        {props.content.length > showNum && (
          <span
            className="extraBtn"
            style={{ paddingTop: '2px', paddingRight: '20px' }}
            onClick={() => setExpanded(!expanded)}>
            {expanded ? '收起' : '展开'}
          </span>
        )}
      </span>
      {props.content.length === 0 && <div style={{ textAlign: 'center' }}>暂无数据</div>}
      {props.content.length > 0 && (
        <div className="cardItem-viewer">
          <div className="cardGroup" style={{ flexWrap: 'wrap' }}>
            {expanded
              ? props.content.map((item) => loadCommonCard(item))
              : props.content.slice(0, showNum).map((item) => loadCommonCard(item))}
          </div>
        </div>
      )}
      {editMode && (
        <OpenFileDialog
          title={`选择`}
          rightShow={false}
          accepts={[props.groupName]}
          showFile
          rootKey={''}
          excludeIds={props.content.map((item) => item.id)}
          onLoadMenu={() => loadSettingMenu('', false, ['单位', '目录', '应用'])}
          onCancel={() => {
            setEditMode(false);
          }}
          onOk={(files) => {
            if (files.length > 0) {
              files[0].toggleCommon().then((success: boolean) => {
                if (success) {
                  message.info('设置成功');
                  setEditMode(false);
                }
              });
            }
          }}
        />
      )}
    </div>
  );
};

export default ContentCard;
