import React from 'react';
import orgCtrl from '@/ts/controller';
import { GroupActivity } from '@/ts/core';
import GroupActivityItem from './group';

const CohortActivity: React.FC = () => {
  var activitys = orgCtrl.chats
    .filter((i) => i.isMyChat && i.isGroup)
    .map((i) => i.activity);
  const cohortActivity = new GroupActivity(
    orgCtrl.user,
    activitys.filter((a, i) => activitys.indexOf(a) === i),
    false,
  );
  return <GroupActivityItem activity={cohortActivity} />;
};

export default CohortActivity;
