import React from 'react';
import orgCtrl from '@/ts/controller';
import { GroupActivity } from '@/ts/core';
import GroupActivityItem from './group';

const NewActivity: React.FC = () => {
  var activitys = orgCtrl.chats
    .filter(async (a) => {
      await a.target.loadMembers();
      return a.isMyChat && a.isGroup;
    })
    .map((i) => i.activity);
  activitys = activitys.filter((a, i) => activitys.indexOf(a) === i);
  var friendsActivitys = [
    orgCtrl.user.session.activity,
    ...orgCtrl.user.memberChats.map((i) => i.activity),
  ];
  var allActivitys = [...activitys, ...friendsActivitys];
  allActivitys = allActivitys.filter((a, i) => allActivitys.indexOf(a) === i);
  const cohortActivity = new GroupActivity(orgCtrl.user, allActivitys, true);
  return <GroupActivityItem activity={cohortActivity} />;
};

export default NewActivity;
