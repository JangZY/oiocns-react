import React from 'react';
import HomeNavTemplate from './components/HomeNavTemplate/index';
import cls from './index.module.less';

const Home: React.FC = () => {
  return (
    <div className={cls.homepage}>
      <HomeNavTemplate />
    </div>
  );
};
export default Home;
